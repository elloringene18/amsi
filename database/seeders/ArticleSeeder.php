<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Traits\CanCreateSlug;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class ArticleSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Generator $faker, Article $model)
    {
        $this->faker = $faker;
        $this->model = $model;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = [];

        $data = [
            [
                'title' => 'Alia Al Shamsi ',
                'content' => '<p>Alia Al Shamsi is an Emarati Italian author whose career started as a photojournalist travelling the world before settling back in Dubai and teaching photography at the American University of Sharjah.</p>
<p>With a BA in Photography from Griffith University, Australia and MA in Photo-Image from Durham University, UK, Alia&rsquo;s work has been exhibited internationally alongside renowned art photographers such as Julia Margaret Cameron and Cindy Sherman, as well as her works being a part of the UAE Embassy in Washington DC private collection and displayed at Rove Hotels &ndash; and these are just a couple of her many accolades.</p>
<p>Now having released her latest body of literary work, Alia Al Shamsi takes readers on a poetic journey through moments of introspection. In her poems are heart-felt musings of life events, detailing the trials and tribulations of past relationships and her personal development through these lived experiences. Her new anthology, &ldquo;The Ocean Sees Through My Soul,&rdquo; covers many facets, sharing intimate feelings of heartbreak, frustration, powerlessness, self-reflection and redemption. Alia writes with hard-won optimism and a mature hopefulness to find meaning in life&rsquo;s unexpected moments.&nbsp;<br /> <br /> &ldquo;Jotted down in the privacy of my diaries across a period of ten years, this book is made up of little parts of me, in every poem a little story of a place, a time and an emotion gone by. Written as therapy, the book follows my personal journey through finding strength in my own vulnerability,&rdquo; says Alia.<br /> <br /> &ldquo;The Ocean Sees Through My Soul&rdquo; is the fourth book written by Al Shamsi and the first book dedicated to an adult audience with her past releases being for children. Previous books include &ldquo;Alayah,&rdquo; for which she received the first Dubai Culture Publishing Award, &ldquo;Night and Day,&rdquo; one of the UAE&rsquo;s first three silent books published in 2018 and &ldquo;You Are Made of Stars,&rdquo; an exercise book with Kharirifa Majirifa &ndash; a collection of reimagined folklore stories written by a group of Emirati authors.&nbsp;<br /> <br /> &ldquo;The Ocean Sees Through My Soul&rdquo; is currently available for pre-order in both Arabic and English&nbsp;<a href="https://store.rawashen.com/product/المحيط-يرى-بين-أضلعي/">here</a>.</p>
',
                'date' => '2021-03-17',
                'photo' => 'img/news/alia.jpg',
                'thumbnail' => 'img/news/alia-thumb.jpg',
            ],
            [
                'title' => 'Masa Adnan, Class of 2017 ISAS',
                'content' => '<p>Masa Adnan, Class of 2017 ISAS</p>
<p>Masa is an Endurance rider in Sheikh Mansour Bin Zayed Al Nahyan Team. Her discipline and determination prepares her to overcome the challenges she faces leading up to every race. <a href="https://equestrianhub.com.au/endurance-riding-a-test-of-strength-for-horse-and-rider/">Endurance riding is a test of strength for horse and rider</a></p>
<p>When Masa is not mounting her horse, she is studying international relations at the University of the Emirates.</p>
<p>Notable highlights</p>
<p>1<sup>st</sup> in Sheikh Mohmmed bin Zayed al Nahyan Festival 100km for ladies in النسخة الاولى من المهرجان</p>
<p>1<sup>st</sup> place in the Sheikh Mohammed bin Mansour bin Zayed Al Nahyan Endurance Cup for 100km</p>
<p>2<sup>nd</sup> place in <em>Abu Dhabi Festival</em> Endurance Ride for 100km</p>
<p>2<sup>nd</sup> place in President&rsquo;s Cup of UAE</p>
<p>2<sup>nd</sup> in Al Wathba Cup for 100km</p>
<p>2<sup>nd</sup> in Sheikh Mohmmed bin Zayed al Nahyan Festival</p>
<p><em>3<sup>rd</sup> place in 120km sakhir Bahrain</em> <em>International Trophy 2020</em></p>
<p><em>&nbsp;</em></p>
<p><em>#AmsiWomanofSubstance #WeLoveOurAlumni #SupportAmsiAlumni</em></p>',
                'date' => '2021-03-17',
                'photo' => 'img/news/masa.jpg',
                'thumbnail' => 'img/news/masa-thumb.jpg',
            ],
            [
                'title' => 'Nada Badran, Class of 2004 AMB. ',
                'content' => '<p>Nada Badran, Class of 2004 AMB.&nbsp;</p>
<p>Nada is strategy professional turned storyteller. She founded a tour business called Wander With Nada that offer guests cultural experiences in the UAE which has been home for over 30 years. She started the company with the mission to offer the antidote to on-the-grid travel &ndash; unique, candid, unscripted moments that happen when you truly wander in the UAE&rsquo;s less glamorous corners. Her guests are the culturally curious who want to get under the skin of their destination and seek something different than the usual prescribed tour.<br /> <br /> Nada is licensed Dubai and Abu Dhabi Guide and host tours in both emirates.</p>
<p>She is also the host of a travel show, Tajawal Ma&rsquo;a Nada, on CNN Arabic where she takes viewers through tales of her global adventures and share her two fils on travel.</p>
<p>&nbsp;</p>
<p><strong>Links:&nbsp;</strong></p>
<p>Website: <a href="http://www.wanderwithnada.com">www.wanderwithnada.com</a></p>
<p><br /> Socials:</p>
<p>Facebook:&nbsp; <a href="https://www.facebook.com/wanderwithnada">https://www.facebook.com/wanderwithnada &nbsp; </a></p>
<p>Instagram:&nbsp;<a href="https://www.instagram.com/wanderwithnada/">https://www.instagram.com/wanderwithnada/</a></p>
<p>Twitter:&nbsp;<a href="https://twitter.com/wanderwithnada">https://twitter.com/wanderwithnada</a></p>
<p>&nbsp;</p>',
                'date' => '2021-03-17',
                'photo' => 'img/news/nada.jpg',
                'thumbnail' => 'img/news/nada-thumb.jpg',
            ],
            [
                'title' => 'Talal Nasalla AMB class of 1999',
                'content' => '<p>Talal Nasalla AMB class of 1999</p>
<p>We would like to congratulate Talal Abdin Nasrallah on his new role as as the company&rsquo;s Chief Executive Officer at Gulf Craft.</p>
<p>Talal joined Gulf Craft in late 2020 as Chief Strategy Officer. His mission will be to implement the company&rsquo;s new global growth strategy with a focus on strengthening&nbsp;its continued development and capabilities.</p>
<p>He had previously been director of private banking at Barclays plc, and also worked as director at Credit Suisse AG. He was also director of the Executive Office at Shuaa Capital.</p>
<p>Nasralla said: &ldquo;Gulf Craft pioneered professional yacht and boat building in the UAE and is today one</p>
<p>of the world&rsquo;s first and only fully integrated production facilities. Operating from its hubs in the UAE and the Maldives, the company&rsquo;s build and design quality and performance have earned the admiration of the global yachting industry. With the deep experience and insights of our Chairman and the board guiding us, we will leverage Gulf Craft&rsquo;s competencies as a proud &lsquo;Made in UAE&rsquo; company delivering world-class yachts to the world.&rdquo;&nbsp;&nbsp;</p>
<p>Talal has a deep understanding of the UAE market and has worked for some of the key players in the banking sector. He was previously Director of Private Banking at Barclays PLC, overseeing and managing high net worth clients&rsquo; assets, having also worked as Director at Credit Suisse AG. He was also a Director of the Executive Office at Shuaa Capital PSC, playing a key role in fund raising and managing large-scale projects in the hospitality and real estate sectors. In addition, Talal was a Board Member of Gulf Finance Corporation and Shuaa Capital Saudi Arabia. He started his career as a Business Development Officer at DMCC&rsquo;s Dubai Diamond Exchange and then as Business Development Manager at the Emirates Credit Information Company (emCredit).&nbsp;</p>',
                'date' => '2021-03-17',
                'photo' => 'img/news/talak.jpg',
                'thumbnail' => 'img/news/talal-thumb.jpg',
            ],
            [
                'title' => 'Tarek Berri, Class of 2002 AMG ',
                'content' => '<p>Tarek Berri, Class of 2002 AMG</p>
<p>Tarek Berri founder of Tarek Berri Architects &amp; Handdis. HANDDIS began in the aid of the automotive industry aiming to change the methods of car parts disposal. HANDDIS visit on a daily basis the automotive junk yards "scraps" all around the United Arab Emirates aiming to save and recycle as much parts as possible to create a unique and one of a kind lighting elements. Each part is carefully handpicked and well restored.</p>',
                'date' => '2021-03-17',
                'photo' => 'img/news/tarek.jpg',
                'thumbnail' => 'img/news/tarek-thumb.jpg',
            ],
        ];

        foreach ($data as $item)
        {
            $item['slug'] = $this->generateSlug($item['title']);
            $item['event_type'] = 'news';

            Article::create($item);
        }



    }
}
