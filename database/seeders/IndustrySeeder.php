<?php

namespace Database\Seeders;

use App\Models\Industry;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;

class IndustrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
                0 => [
                    'industry' => [
                        'industryId' => '1',
                        'industryName' => 'Aerospace and Defense',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '1_1',
                            'industryName' => 'Aerospace Products and Parts',
                        ],
                        1 => [
                            'industryId' => '1_2',
                            'industryName' => 'Aerospace Research and Development',
                        ],
                        2 => [
                            'industryId' => '1_3',
                            'industryName' => 'Aircraft Manufacturing',
                        ],
                        3 => [
                            'industryId' => '1_4',
                            'industryName' => 'Military Vehicles Manufacturing',
                        ],
                        4 => [
                            'industryId' => '1_5',
                            'industryName' => 'Ordnance, Missiles, Weaponry and Related',
                        ],
                        5 => [
                            'industryId' => '1_6',
                            'industryName' => 'Space Vehicles, Satellites and Related',
                        ],
                    ],
                ],
                1 => [
                    'industry' => [
                        'industryId' => '2',
                        'industryName' => 'Agriculture and Forestry',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '2_1',
                            'industryName' => 'Agricultural Machinery and Equipment',
                        ],
                        1 => [
                            'industryId' => '2_10',
                            'industryName' => 'Gardening Supplies',
                        ],
                        2 => [
                            'industryId' => '2_11',
                            'industryName' => 'Grains Farming',
                        ],
                        3 => [
                            'industryId' => '2_12',
                            'industryName' => 'Horticulture',
                        ],
                        4 => [
                            'industryId' => '2_13',
                            'industryName' => 'Irrigation and Drainage Districts',
                        ],
                        5 => [
                            'industryId' => '2_14',
                            'industryName' => 'Livestock and Husbandry',
                        ],
                        6 => [
                            'industryId' => '2_15',
                            'industryName' => 'Oilseeds Farming',
                        ],
                        7 => [
                            'industryId' => '2_16',
                            'industryName' => 'Pulses and Legume Farming',
                        ],
                        8 => [
                            'industryId' => '2_17',
                            'industryName' => 'Sericulture and Beekeeping',
                        ],
                        9 => [
                            'industryId' => '2_2',
                            'industryName' => 'Agricultural Product Distribution',
                        ],
                        10 => [
                            'industryId' => '2_3',
                            'industryName' => 'Agriculture Services',
                        ],
                        11 => [
                            'industryId' => '2_4',
                            'industryName' => 'Coffee, Tea and Cocoa Farming',
                        ],
                        12 => [
                            'industryId' => '2_5',
                            'industryName' => 'Cotton and Textile Farming',
                        ],
                        13 => [
                            'industryId' => '2_6',
                            'industryName' => 'Farming Materials and Supplies',
                        ],
                        14 => [
                            'industryId' => '2_7',
                            'industryName' => 'Fishing and Aquaculture',
                        ],
                        15 => [
                            'industryId' => '2_8',
                            'industryName' => 'Forestry and Logging',
                        ],
                        16 => [
                            'industryId' => '2_9',
                            'industryName' => 'Fruits and Vegetables Farming',
                        ],
                    ],
                ],
                2 => [
                    'industry' => [
                        'industryId' => '3',
                        'industryName' => 'Automotive',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '3_1',
                            'industryName' => 'Automobile Parts Manufacturing',
                        ],
                        1 => [
                            'industryId' => '3_2',
                            'industryName' => 'Motor Vehicle Manufacturing',
                        ],
                        2 => [
                            'industryId' => '3_3',
                            'industryName' => 'Motor Vehicle Parts Suppliers',
                        ],
                        3 => [
                            'industryId' => '3_4',
                            'industryName' => 'Motor Vehicle Repair and Servicing',
                        ],
                        4 => [
                            'industryId' => '3_5',
                            'industryName' => 'Truck and Bus Parts Manufacturing',
                        ],
                        5 => [
                            'industryId' => '3_6',
                            'industryName' => 'Truck, Bus and Other Vehicle Manufacturing',
                        ],
                    ],
                ],
                3 => [
                    'industry' => [
                        'industryId' => '4',
                        'industryName' => 'Banks',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '4_1',
                            'industryName' => 'Automated Teller Machine Operators',
                        ],
                        1 => [
                            'industryId' => '4_10',
                            'industryName' => 'Offshore Banks',
                        ],
                        2 => [
                            'industryId' => '4_11',
                            'industryName' => 'Regional Banks',
                        ],
                        3 => [
                            'industryId' => '4_12',
                            'industryName' => 'Savings Institutions',
                        ],
                        4 => [
                            'industryId' => '4_13',
                            'industryName' => 'Short-Term Business Loans and Credit',
                        ],
                        5 => [
                            'industryId' => '4_14',
                            'industryName' => 'Trust, Fiduciary and Custody Activities',
                        ],
                        6 => [
                            'industryId' => '4_2',
                            'industryName' => 'Banking Transaction Processing',
                        ],
                        7 => [
                            'industryId' => '4_3',
                            'industryName' => 'Central Banks',
                        ],
                        8 => [
                            'industryId' => '4_4',
                            'industryName' => 'Commercial Banking ',
                        ],
                        9 => [
                            'industryId' => '4_5',
                            'industryName' => 'Credit Agencies',
                        ],
                        10 => [
                            'industryId' => '4_6',
                            'industryName' => 'Credit Unions',
                        ],
                        11 => [
                            'industryId' => '4_7',
                            'industryName' => 'Islamic Banks',
                        ],
                        12 => [
                            'industryId' => '4_8',
                            'industryName' => 'Landesbanken',
                        ],
                        13 => [
                            'industryId' => '4_9',
                            'industryName' => 'Mortgage Banking',
                        ],
                    ],
                ],
                4 => [
                    'industry' => [
                        'industryId' => '5',
                        'industryName' => 'Chemicals',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '5_1',
                            'industryName' => 'Agricultural Chemicals',
                        ],
                        1 => [
                            'industryId' => '5_10',
                            'industryName' => 'Polymers and Films',
                        ],
                        2 => [
                            'industryId' => '5_11',
                            'industryName' => 'Sealants and Adhesives',
                        ],
                        3 => [
                            'industryId' => '5_12',
                            'industryName' => 'Specialty Chemicals',
                        ],
                        4 => [
                            'industryId' => '5_2',
                            'industryName' => 'Commodity Chemicals',
                        ],
                        5 => [
                            'industryId' => '5_3',
                            'industryName' => 'Diversified Chemicals',
                        ],
                        6 => [
                            'industryId' => '5_4',
                            'industryName' => 'Explosives and Petrochemicals',
                        ],
                        7 => [
                            'industryId' => '5_5',
                            'industryName' => 'Fine Chemicals',
                        ],
                        8 => [
                            'industryId' => '5_6',
                            'industryName' => 'Industrial Fluid Manufacturing',
                        ],
                        9 => [
                            'industryId' => '5_7',
                            'industryName' => 'Medicinal Chemicals and Botanicals',
                        ],
                        10 => [
                            'industryId' => '5_8',
                            'industryName' => 'Paints, Dyes, Varnishes and Lacquers',
                        ],
                        11 => [
                            'industryId' => '5_9',
                            'industryName' => 'Plastic and Fiber Manufacturing',
                        ],
                    ],
                ],
                5 => [
                    'industry' => [
                        'industryId' => '6',
                        'industryName' => 'Civic, Non-Profit and Membership Groups',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '6_1',
                            'industryName' => 'Business Associations',
                        ],
                        1 => [
                            'industryId' => '6_10',
                            'industryName' => 'Social Services Institutions',
                        ],
                        2 => [
                            'industryId' => '6_2',
                            'industryName' => 'Charities',
                        ],
                        3 => [
                            'industryId' => '6_3',
                            'industryName' => 'Environmental and Wildlife Organizations',
                        ],
                        4 => [
                            'industryId' => '6_4',
                            'industryName' => 'Foundations',
                        ],
                        5 => [
                            'industryId' => '6_5',
                            'industryName' => 'Humanitarian and Emergency Relief',
                        ],
                        6 => [
                            'industryId' => '6_6',
                            'industryName' => 'Labor Unions',
                        ],
                        7 => [
                            'industryId' => '6_7',
                            'industryName' => 'Political Organizations',
                        ],
                        8 => [
                            'industryId' => '6_8',
                            'industryName' => 'Public Policy Research and Advocacy',
                        ],
                        9 => [
                            'industryId' => '6_9',
                            'industryName' => 'Religious Organizations',
                        ],
                    ],
                ],
                6 => [
                    'industry' => [
                        'industryId' => '7',
                        'industryName' => 'Computer Hardware',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '7_1',
                            'industryName' => 'ATMs and Self-Service Terminals',
                        ],
                        1 => [
                            'industryId' => '7_10',
                            'industryName' => 'Optical, Magnetic and Mass Storage',
                        ],
                        2 => [
                            'industryId' => '7_11',
                            'industryName' => 'Peripherals, Computers and Accessories',
                        ],
                        3 => [
                            'industryId' => '7_12',
                            'industryName' => 'Personal Storage Drives and Media',
                        ],
                        4 => [
                            'industryId' => '7_13',
                            'industryName' => 'POS and Electronic Retail Systems',
                        ],
                        5 => [
                            'industryId' => '7_14',
                            'industryName' => 'Printing and Imaging Equipment',
                        ],
                        6 => [
                            'industryId' => '7_15',
                            'industryName' => 'Routing and Switching',
                        ],
                        7 => [
                            'industryId' => '7_16',
                            'industryName' => 'Semiconductor Equipment and Testing',
                        ],
                        8 => [
                            'industryId' => '7_17',
                            'industryName' => 'Semiconductors',
                        ],
                        9 => [
                            'industryId' => '7_18',
                            'industryName' => 'Servers and Mainframes',
                        ],
                        10 => [
                            'industryId' => '7_19',
                            'industryName' => 'Storage Networking',
                        ],
                        11 => [
                            'industryId' => '7_2',
                            'industryName' => 'Computer Communications Equipment',
                        ],
                        12 => [
                            'industryId' => '7_20',
                            'industryName' => 'Wireless Networking',
                        ],
                        13 => [
                            'industryId' => '7_3',
                            'industryName' => 'Computer Display Units and Projectors',
                        ],
                        14 => [
                            'industryId' => '7_4',
                            'industryName' => 'Computer Input Devices and Speakers',
                        ],
                        15 => [
                            'industryId' => '7_5',
                            'industryName' => 'Computer Networking Equipment',
                        ],
                        16 => [
                            'industryId' => '7_6',
                            'industryName' => 'Computer Storage Devices',
                        ],
                        17 => [
                            'industryId' => '7_7',
                            'industryName' => 'Network Security Devices',
                        ],
                        18 => [
                            'industryId' => '7_8',
                            'industryName' => 'Office Equipment',
                        ],
                        19 => [
                            'industryId' => '7_9',
                            'industryName' => 'Office Supplies',
                        ],
                    ],
                ],
                7 => [
                    'industry' => [
                        'industryId' => '8',
                        'industryName' => 'Computer Software',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '8_1',
                            'industryName' => 'Accounting and Tax Software',
                        ],
                        1 => [
                            'industryId' => '8_10',
                            'industryName' => 'Casino Management Software',
                        ],
                        2 => [
                            'industryId' => '8_11',
                            'industryName' => 'Catalog Management Software',
                        ],
                        3 => [
                            'industryId' => '8_12',
                            'industryName' => 'Channel Partner Management Software',
                        ],
                        4 => [
                            'industryId' => '8_13',
                            'industryName' => 'Collaborative Software',
                        ],
                        5 => [
                            'industryId' => '8_14',
                            'industryName' => 'Compliance and Governance Software',
                        ],
                        6 => [
                            'industryId' => '8_15',
                            'industryName' => 'Computer-Aided Manufacturing Software',
                        ],
                        7 => [
                            'industryId' => '8_16',
                            'industryName' => 'Construction and Architecture Software',
                        ],
                        8 => [
                            'industryId' => '8_17',
                            'industryName' => 'Content Management Software',
                        ],
                        9 => [
                            'industryId' => '8_18',
                            'industryName' => 'Customer Relationship Management',
                        ],
                        10 => [
                            'industryId' => '8_19',
                            'industryName' => 'Data Warehousing',
                        ],
                        11 => [
                            'industryId' => '8_2',
                            'industryName' => 'Advertising Industry Software',
                        ],
                        12 => [
                            'industryId' => '8_20',
                            'industryName' => 'Database Management Software',
                        ],
                        13 => [
                            'industryId' => '8_21',
                            'industryName' => 'Defense and Military Software',
                        ],
                        14 => [
                            'industryId' => '8_22',
                            'industryName' => 'Desktop Publishing Software',
                        ],
                        15 => [
                            'industryId' => '8_23',
                            'industryName' => 'Development Tools and Utilities Software',
                        ],
                        16 => [
                            'industryId' => '8_24',
                            'industryName' => 'Distribution Software',
                        ],
                        17 => [
                            'industryId' => '8_25',
                            'industryName' => 'Document Management Software',
                        ],
                        18 => [
                            'industryId' => '8_26',
                            'industryName' => 'E-Commerce Software',
                        ],
                        19 => [
                            'industryId' => '8_27',
                            'industryName' => 'Education and Training Software',
                        ],
                        20 => [
                            'industryId' => '8_28',
                            'industryName' => 'Electronics Industry Software',
                        ],
                        21 => [
                            'industryId' => '8_29',
                            'industryName' => 'Engineering, Scientific and CAD Software',
                        ],
                        22 => [
                            'industryId' => '8_3',
                            'industryName' => 'Agriculture Industry Software',
                        ],
                        23 => [
                            'industryId' => '8_30',
                            'industryName' => 'Enterprise Application Integration (EAI)',
                        ],
                        24 => [
                            'industryId' => '8_31',
                            'industryName' => 'Enterprise Resource Planning Software',
                        ],
                        25 => [
                            'industryId' => '8_32',
                            'industryName' => 'Event Planning Industry Software',
                        ],
                        26 => [
                            'industryId' => '8_33',
                            'industryName' => 'File Management Software',
                        ],
                        27 => [
                            'industryId' => '8_34',
                            'industryName' => 'Financial Services Software',
                        ],
                        28 => [
                            'industryId' => '8_35',
                            'industryName' => 'Food and Beverage Industry Software',
                        ],
                        29 => [
                            'industryId' => '8_36',
                            'industryName' => 'Health Care Management Software',
                        ],
                        30 => [
                            'industryId' => '8_37',
                            'industryName' => 'Hotel Management Industry Software',
                        ],
                        31 => [
                            'industryId' => '8_38',
                            'industryName' => 'Human Resources Software',
                        ],
                        32 => [
                            'industryId' => '8_39',
                            'industryName' => 'Insurance Industry Software',
                        ],
                        33 => [
                            'industryId' => '8_4',
                            'industryName' => 'Application Service Providers (ASPs)',
                        ],
                        34 => [
                            'industryId' => '8_40',
                            'industryName' => 'Law Enforcement Industry Software',
                        ],
                        35 => [
                            'industryId' => '8_41',
                            'industryName' => 'Legal Industry Software',
                        ],
                        36 => [
                            'industryId' => '8_42',
                            'industryName' => 'Management Consulting Software',
                        ],
                        37 => [
                            'industryId' => '8_43',
                            'industryName' => 'Manufacturing and Industrial Software',
                        ],
                        38 => [
                            'industryId' => '8_44',
                            'industryName' => 'Marketing Automation Software',
                        ],
                        39 => [
                            'industryId' => '8_45',
                            'industryName' => 'Message, Conference and Communications',
                        ],
                        40 => [
                            'industryId' => '8_46',
                            'industryName' => 'Mobile Application Software',
                        ],
                        41 => [
                            'industryId' => '8_47',
                            'industryName' => 'Multimedia and Graphics Software',
                        ],
                        42 => [
                            'industryId' => '8_48',
                            'industryName' => 'Networking and Connectivity Software',
                        ],
                        43 => [
                            'industryId' => '8_49',
                            'industryName' => 'Operating System Software',
                        ],
                        44 => [
                            'industryId' => '8_5',
                            'industryName' => 'Automotive Industry Software',
                        ],
                        45 => [
                            'industryId' => '8_50',
                            'industryName' => 'Order Management Software',
                        ],
                        46 => [
                            'industryId' => '8_51',
                            'industryName' => 'Pharma and Biotech Software',
                        ],
                        47 => [
                            'industryId' => '8_52',
                            'industryName' => 'Procurement Software',
                        ],
                        48 => [
                            'industryId' => '8_53',
                            'industryName' => 'Programming and Data Processing Services',
                        ],
                        49 => [
                            'industryId' => '8_54',
                            'industryName' => 'Project Management Software',
                        ],
                        50 => [
                            'industryId' => '8_55',
                            'industryName' => 'Purchasing Software',
                        ],
                        51 => [
                            'industryId' => '8_56',
                            'industryName' => 'Quality Assurance Software',
                        ],
                        52 => [
                            'industryId' => '8_57',
                            'industryName' => 'Real Estate Industry Software',
                        ],
                        53 => [
                            'industryId' => '8_58',
                            'industryName' => 'Restaurant Industry Software',
                        ],
                        54 => [
                            'industryId' => '8_59',
                            'industryName' => 'Retail Management Software',
                        ],
                        55 => [
                            'industryId' => '8_6',
                            'industryName' => 'Banking Software',
                        ],
                        56 => [
                            'industryId' => '8_60',
                            'industryName' => 'Sales Force Automation Software',
                        ],
                        57 => [
                            'industryId' => '8_61',
                            'industryName' => 'Sales Intelligence Software',
                        ],
                        58 => [
                            'industryId' => '8_62',
                            'industryName' => 'Security Software',
                        ],
                        59 => [
                            'industryId' => '8_63',
                            'industryName' => 'Service Industry Software',
                        ],
                        60 => [
                            'industryId' => '8_64',
                            'industryName' => 'Smart Home Software',
                        ],
                        61 => [
                            'industryId' => '8_65',
                            'industryName' => 'Storage and Systems Management Software',
                        ],
                        62 => [
                            'industryId' => '8_66',
                            'industryName' => 'Supply Chain and Logistics Software',
                        ],
                        63 => [
                            'industryId' => '8_67',
                            'industryName' => 'Telecommunication Software',
                        ],
                        64 => [
                            'industryId' => '8_68',
                            'industryName' => 'Textiles Industry Software',
                        ],
                        65 => [
                            'industryId' => '8_69',
                            'industryName' => 'Tourism Industry Software',
                        ],
                        66 => [
                            'industryId' => '8_7',
                            'industryName' => 'Billing and Service Provision Software',
                        ],
                        67 => [
                            'industryId' => '8_70',
                            'industryName' => 'Trading and Order Management Software',
                        ],
                        68 => [
                            'industryId' => '8_71',
                            'industryName' => 'Transportation Industry Software',
                        ],
                        69 => [
                            'industryId' => '8_72',
                            'industryName' => 'Warehousing Software',
                        ],
                        70 => [
                            'industryId' => '8_73',
                            'industryName' => 'Wireless Communication Software',
                        ],
                        71 => [
                            'industryId' => '8_8',
                            'industryName' => 'Budgeting and Forecasting Software',
                        ],
                        72 => [
                            'industryId' => '8_9',
                            'industryName' => 'Business Intelligence Software',
                        ],
                    ],
                ],
                8 => [
                    'industry' => [
                        'industryId' => '9',
                        'industryName' => 'Construction and Building Materials',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '9_1',
                            'industryName' => 'Aggregates, Concrete and Cement',
                        ],
                        1 => [
                            'industryId' => '9_10',
                            'industryName' => 'Construction Materials',
                        ],
                        2 => [
                            'industryId' => '9_11',
                            'industryName' => 'Electrical Work',
                        ],
                        3 => [
                            'industryId' => '9_12',
                            'industryName' => 'Engineering Services',
                        ],
                        4 => [
                            'industryId' => '9_13',
                            'industryName' => 'General Contractors - Non-Residential',
                        ],
                        5 => [
                            'industryId' => '9_14',
                            'industryName' => 'General Contractors - Residential',
                        ],
                        6 => [
                            'industryId' => '9_15',
                            'industryName' => 'Hardware Wholesalers',
                        ],
                        7 => [
                            'industryId' => '9_16',
                            'industryName' => 'Heavy Construction',
                        ],
                        8 => [
                            'industryId' => '9_17',
                            'industryName' => 'Plumbing and HVAC Equipment ',
                        ],
                        9 => [
                            'industryId' => '9_18',
                            'industryName' => 'Plywood, Veneer and Particle Board',
                        ],
                        10 => [
                            'industryId' => '9_19',
                            'industryName' => 'Prefabricated Buildings',
                        ],
                        11 => [
                            'industryId' => '9_2',
                            'industryName' => 'Apartment and Condominium Construction',
                        ],
                        12 => [
                            'industryId' => '9_20',
                            'industryName' => 'Sawmills and Other Mill Operations',
                        ],
                        13 => [
                            'industryId' => '9_21',
                            'industryName' => 'Single-Family Housing Builders',
                        ],
                        14 => [
                            'industryId' => '9_22',
                            'industryName' => 'Specialty Construction',
                        ],
                        15 => [
                            'industryId' => '9_23',
                            'industryName' => 'Water, Sewer and Power Line',
                        ],
                        16 => [
                            'industryId' => '9_24',
                            'industryName' => 'Wood Production and Timber Operations',
                        ],
                        17 => [
                            'industryId' => '9_25',
                            'industryName' => 'Wood Products',
                        ],
                        18 => [
                            'industryId' => '9_26',
                            'industryName' => 'Wood Window and Door Manufacturing',
                        ],
                        19 => [
                            'industryId' => '9_3',
                            'industryName' => 'Architecture and Engineering Services',
                        ],
                        20 => [
                            'industryId' => '9_4',
                            'industryName' => 'Asphalt and Roofing Materials',
                        ],
                        21 => [
                            'industryId' => '9_5',
                            'industryName' => 'Carpentry and Floor Work',
                        ],
                        22 => [
                            'industryId' => '9_6',
                            'industryName' => 'Ceramic, Tile, Roofing and Clay Products',
                        ],
                        23 => [
                            'industryId' => '9_7',
                            'industryName' => 'Construction and Engineering',
                        ],
                        24 => [
                            'industryId' => '9_8',
                            'industryName' => 'Construction and Mining Equipment Sales',
                        ],
                        25 => [
                            'industryId' => '9_9',
                            'industryName' => 'Construction Equipment',
                        ],
                    ],
                ],
                9 => [
                    'industry' => [
                        'industryId' => '10',
                        'industryName' => 'Consumer Products',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '10_1',
                            'industryName' => 'Accessories',
                        ],
                        1 => [
                            'industryId' => '10_10',
                            'industryName' => 'Cutlery and Handtools',
                        ],
                        2 => [
                            'industryId' => '10_11',
                            'industryName' => 'Dinnerware, Cookware and Cutlery',
                        ],
                        3 => [
                            'industryId' => '10_12',
                            'industryName' => 'Electrical Housewares',
                        ],
                        4 => [
                            'industryId' => '10_13',
                            'industryName' => 'Eyewear Manufacturers',
                        ],
                        5 => [
                            'industryId' => '10_14',
                            'industryName' => 'Footwear',
                        ],
                        6 => [
                            'industryId' => '10_15',
                            'industryName' => 'Garden Equipment, Mowers and Tractors ',
                        ],
                        7 => [
                            'industryId' => '10_16',
                            'industryName' => 'Gold and Silverware',
                        ],
                        8 => [
                            'industryId' => '10_17',
                            'industryName' => 'Hand and Power Tools',
                        ],
                        9 => [
                            'industryId' => '10_18',
                            'industryName' => 'Home Furnishings',
                        ],
                        10 => [
                            'industryId' => '10_19',
                            'industryName' => 'Household Furniture',
                        ],
                        11 => [
                            'industryId' => '10_2',
                            'industryName' => 'Apparel',
                        ],
                        12 => [
                            'industryId' => '10_20',
                            'industryName' => 'Household Products',
                        ],
                        13 => [
                            'industryId' => '10_21',
                            'industryName' => 'Housewares and Home Storage Products',
                        ],
                        14 => [
                            'industryId' => '10_22',
                            'industryName' => 'Jewelry and Gemstones',
                        ],
                        15 => [
                            'industryId' => '10_23',
                            'industryName' => 'Knitting Mills',
                        ],
                        16 => [
                            'industryId' => '10_24',
                            'industryName' => 'Lighting and Wiring Equipment',
                        ],
                        17 => [
                            'industryId' => '10_25',
                            'industryName' => 'Linens',
                        ],
                        18 => [
                            'industryId' => '10_26',
                            'industryName' => 'Major Appliances',
                        ],
                        19 => [
                            'industryId' => '10_27',
                            'industryName' => 'Man-Made Fiber and Silk Mills',
                        ],
                        20 => [
                            'industryId' => '10_28',
                            'industryName' => 'Mattresses and Bed Manufacturers',
                        ],
                        21 => [
                            'industryId' => '10_29',
                            'industryName' => 'Men\'s Clothing',
                        ],
                        22 => [
                            'industryId' => '10_3',
                            'industryName' => 'Baby Supplies and Accessories',
                        ],
                        23 => [
                            'industryId' => '10_30',
                            'industryName' => 'Miscellaneous Durable Goods',
                        ],
                        24 => [
                            'industryId' => '10_31',
                            'industryName' => 'Miscellaneous Non-Durable Goods',
                        ],
                        25 => [
                            'industryId' => '10_32',
                            'industryName' => 'Office Furniture and Fixtures',
                        ],
                        26 => [
                            'industryId' => '10_33',
                            'industryName' => 'Paper and Paper Products',
                        ],
                        27 => [
                            'industryId' => '10_34',
                            'industryName' => 'Perfumes, Cosmetics and Toiletries',
                        ],
                        28 => [
                            'industryId' => '10_35',
                            'industryName' => 'Pet Products',
                        ],
                        29 => [
                            'industryId' => '10_36',
                            'industryName' => 'Photographic Equipment and Supplies',
                        ],
                        30 => [
                            'industryId' => '10_37',
                            'industryName' => 'Pottery',
                        ],
                        31 => [
                            'industryId' => '10_38',
                            'industryName' => 'Shelving, Partitions and Fixtures',
                        ],
                        32 => [
                            'industryId' => '10_39',
                            'industryName' => 'Soaps and Detergents',
                        ],
                        33 => [
                            'industryId' => '10_4',
                            'industryName' => 'Biological Products',
                        ],
                        34 => [
                            'industryId' => '10_40',
                            'industryName' => 'Specialty Cleaning Products',
                        ],
                        35 => [
                            'industryId' => '10_41',
                            'industryName' => 'Textile Products',
                        ],
                        36 => [
                            'industryId' => '10_42',
                            'industryName' => 'Textiles and Leather Goods',
                        ],
                        37 => [
                            'industryId' => '10_43',
                            'industryName' => 'Tobacco Products and Distributors',
                        ],
                        38 => [
                            'industryId' => '10_44',
                            'industryName' => 'Watches, Clocks and Clockwork Devices',
                        ],
                        39 => [
                            'industryId' => '10_45',
                            'industryName' => 'Window Coverings and Wall Coverings',
                        ],
                        40 => [
                            'industryId' => '10_46',
                            'industryName' => 'Women\'s Clothing',
                        ],
                        41 => [
                            'industryId' => '10_47',
                            'industryName' => 'Wood Furniture',
                        ],
                        42 => [
                            'industryId' => '10_5',
                            'industryName' => 'Carpets, Rugs and Floor Coverings',
                        ],
                        43 => [
                            'industryId' => '10_6',
                            'industryName' => 'Children\'s Clothing',
                        ],
                        44 => [
                            'industryId' => '10_7',
                            'industryName' => 'Collectibles and Giftware Products',
                        ],
                        45 => [
                            'industryId' => '10_8',
                            'industryName' => 'Costume Jewelry',
                        ],
                        46 => [
                            'industryId' => '10_9',
                            'industryName' => 'Cotton Fabric Mills',
                        ],
                    ],
                ],
                10 => [
                    'industry' => [
                        'industryId' => '11',
                        'industryName' => 'Consumer Services',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '11_1',
                            'industryName' => 'Child Care Services',
                        ],
                        1 => [
                            'industryId' => '11_10',
                            'industryName' => 'Optical Services',
                        ],
                        2 => [
                            'industryId' => '11_11',
                            'industryName' => 'Personal Products',
                        ],
                        3 => [
                            'industryId' => '11_12',
                            'industryName' => 'Personal Services',
                        ],
                        4 => [
                            'industryId' => '11_13',
                            'industryName' => 'Photographic Services',
                        ],
                        5 => [
                            'industryId' => '11_14',
                            'industryName' => 'Spas and Fitness Services',
                        ],
                        6 => [
                            'industryId' => '11_15',
                            'industryName' => 'Taxi and Limousine Services',
                        ],
                        7 => [
                            'industryId' => '11_16',
                            'industryName' => 'Travel Agencies and Services',
                        ],
                        8 => [
                            'industryId' => '11_17',
                            'industryName' => 'Veterinary Care',
                        ],
                        9 => [
                            'industryId' => '11_18',
                            'industryName' => 'Weight and Health Management',
                        ],
                        10 => [
                            'industryId' => '11_2',
                            'industryName' => 'Coffee and Water Beverage Services',
                        ],
                        11 => [
                            'industryId' => '11_3',
                            'industryName' => 'Courier, Messenger and Delivery Services',
                        ],
                        12 => [
                            'industryId' => '11_4',
                            'industryName' => 'Death Care Products and Services',
                        ],
                        13 => [
                            'industryId' => '11_5',
                            'industryName' => 'Hair Salons and Beauty Treatments ',
                        ],
                        14 => [
                            'industryId' => '11_6',
                            'industryName' => 'Laundry and Dry Cleaning Services',
                        ],
                        15 => [
                            'industryId' => '11_7',
                            'industryName' => 'Motor Vehicle Parking and Garages',
                        ],
                        16 => [
                            'industryId' => '11_8',
                            'industryName' => 'Motor Vehicle Rental and Leasing',
                        ],
                        17 => [
                            'industryId' => '11_9',
                            'industryName' => 'Moving Services',
                        ],
                    ],
                ],
                11 => [
                    'industry' => [
                        'industryId' => '12',
                        'industryName' => 'Corporate Services',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '12_1',
                            'industryName' => 'Advertising Agencies',
                        ],
                        1 => [
                            'industryId' => '12_10',
                            'industryName' => 'Computer Processing and Data Services',
                        ],
                        2 => [
                            'industryId' => '12_11',
                            'industryName' => 'Consumer Credit Reporting',
                        ],
                        3 => [
                            'industryId' => '12_12',
                            'industryName' => 'Detective and Other Security Services',
                        ],
                        4 => [
                            'industryId' => '12_13',
                            'industryName' => 'Direct Marketing',
                        ],
                        5 => [
                            'industryId' => '12_14',
                            'industryName' => 'Executive Search',
                        ],
                        6 => [
                            'industryId' => '12_15',
                            'industryName' => 'Facilities Support',
                        ],
                        7 => [
                            'industryId' => '12_16',
                            'industryName' => 'General Rental Center',
                        ],
                        8 => [
                            'industryId' => '12_17',
                            'industryName' => 'Human Resources and Staffing',
                        ],
                        9 => [
                            'industryId' => '12_18',
                            'industryName' => 'Integrated Computer Systems Design',
                        ],
                        10 => [
                            'industryId' => '12_19',
                            'industryName' => 'IT Services and Consulting',
                        ],
                        11 => [
                            'industryId' => '12_2',
                            'industryName' => 'Arbitration Services',
                        ],
                        12 => [
                            'industryId' => '12_20',
                            'industryName' => 'Law Firms',
                        ],
                        13 => [
                            'industryId' => '12_21',
                            'industryName' => 'Legal Services',
                        ],
                        14 => [
                            'industryId' => '12_22',
                            'industryName' => 'Mailing and Commercial Art Services',
                        ],
                        15 => [
                            'industryId' => '12_23',
                            'industryName' => 'Management Consulting',
                        ],
                        16 => [
                            'industryId' => '12_24',
                            'industryName' => 'Management Services',
                        ],
                        17 => [
                            'industryId' => '12_25',
                            'industryName' => 'Market Research Services',
                        ],
                        18 => [
                            'industryId' => '12_26',
                            'industryName' => 'Marketing and Advertising',
                        ],
                        19 => [
                            'industryId' => '12_27',
                            'industryName' => 'Media Representatives',
                        ],
                        20 => [
                            'industryId' => '12_28',
                            'industryName' => 'Miscellaneous',
                        ],
                        21 => [
                            'industryId' => '12_29',
                            'industryName' => 'Miscellaneous Computer Related Services',
                        ],
                        22 => [
                            'industryId' => '12_3',
                            'industryName' => 'Artists, Writers and Performers',
                        ],
                        23 => [
                            'industryId' => '12_30',
                            'industryName' => 'Miscellaneous Repair Services',
                        ],
                        24 => [
                            'industryId' => '12_31',
                            'industryName' => 'Miscellaneous Services',
                        ],
                        25 => [
                            'industryId' => '12_32',
                            'industryName' => 'Mobile Application Developers',
                        ],
                        26 => [
                            'industryId' => '12_33',
                            'industryName' => 'Networking Services',
                        ],
                        27 => [
                            'industryId' => '12_34',
                            'industryName' => 'Online Staffing and Recruitment Services',
                        ],
                        28 => [
                            'industryId' => '12_35',
                            'industryName' => 'Other Equipment Rental and Leasing',
                        ],
                        29 => [
                            'industryId' => '12_36',
                            'industryName' => 'Outsourced Human Resources Services',
                        ],
                        30 => [
                            'industryId' => '12_37',
                            'industryName' => 'Photo Labs',
                        ],
                        31 => [
                            'industryId' => '12_38',
                            'industryName' => 'Public Relations',
                        ],
                        32 => [
                            'industryId' => '12_39',
                            'industryName' => 'Records Management Services',
                        ],
                        33 => [
                            'industryId' => '12_4',
                            'industryName' => 'Auctions and Internet Auctions',
                        ],
                        34 => [
                            'industryId' => '12_40',
                            'industryName' => 'Sales Promotion and Targeted Marketing',
                        ],
                        35 => [
                            'industryId' => '12_41',
                            'industryName' => 'Services for the Printing Trade',
                        ],
                        36 => [
                            'industryId' => '12_42',
                            'industryName' => 'Talent and Modeling Agencies',
                        ],
                        37 => [
                            'industryId' => '12_43',
                            'industryName' => 'Technology',
                        ],
                        38 => [
                            'industryId' => '12_44',
                            'industryName' => 'Testing Lab and Scientific Research ',
                        ],
                        39 => [
                            'industryId' => '12_45',
                            'industryName' => 'Trade Show, Event Planning and Support',
                        ],
                        40 => [
                            'industryId' => '12_46',
                            'industryName' => 'Uniform Supplies',
                        ],
                        41 => [
                            'industryId' => '12_5',
                            'industryName' => 'Billboards and Outdoor Advertising',
                        ],
                        42 => [
                            'industryId' => '12_6',
                            'industryName' => 'Business Services',
                        ],
                        43 => [
                            'industryId' => '12_7',
                            'industryName' => 'Cleaning and Facilities Management',
                        ],
                        44 => [
                            'industryId' => '12_8',
                            'industryName' => 'Commercial Printing Services',
                        ],
                        45 => [
                            'industryId' => '12_9',
                            'industryName' => 'Computer Facilities ',
                        ],
                    ],
                ],
                12 => [
                    'industry' => [
                        'industryId' => '13',
                        'industryName' => 'Electronics',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '13_1',
                            'industryName' => 'Appliances and Tools and Housewares',
                        ],
                        1 => [
                            'industryId' => '13_10',
                            'industryName' => 'Magnetic and Optical Recording',
                        ],
                        2 => [
                            'industryId' => '13_11',
                            'industryName' => 'Miscellaneous Electrical Equipment',
                        ],
                        3 => [
                            'industryId' => '13_12',
                            'industryName' => 'Printed Circuit Boards',
                        ],
                        4 => [
                            'industryId' => '13_13',
                            'industryName' => 'Sound and Lighting Equipment',
                        ],
                        5 => [
                            'industryId' => '13_2',
                            'industryName' => 'Consumer Electronics',
                        ],
                        6 => [
                            'industryId' => '13_3',
                            'industryName' => 'Electric Testing Equipment',
                        ],
                        7 => [
                            'industryId' => '13_4',
                            'industryName' => 'Electronic Coils and Transformers',
                        ],
                        8 => [
                            'industryId' => '13_5',
                            'industryName' => 'Electronic Components and Accessories',
                        ],
                        9 => [
                            'industryId' => '13_6',
                            'industryName' => 'Electronic Connectors',
                        ],
                        10 => [
                            'industryId' => '13_7',
                            'industryName' => 'Electronic Gaming Products',
                        ],
                        11 => [
                            'industryId' => '13_8',
                            'industryName' => 'Electronic Parts and Equipment',
                        ],
                        12 => [
                            'industryId' => '13_9',
                            'industryName' => 'Heavy Electrical Equipment',
                        ],
                    ],
                ],
                13 => [
                    'industry' => [
                        'industryId' => '14',
                        'industryName' => 'Energy and Environmental',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '14_1',
                            'industryName' => 'Alternative Energy Sources',
                        ],
                        1 => [
                            'industryId' => '14_10',
                            'industryName' => 'Electricity Transmission',
                        ],
                        2 => [
                            'industryId' => '14_11',
                            'industryName' => 'Electricity, Gas and Sanitation Services',
                        ],
                        3 => [
                            'industryId' => '14_12',
                            'industryName' => 'Energy Equipment and Services',
                        ],
                        4 => [
                            'industryId' => '14_13',
                            'industryName' => 'Environmental Services',
                        ],
                        5 => [
                            'industryId' => '14_14',
                            'industryName' => 'Fossil Fuel Power Generation',
                        ],
                        6 => [
                            'industryId' => '14_15',
                            'industryName' => 'Fuel Oil Dealers',
                        ],
                        7 => [
                            'industryId' => '14_16',
                            'industryName' => 'Gasoline Retailers',
                        ],
                        8 => [
                            'industryId' => '14_17',
                            'industryName' => 'Hazardous Waste Management',
                        ],
                        9 => [
                            'industryId' => '14_18',
                            'industryName' => 'Hydroelectric Power Generation',
                        ],
                        10 => [
                            'industryId' => '14_19',
                            'industryName' => 'Independent/Merchant Power Production',
                        ],
                        11 => [
                            'industryId' => '14_2',
                            'industryName' => 'Cogeneration and Small Power Producers',
                        ],
                        12 => [
                            'industryId' => '14_20',
                            'industryName' => 'Integrated Oil and Gas',
                        ],
                        13 => [
                            'industryId' => '14_21',
                            'industryName' => 'Liquefied Petroleum Gas Dealers',
                        ],
                        14 => [
                            'industryId' => '14_22',
                            'industryName' => 'Natural Gas Gathering and Processing ',
                        ],
                        15 => [
                            'industryId' => '14_23',
                            'industryName' => 'Natural Gas Pipelines',
                        ],
                        16 => [
                            'industryId' => '14_24',
                            'industryName' => 'Natural Gas Transmission',
                        ],
                        17 => [
                            'industryId' => '14_25',
                            'industryName' => 'Natural Gas Utilities',
                        ],
                        18 => [
                            'industryId' => '14_26',
                            'industryName' => 'Nuclear Power Generation',
                        ],
                        19 => [
                            'industryId' => '14_27',
                            'industryName' => 'Oil and Gas - Field Services',
                        ],
                        20 => [
                            'industryId' => '14_28',
                            'industryName' => 'Oil and Gas - Marketing and Refining',
                        ],
                        21 => [
                            'industryId' => '14_29',
                            'industryName' => 'Oil and Gas - Production and Exploration',
                        ],
                        22 => [
                            'industryId' => '14_3',
                            'industryName' => 'Combined Electric and Gas Utilities',
                        ],
                        23 => [
                            'industryId' => '14_30',
                            'industryName' => 'Oil and Gas - Transportation and Storage',
                        ],
                        24 => [
                            'industryId' => '14_31',
                            'industryName' => 'Oil Drilling and Gas Wells',
                        ],
                        25 => [
                            'industryId' => '14_32',
                            'industryName' => 'Oil Related Equipment and Services',
                        ],
                        26 => [
                            'industryId' => '14_33',
                            'industryName' => 'Other Utilities ',
                        ],
                        27 => [
                            'industryId' => '14_34',
                            'industryName' => 'Petroleum Pipelines',
                        ],
                        28 => [
                            'industryId' => '14_35',
                            'industryName' => 'Refined Petroleum Pipelines',
                        ],
                        29 => [
                            'industryId' => '14_36',
                            'industryName' => 'Refuse Systems',
                        ],
                        30 => [
                            'industryId' => '14_37',
                            'industryName' => 'Remediation and Environmental Cleanup ',
                        ],
                        31 => [
                            'industryId' => '14_38',
                            'industryName' => 'Retail Energy Marketing',
                        ],
                        32 => [
                            'industryId' => '14_39',
                            'industryName' => 'Sanitary and Sewage Districts',
                        ],
                        33 => [
                            'industryId' => '14_4',
                            'industryName' => 'Conservation Districts',
                        ],
                        34 => [
                            'industryId' => '14_40',
                            'industryName' => 'Sanitation Services',
                        ],
                        35 => [
                            'industryId' => '14_41',
                            'industryName' => 'Solid Waste Services and Recycling',
                        ],
                        36 => [
                            'industryId' => '14_42',
                            'industryName' => 'Trusts',
                        ],
                        37 => [
                            'industryId' => '14_43',
                            'industryName' => 'Utilities - Multiline',
                        ],
                        38 => [
                            'industryId' => '14_44',
                            'industryName' => 'Waste Management Districts',
                        ],
                        39 => [
                            'industryId' => '14_45',
                            'industryName' => 'Wastewater Treatment',
                        ],
                        40 => [
                            'industryId' => '14_46',
                            'industryName' => 'Water Distribution',
                        ],
                        41 => [
                            'industryId' => '14_47',
                            'industryName' => 'Water Supply and Utilities',
                        ],
                        42 => [
                            'industryId' => '14_48',
                            'industryName' => 'Wholesale Energy Trading and Marketing',
                        ],
                        43 => [
                            'industryId' => '14_49',
                            'industryName' => 'Wholesale Petroleum and Related Products',
                        ],
                        44 => [
                            'industryId' => '14_5',
                            'industryName' => 'Crude Petroleum and Natural Gas',
                        ],
                        45 => [
                            'industryId' => '14_50',
                            'industryName' => 'Wholesale Petroleum Bulk Stations',
                        ],
                        46 => [
                            'industryId' => '14_6',
                            'industryName' => 'Crude Petroleum Pipelines',
                        ],
                        47 => [
                            'industryId' => '14_7',
                            'industryName' => 'Electric Power Distribution',
                        ],
                        48 => [
                            'industryId' => '14_8',
                            'industryName' => 'Electric Power Transmission',
                        ],
                        49 => [
                            'industryId' => '14_9',
                            'industryName' => 'Electric Utilities',
                        ],
                    ],
                ],
                14 => [
                    'industry' => [
                        'industryId' => '15',
                        'industryName' => 'Financial Services',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '15_1',
                            'industryName' => 'Accounting, Tax, Bookkeeping and Payroll',
                        ],
                        1 => [
                            'industryId' => '15_10',
                            'industryName' => 'Currency and Forex Brokers',
                        ],
                        2 => [
                            'industryId' => '15_11',
                            'industryName' => 'Currency, Commodity & Futures Trading',
                        ],
                        3 => [
                            'industryId' => '15_12',
                            'industryName' => 'Diversified Financial Services',
                        ],
                        4 => [
                            'industryId' => '15_13',
                            'industryName' => 'Diversified Lending',
                        ],
                        5 => [
                            'industryId' => '15_14',
                            'industryName' => 'Electronic Funds Transfer',
                        ],
                        6 => [
                            'industryId' => '15_15',
                            'industryName' => 'Electronic Payment Systems',
                        ],
                        7 => [
                            'industryId' => '15_16',
                            'industryName' => 'Energy Exchanges',
                        ],
                        8 => [
                            'industryId' => '15_17',
                            'industryName' => 'Finance Authorities',
                        ],
                        9 => [
                            'industryId' => '15_18',
                            'industryName' => 'Financial Leasing Companies',
                        ],
                        10 => [
                            'industryId' => '15_19',
                            'industryName' => 'Financial Transaction Settlement',
                        ],
                        11 => [
                            'industryId' => '15_2',
                            'industryName' => 'Asset Management',
                        ],
                        12 => [
                            'industryId' => '15_20',
                            'industryName' => 'Forfeiting and Factoring',
                        ],
                        13 => [
                            'industryId' => '15_21',
                            'industryName' => 'Hedge Funds',
                        ],
                        14 => [
                            'industryId' => '15_22',
                            'industryName' => 'Investment Banking',
                        ],
                        15 => [
                            'industryId' => '15_23',
                            'industryName' => 'Investment Management and Fund Operators',
                        ],
                        16 => [
                            'industryId' => '15_24',
                            'industryName' => 'Investment Services and Advice',
                        ],
                        17 => [
                            'industryId' => '15_25',
                            'industryName' => 'Investment Trusts ',
                        ],
                        18 => [
                            'industryId' => '15_26',
                            'industryName' => 'Leveraged Finance',
                        ],
                        19 => [
                            'industryId' => '15_27',
                            'industryName' => 'Market Makers and Trade Clearing',
                        ],
                        20 => [
                            'industryId' => '15_28',
                            'industryName' => 'Miscellaneous Investment Firms',
                        ],
                        21 => [
                            'industryId' => '15_29',
                            'industryName' => 'Mobile Payment Systems',
                        ],
                        22 => [
                            'industryId' => '15_3',
                            'industryName' => 'Blank Check Companies',
                        ],
                        23 => [
                            'industryId' => '15_30',
                            'industryName' => 'Mortgage Brokers',
                        ],
                        24 => [
                            'industryId' => '15_31',
                            'industryName' => 'Patent Owners and Lessors',
                        ],
                        25 => [
                            'industryId' => '15_32',
                            'industryName' => 'Pension and Retirement Funds',
                        ],
                        26 => [
                            'industryId' => '15_33',
                            'industryName' => 'Private Equity',
                        ],
                        27 => [
                            'industryId' => '15_34',
                            'industryName' => 'Royalty Trusts',
                        ],
                        28 => [
                            'industryId' => '15_35',
                            'industryName' => 'Securities Brokers and Traders ',
                        ],
                        29 => [
                            'industryId' => '15_36',
                            'industryName' => 'Specialty Financial Services',
                        ],
                        30 => [
                            'industryId' => '15_37',
                            'industryName' => 'Stock Exchanges',
                        ],
                        31 => [
                            'industryId' => '15_38',
                            'industryName' => 'Trade Facilitation',
                        ],
                        32 => [
                            'industryId' => '15_39',
                            'industryName' => 'Venture Capital',
                        ],
                        33 => [
                            'industryId' => '15_4',
                            'industryName' => 'Consumer Credit Reporting',
                        ],
                        34 => [
                            'industryId' => '15_5',
                            'industryName' => 'Credit and Collection Services',
                        ],
                        35 => [
                            'industryId' => '15_6',
                            'industryName' => 'Credit Cards',
                        ],
                        36 => [
                            'industryId' => '15_7',
                            'industryName' => 'Credit Intermediation',
                        ],
                        37 => [
                            'industryId' => '15_8',
                            'industryName' => 'Credit Rating Agency',
                        ],
                        38 => [
                            'industryId' => '15_9',
                            'industryName' => 'Crowdfunding',
                        ],
                    ],
                ],
                15 => [
                    'industry' => [
                        'industryId' => '16',
                        'industryName' => 'Food and Beverage',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '16_1',
                            'industryName' => 'Bakery and Bakery Products',
                        ],
                        1 => [
                            'industryId' => '16_10',
                            'industryName' => 'Flours and Baked Goods',
                        ],
                        2 => [
                            'industryId' => '16_11',
                            'industryName' => 'Food Oils',
                        ],
                        3 => [
                            'industryId' => '16_12',
                            'industryName' => 'Food Processing',
                        ],
                        4 => [
                            'industryId' => '16_13',
                            'industryName' => 'Food Products',
                        ],
                        5 => [
                            'industryId' => '16_14',
                            'industryName' => 'Food Wholesale Distributors',
                        ],
                        6 => [
                            'industryId' => '16_15',
                            'industryName' => 'Fresh and Frozen Seafood',
                        ],
                        7 => [
                            'industryId' => '16_16',
                            'industryName' => 'Meat Packing and Meat Products',
                        ],
                        8 => [
                            'industryId' => '16_17',
                            'industryName' => 'Pastas and Cereals',
                        ],
                        9 => [
                            'industryId' => '16_18',
                            'industryName' => 'Poultry',
                        ],
                        10 => [
                            'industryId' => '16_19',
                            'industryName' => 'Sausages and Other Prepared Meats',
                        ],
                        11 => [
                            'industryId' => '16_2',
                            'industryName' => 'Beverages - Brewers',
                        ],
                        12 => [
                            'industryId' => '16_3',
                            'industryName' => 'Beverages - Distillers and Wineries',
                        ],
                        13 => [
                            'industryId' => '16_4',
                            'industryName' => 'Beverages - Non-Alcoholic',
                        ],
                        14 => [
                            'industryId' => '16_5',
                            'industryName' => 'Bottling and Distribution',
                        ],
                        15 => [
                            'industryId' => '16_6',
                            'industryName' => 'Candy and Confections',
                        ],
                        16 => [
                            'industryId' => '16_7',
                            'industryName' => 'Canned and Frozen Fruits and Vegetables',
                        ],
                        17 => [
                            'industryId' => '16_8',
                            'industryName' => 'Dairy Products',
                        ],
                        18 => [
                            'industryId' => '16_9',
                            'industryName' => 'Flavorings, Spices and Other Ingredients',
                        ],
                    ],
                ],
                16 => [
                    'industry' => [
                        'industryId' => '17',
                        'industryName' => 'Government',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '17_1',
                            'industryName' => 'Cities, Towns, and Municipalities',
                        ],
                        1 => [
                            'industryId' => '17_10',
                            'industryName' => 'Legislatures',
                        ],
                        2 => [
                            'industryId' => '17_11',
                            'industryName' => 'National Security',
                        ],
                        3 => [
                            'industryId' => '17_12',
                            'industryName' => 'Police Protection',
                        ],
                        4 => [
                            'industryId' => '17_13',
                            'industryName' => 'Special Districts',
                        ],
                        5 => [
                            'industryId' => '17_14',
                            'industryName' => 'State, Provincial or Regional Government',
                        ],
                        6 => [
                            'industryId' => '17_15',
                            'industryName' => 'Villages and Small Municipalities',
                        ],
                        7 => [
                            'industryId' => '17_2',
                            'industryName' => 'Correctional Facilities',
                        ],
                        8 => [
                            'industryId' => '17_3',
                            'industryName' => 'County Governments',
                        ],
                        9 => [
                            'industryId' => '17_4',
                            'industryName' => 'Courts of Law',
                        ],
                        10 => [
                            'industryId' => '17_5',
                            'industryName' => 'Executive Government Offices',
                        ],
                        11 => [
                            'industryId' => '17_6',
                            'industryName' => 'Federal Government Agencies',
                        ],
                        12 => [
                            'industryId' => '17_7',
                            'industryName' => 'Fire Protection',
                        ],
                        13 => [
                            'industryId' => '17_8',
                            'industryName' => 'International Government Agencies',
                        ],
                        14 => [
                            'industryId' => '17_9',
                            'industryName' => 'Legal Counsel and Prosecution',
                        ],
                    ],
                ],
                17 => [
                    'industry' => [
                        'industryId' => '18',
                        'industryName' => 'Holding Companies ',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '18_1',
                            'industryName' => 'Industrial Conglomerates',
                        ],
                    ],
                ],
                18 => [
                    'industry' => [
                        'industryId' => '19',
                        'industryName' => 'Hospitals and Healthcare',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '19_1',
                            'industryName' => 'Ambulance Services',
                        ],
                        1 => [
                            'industryId' => '19_10',
                            'industryName' => 'Diagnostic Imaging Centers',
                        ],
                        2 => [
                            'industryId' => '19_11',
                            'industryName' => 'Electromedical and Therapeutic Equipment',
                        ],
                        3 => [
                            'industryId' => '19_12',
                            'industryName' => 'Emergency Medical Services',
                        ],
                        4 => [
                            'industryId' => '19_13',
                            'industryName' => 'Family Planning Clinics',
                        ],
                        5 => [
                            'industryId' => '19_14',
                            'industryName' => 'Fertility Clinics',
                        ],
                        6 => [
                            'industryId' => '19_15',
                            'industryName' => 'General Healthcare Equipment',
                        ],
                        7 => [
                            'industryId' => '19_16',
                            'industryName' => 'General Medical and Surgical Hospitals',
                        ],
                        8 => [
                            'industryId' => '19_17',
                            'industryName' => 'General Physicians and Clinics',
                        ],
                        9 => [
                            'industryId' => '19_18',
                            'industryName' => 'Healthcare Districts',
                        ],
                        10 => [
                            'industryId' => '19_19',
                            'industryName' => 'Home Healthcare',
                        ],
                        11 => [
                            'industryId' => '19_2',
                            'industryName' => 'Assisted Living Facilities',
                        ],
                        12 => [
                            'industryId' => '19_20',
                            'industryName' => 'Hospice Services',
                        ],
                        13 => [
                            'industryId' => '19_21',
                            'industryName' => 'Integrated Healthcare Networks',
                        ],
                        14 => [
                            'industryId' => '19_22',
                            'industryName' => 'Kidney Dialysis Centers',
                        ],
                        15 => [
                            'industryId' => '19_23',
                            'industryName' => 'Medical and Dental Laboratories',
                        ],
                        16 => [
                            'industryId' => '19_24',
                            'industryName' => 'Medical Practice Management and Services',
                        ],
                        17 => [
                            'industryId' => '19_25',
                            'industryName' => 'Mental Health Practitioners',
                        ],
                        18 => [
                            'industryId' => '19_26',
                            'industryName' => 'Nursing Homes and Extended Care',
                        ],
                        19 => [
                            'industryId' => '19_27',
                            'industryName' => 'Obstetricians and Gynecologists',
                        ],
                        20 => [
                            'industryId' => '19_28',
                            'industryName' => 'Oncology Services',
                        ],
                        21 => [
                            'industryId' => '19_29',
                            'industryName' => 'Ophthalmic Equipment',
                        ],
                        22 => [
                            'industryId' => '19_3',
                            'industryName' => 'Blood and Organ Banks',
                        ],
                        23 => [
                            'industryId' => '19_30',
                            'industryName' => 'Ophthalmologists and Optometrists',
                        ],
                        24 => [
                            'industryId' => '19_31',
                            'industryName' => 'Orthopedic and Prosthetic Equipment',
                        ],
                        25 => [
                            'industryId' => '19_32',
                            'industryName' => 'Orthopedic Services',
                        ],
                        26 => [
                            'industryId' => '19_33',
                            'industryName' => 'Other Specialty Hospitals',
                        ],
                        27 => [
                            'industryId' => '19_34',
                            'industryName' => 'Pediatricians',
                        ],
                        28 => [
                            'industryId' => '19_35',
                            'industryName' => 'Physical Therapy Facilities',
                        ],
                        29 => [
                            'industryId' => '19_36',
                            'industryName' => 'Podiatrists',
                        ],
                        30 => [
                            'industryId' => '19_37',
                            'industryName' => 'Psychiatric Hospitals',
                        ],
                        31 => [
                            'industryId' => '19_38',
                            'industryName' => 'Radiology Services',
                        ],
                        32 => [
                            'industryId' => '19_39',
                            'industryName' => 'Specialty Surgical Hospitals',
                        ],
                        33 => [
                            'industryId' => '19_4',
                            'industryName' => 'Cardiologists',
                        ],
                        34 => [
                            'industryId' => '19_40',
                            'industryName' => 'Substance Abuse Rehabilitation Centers',
                        ],
                        35 => [
                            'industryId' => '19_41',
                            'industryName' => 'Surgical and Medical Devices',
                        ],
                        36 => [
                            'industryId' => '19_42',
                            'industryName' => 'Urgent Care Centers',
                        ],
                        37 => [
                            'industryId' => '19_43',
                            'industryName' => 'X-Ray Equipment',
                        ],
                        38 => [
                            'industryId' => '19_5',
                            'industryName' => 'Children\'s Hospitals',
                        ],
                        39 => [
                            'industryId' => '19_6',
                            'industryName' => 'Chiropractors',
                        ],
                        40 => [
                            'industryId' => '19_7',
                            'industryName' => 'Dental Equipment and Supplies',
                        ],
                        41 => [
                            'industryId' => '19_8',
                            'industryName' => 'Dentists',
                        ],
                        42 => [
                            'industryId' => '19_9',
                            'industryName' => 'Dermatologists',
                        ],
                    ],
                ],
                19 => [
                    'industry' => [
                        'industryId' => '20',
                        'industryName' => 'Industrial Manufacturing and Services',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '20_1',
                            'industryName' => 'Advanced Medical Equipment',
                        ],
                        1 => [
                            'industryId' => '20_10',
                            'industryName' => 'Electromedical Devices and Equipment',
                        ],
                        2 => [
                            'industryId' => '20_11',
                            'industryName' => 'Engines and Turbines',
                        ],
                        3 => [
                            'industryId' => '20_12',
                            'industryName' => 'Envelopes',
                        ],
                        4 => [
                            'industryId' => '20_13',
                            'industryName' => 'Fabricated Rubber Products',
                        ],
                        5 => [
                            'industryId' => '20_14',
                            'industryName' => 'Farm Equipment',
                        ],
                        6 => [
                            'industryId' => '20_15',
                            'industryName' => 'Flat Glass',
                        ],
                        7 => [
                            'industryId' => '20_16',
                            'industryName' => 'Gaskets and Sealing Devices',
                        ],
                        8 => [
                            'industryId' => '20_17',
                            'industryName' => 'Glass, Glass Containers and Glassware',
                        ],
                        9 => [
                            'industryId' => '20_18',
                            'industryName' => 'Heating Equipment',
                        ],
                        10 => [
                            'industryId' => '20_19',
                            'industryName' => 'Industrial Equipment and Machinery',
                        ],
                        11 => [
                            'industryId' => '20_2',
                            'industryName' => 'Air and Gas Compressors',
                        ],
                        12 => [
                            'industryId' => '20_20',
                            'industryName' => 'Industrial Fans',
                        ],
                        13 => [
                            'industryId' => '20_21',
                            'industryName' => 'Industrial Furnaces and Ovens',
                        ],
                        14 => [
                            'industryId' => '20_22',
                            'industryName' => 'Industrial Machinery Distribution',
                        ],
                        15 => [
                            'industryId' => '20_23',
                            'industryName' => 'Industrial Measurement Devices',
                        ],
                        16 => [
                            'industryId' => '20_24',
                            'industryName' => 'Laboratory Equipment',
                        ],
                        17 => [
                            'industryId' => '20_25',
                            'industryName' => 'Manifold Business Forms',
                        ],
                        18 => [
                            'industryId' => '20_26',
                            'industryName' => 'Measuring Devices and Controllers',
                        ],
                        19 => [
                            'industryId' => '20_27',
                            'industryName' => 'Metalworking Machinery',
                        ],
                        20 => [
                            'industryId' => '20_28',
                            'industryName' => 'Miscellaneous Fabricated Wire Products',
                        ],
                        21 => [
                            'industryId' => '20_29',
                            'industryName' => 'Miscellaneous Manufacturing',
                        ],
                        22 => [
                            'industryId' => '20_3',
                            'industryName' => 'Automated Climate Controls',
                        ],
                        23 => [
                            'industryId' => '20_30',
                            'industryName' => 'Miscellaneous Petroleum Products',
                        ],
                        24 => [
                            'industryId' => '20_31',
                            'industryName' => 'Miscellaneous Plastics',
                        ],
                        25 => [
                            'industryId' => '20_32',
                            'industryName' => 'Motors and Generators',
                        ],
                        26 => [
                            'industryId' => '20_33',
                            'industryName' => 'Paper Board and Paper Products',
                        ],
                        27 => [
                            'industryId' => '20_34',
                            'industryName' => 'Paper Containers and Packaging',
                        ],
                        28 => [
                            'industryId' => '20_35',
                            'industryName' => 'Paper Mills',
                        ],
                        29 => [
                            'industryId' => '20_36',
                            'industryName' => 'Paperboard Mills',
                        ],
                        30 => [
                            'industryId' => '20_37',
                            'industryName' => 'Plastic Materials and Synthetic Resins',
                        ],
                        31 => [
                            'industryId' => '20_38',
                            'industryName' => 'Plastics, Foil and Coated Paper Bags',
                        ],
                        32 => [
                            'industryId' => '20_39',
                            'industryName' => 'Power Distribution and Transformers',
                        ],
                        33 => [
                            'industryId' => '20_4',
                            'industryName' => 'Ball Bearings and Roller Bearing',
                        ],
                        34 => [
                            'industryId' => '20_40',
                            'industryName' => 'Printing Press Machinery',
                        ],
                        35 => [
                            'industryId' => '20_41',
                            'industryName' => 'Pulp Mills',
                        ],
                        36 => [
                            'industryId' => '20_42',
                            'industryName' => 'Pumping Equipment',
                        ],
                        37 => [
                            'industryId' => '20_43',
                            'industryName' => 'Relays and Industrial Controls',
                        ],
                        38 => [
                            'industryId' => '20_44',
                            'industryName' => 'Screw Machines',
                        ],
                        39 => [
                            'industryId' => '20_45',
                            'industryName' => 'Specialty Industrial Machinery',
                        ],
                        40 => [
                            'industryId' => '20_46',
                            'industryName' => 'Stationery and Related Products',
                        ],
                        41 => [
                            'industryId' => '20_47',
                            'industryName' => 'Switching and Switchboard Equipment',
                        ],
                        42 => [
                            'industryId' => '20_48',
                            'industryName' => 'Tires and Inner Tubes',
                        ],
                        43 => [
                            'industryId' => '20_49',
                            'industryName' => 'Totalizing Fluid Meters',
                        ],
                        44 => [
                            'industryId' => '20_5',
                            'industryName' => 'Bolts, Nuts, Screws, Rivets and Washers',
                        ],
                        45 => [
                            'industryId' => '20_6',
                            'industryName' => 'Building Climate Control and HVAC',
                        ],
                        46 => [
                            'industryId' => '20_7',
                            'industryName' => 'Commercial Equipment and Supplies',
                        ],
                        47 => [
                            'industryId' => '20_8',
                            'industryName' => 'Converted Paper Products',
                        ],
                        48 => [
                            'industryId' => '20_9',
                            'industryName' => 'Electric Lighting and Wiring',
                        ],
                    ],
                ],
                20 => [
                    'industry' => [
                        'industryId' => '21',
                        'industryName' => 'Insurance',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '21_1',
                            'industryName' => 'Automobile Insurance',
                        ],
                        1 => [
                            'industryId' => '21_10',
                            'industryName' => 'Insurance Financing',
                        ],
                        2 => [
                            'industryId' => '21_11',
                            'industryName' => 'Liability Insurance',
                        ],
                        3 => [
                            'industryId' => '21_12',
                            'industryName' => 'Life Insurance',
                        ],
                        4 => [
                            'industryId' => '21_13',
                            'industryName' => 'Mortgage Insurance',
                        ],
                        5 => [
                            'industryId' => '21_14',
                            'industryName' => 'Multiline Insurance',
                        ],
                        6 => [
                            'industryId' => '21_15',
                            'industryName' => 'Property and Casualty Insurance',
                        ],
                        7 => [
                            'industryId' => '21_16',
                            'industryName' => 'Reinsurance',
                        ],
                        8 => [
                            'industryId' => '21_17',
                            'industryName' => 'Risk Management',
                        ],
                        9 => [
                            'industryId' => '21_18',
                            'industryName' => 'Specialty Insurance',
                        ],
                        10 => [
                            'industryId' => '21_19',
                            'industryName' => 'Surety Insurance',
                        ],
                        11 => [
                            'industryId' => '21_2',
                            'industryName' => 'Claims Administration and Processing',
                        ],
                        12 => [
                            'industryId' => '21_20',
                            'industryName' => 'Travel Insurance',
                        ],
                        13 => [
                            'industryId' => '21_21',
                            'industryName' => 'Workers\' Compensation',
                        ],
                        14 => [
                            'industryId' => '21_3',
                            'industryName' => 'Commercial Insurance',
                        ],
                        15 => [
                            'industryId' => '21_4',
                            'industryName' => 'Credit Insurance',
                        ],
                        16 => [
                            'industryId' => '21_5',
                            'industryName' => 'Disability Insurance',
                        ],
                        17 => [
                            'industryId' => '21_6',
                            'industryName' => 'Fire and Marine Insurance',
                        ],
                        18 => [
                            'industryId' => '21_7',
                            'industryName' => 'Health Insurance',
                        ],
                        19 => [
                            'industryId' => '21_8',
                            'industryName' => 'Homeowners and Title Insurance',
                        ],
                        20 => [
                            'industryId' => '21_9',
                            'industryName' => 'Insurance Agents and Brokers',
                        ],
                    ],
                ],
                21 => [
                    'industry' => [
                        'industryId' => '22',
                        'industryName' => 'Leisure, Sports and Recreation',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '22_1',
                            'industryName' => 'Accommodation',
                        ],
                        1 => [
                            'industryId' => '22_10',
                            'industryName' => 'Golf Equipment ',
                        ],
                        2 => [
                            'industryId' => '22_11',
                            'industryName' => 'Hotels and Motels',
                        ],
                        3 => [
                            'industryId' => '22_12',
                            'industryName' => 'Leisure Products',
                        ],
                        4 => [
                            'industryId' => '22_13',
                            'industryName' => 'Motion Picture Distribution',
                        ],
                        5 => [
                            'industryId' => '22_14',
                            'industryName' => 'Museums and Art Galleries',
                        ],
                        6 => [
                            'industryId' => '22_15',
                            'industryName' => 'Musical Instruments',
                        ],
                        7 => [
                            'industryId' => '22_16',
                            'industryName' => 'Park and Recreation Districts',
                        ],
                        8 => [
                            'industryId' => '22_17',
                            'industryName' => 'Professional Sports Teams',
                        ],
                        9 => [
                            'industryId' => '22_18',
                            'industryName' => 'Racetracks',
                        ],
                        10 => [
                            'industryId' => '22_19',
                            'industryName' => 'Sporting Goods, Outdoor Gear and Apparel ',
                        ],
                        11 => [
                            'industryId' => '22_2',
                            'industryName' => 'Amusement and Recreation',
                        ],
                        12 => [
                            'industryId' => '22_20',
                            'industryName' => 'Sports and Recreations Clubs',
                        ],
                        13 => [
                            'industryId' => '22_21',
                            'industryName' => 'Sports Equipment ',
                        ],
                        14 => [
                            'industryId' => '22_3',
                            'industryName' => 'Art Supplies',
                        ],
                        15 => [
                            'industryId' => '22_4',
                            'industryName' => 'Athletic Facilities',
                        ],
                        16 => [
                            'industryId' => '22_5',
                            'industryName' => 'Bicycles and Accessories ',
                        ],
                        17 => [
                            'industryId' => '22_6',
                            'industryName' => 'Casinos and Gambling',
                        ],
                        18 => [
                            'industryId' => '22_7',
                            'industryName' => 'Dolls and Stuffed Toys',
                        ],
                        19 => [
                            'industryId' => '22_8',
                            'industryName' => 'Entertainment Services',
                        ],
                        20 => [
                            'industryId' => '22_9',
                            'industryName' => 'Games and Toys',
                        ],
                    ],
                ],
                22 => [
                    'industry' => [
                        'industryId' => '23',
                        'industryName' => 'Media',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '23_1',
                            'industryName' => 'Audiovisual Equipment Sales and Services',
                        ],
                        1 => [
                            'industryId' => '23_10',
                            'industryName' => 'Diversified Media',
                        ],
                        2 => [
                            'industryId' => '23_11',
                            'industryName' => 'Film and Video',
                        ],
                        3 => [
                            'industryId' => '23_12',
                            'industryName' => 'Greeting Cards',
                        ],
                        4 => [
                            'industryId' => '23_13',
                            'industryName' => 'Internet Content Providers',
                        ],
                        5 => [
                            'industryId' => '23_14',
                            'industryName' => 'Internet Information Services',
                        ],
                        6 => [
                            'industryId' => '23_15',
                            'industryName' => 'Internet Search and Navigation Services',
                        ],
                        7 => [
                            'industryId' => '23_16',
                            'industryName' => 'Motion Picture and Video Production',
                        ],
                        8 => [
                            'industryId' => '23_17',
                            'industryName' => 'Motion Picture Equipment',
                        ],
                        9 => [
                            'industryId' => '23_18',
                            'industryName' => 'Motion Picture Theaters',
                        ],
                        10 => [
                            'industryId' => '23_19',
                            'industryName' => 'Music Recording and Distribution',
                        ],
                        11 => [
                            'industryId' => '23_2',
                            'industryName' => 'Book Publishers',
                        ],
                        12 => [
                            'industryId' => '23_20',
                            'industryName' => 'Newspaper Publishers',
                        ],
                        13 => [
                            'industryId' => '23_21',
                            'industryName' => 'Newspapers and Online News Organizations',
                        ],
                        14 => [
                            'industryId' => '23_22',
                            'industryName' => 'Periodical and Magazine Publishers',
                        ],
                        15 => [
                            'industryId' => '23_23',
                            'industryName' => 'Publishing',
                        ],
                        16 => [
                            'industryId' => '23_24',
                            'industryName' => 'Radio Broadcasting',
                        ],
                        17 => [
                            'industryId' => '23_25',
                            'industryName' => 'Television Broadcasting',
                        ],
                        18 => [
                            'industryId' => '23_26',
                            'industryName' => 'Trading Cards and Comic Books',
                        ],
                        19 => [
                            'industryId' => '23_27',
                            'industryName' => 'Video Game Software',
                        ],
                        20 => [
                            'industryId' => '23_3',
                            'industryName' => 'Broadcasting',
                        ],
                        21 => [
                            'industryId' => '23_4',
                            'industryName' => 'Broadcasting Equipment',
                        ],
                        22 => [
                            'industryId' => '23_5',
                            'industryName' => 'Cable and Satellite Services',
                        ],
                        23 => [
                            'industryId' => '23_6',
                            'industryName' => 'Cable Networks and Program Distribution ',
                        ],
                        24 => [
                            'industryId' => '23_7',
                            'industryName' => 'Cable TV System Operators',
                        ],
                        25 => [
                            'industryId' => '23_8',
                            'industryName' => 'CD and DVD Manufacturing and Supply',
                        ],
                        26 => [
                            'industryId' => '23_9',
                            'industryName' => 'Directories and Yellow Pages Publishers',
                        ],
                    ],
                ],
                23 => [
                    'industry' => [
                        'industryId' => '24',
                        'industryName' => 'Mining and Metals',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '24_1',
                            'industryName' => 'Aluminum Producers',
                        ],
                        1 => [
                            'industryId' => '24_10',
                            'industryName' => 'Metal Forgings',
                        ],
                        2 => [
                            'industryId' => '24_11',
                            'industryName' => 'Metals and Minerals Distribution',
                        ],
                        3 => [
                            'industryId' => '24_12',
                            'industryName' => 'Mining Machinery',
                        ],
                        4 => [
                            'industryId' => '24_13',
                            'industryName' => 'Miscellaneous Fabricated Metal Products',
                        ],
                        5 => [
                            'industryId' => '24_14',
                            'industryName' => 'Miscellaneous Metal Mining',
                        ],
                        6 => [
                            'industryId' => '24_15',
                            'industryName' => 'Miscellaneous Metal Products',
                        ],
                        7 => [
                            'industryId' => '24_16',
                            'industryName' => 'Non-Ferrous Foundries',
                        ],
                        8 => [
                            'industryId' => '24_17',
                            'industryName' => 'Non-Metallic Mining and Quarrying',
                        ],
                        9 => [
                            'industryId' => '24_18',
                            'industryName' => 'Other Non Metallic Mineral Products',
                        ],
                        10 => [
                            'industryId' => '24_19',
                            'industryName' => 'Precious Metals and Minerals',
                        ],
                        11 => [
                            'industryId' => '24_2',
                            'industryName' => 'Coal Mining',
                        ],
                        12 => [
                            'industryId' => '24_20',
                            'industryName' => 'Prefabricated Metal Buildings',
                        ],
                        13 => [
                            'industryId' => '24_21',
                            'industryName' => 'Rolling and Extruding Non-Ferrous Metals',
                        ],
                        14 => [
                            'industryId' => '24_22',
                            'industryName' => 'Sheet Metal',
                        ],
                        15 => [
                            'industryId' => '24_23',
                            'industryName' => 'Shipping Barrels, Drums, Kegs and Pails',
                        ],
                        16 => [
                            'industryId' => '24_24',
                            'industryName' => 'Smelting and Refining Non-Ferrous Metals',
                        ],
                        17 => [
                            'industryId' => '24_25',
                            'industryName' => 'Specialty Mining and Metals',
                        ],
                        18 => [
                            'industryId' => '24_26',
                            'industryName' => 'Steel Pipes and Tubes',
                        ],
                        19 => [
                            'industryId' => '24_27',
                            'industryName' => 'Steel Works and Coke Ovens',
                        ],
                        20 => [
                            'industryId' => '24_28',
                            'industryName' => 'Stone Products',
                        ],
                        21 => [
                            'industryId' => '24_3',
                            'industryName' => 'Coating and Engravings',
                        ],
                        22 => [
                            'industryId' => '24_4',
                            'industryName' => 'Concrete and Gypsum',
                        ],
                        23 => [
                            'industryId' => '24_5',
                            'industryName' => 'Fabricated Structural Metal',
                        ],
                        24 => [
                            'industryId' => '24_6',
                            'industryName' => 'Gold and Silver Mining',
                        ],
                        25 => [
                            'industryId' => '24_7',
                            'industryName' => 'Iron and Steel Foundries',
                        ],
                        26 => [
                            'industryId' => '24_8',
                            'industryName' => 'Machine Tools and Metal Equipment ',
                        ],
                        27 => [
                            'industryId' => '24_9',
                            'industryName' => 'Metal Cans',
                        ],
                    ],
                ],
                24 => [
                    'industry' => [
                        'industryId' => '25',
                        'industryName' => 'Pharmaceuticals and Biotech',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '25_1',
                            'industryName' => 'Biopharmaceuticals and Biotherapeutics',
                        ],
                        1 => [
                            'industryId' => '25_10',
                            'industryName' => 'Pharmaceuticals - Generic and Specialty',
                        ],
                        2 => [
                            'industryId' => '25_11',
                            'industryName' => 'Pharmaceuticals Wholesale',
                        ],
                        3 => [
                            'industryId' => '25_12',
                            'industryName' => 'Vitamin, Nutritional and Health Products',
                        ],
                        4 => [
                            'industryId' => '25_13',
                            'industryName' => 'Wholesale Drugs and Pharmacy Supplies',
                        ],
                        5 => [
                            'industryId' => '25_2',
                            'industryName' => 'Biotechnology',
                        ],
                        6 => [
                            'industryId' => '25_3',
                            'industryName' => 'Biotechnology Research Equipment',
                        ],
                        7 => [
                            'industryId' => '25_4',
                            'industryName' => 'Biotechnology Research Services',
                        ],
                        8 => [
                            'industryId' => '25_5',
                            'industryName' => 'Commercial Scientific Research Services',
                        ],
                        9 => [
                            'industryId' => '25_6',
                            'industryName' => 'Diagnostic Substances',
                        ],
                        10 => [
                            'industryId' => '25_7',
                            'industryName' => 'Drug Delivery Systems',
                        ],
                        11 => [
                            'industryId' => '25_8',
                            'industryName' => 'Over-the-Counter Medications',
                        ],
                        12 => [
                            'industryId' => '25_9',
                            'industryName' => 'Pharmaceuticals - Diversified',
                        ],
                    ],
                ],
                25 => [
                    'industry' => [
                        'industryId' => '26',
                        'industryName' => 'Real Estate Services',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '26_1',
                            'industryName' => 'Building Operators',
                        ],
                        1 => [
                            'industryId' => '26_10',
                            'industryName' => 'Mortgage and Investment REITs',
                        ],
                        2 => [
                            'industryId' => '26_11',
                            'industryName' => 'Office REITs',
                        ],
                        3 => [
                            'industryId' => '26_12',
                            'industryName' => 'Other Real Estate Investors',
                        ],
                        4 => [
                            'industryId' => '26_13',
                            'industryName' => 'Planning and Development Authorities',
                        ],
                        5 => [
                            'industryId' => '26_14',
                            'industryName' => 'Real Estate Appraisers',
                        ],
                        6 => [
                            'industryId' => '26_15',
                            'industryName' => 'Real Estate Brokers',
                        ],
                        7 => [
                            'industryId' => '26_16',
                            'industryName' => 'Real Estate Development',
                        ],
                        8 => [
                            'industryId' => '26_17',
                            'industryName' => 'Real Estate Investment Trusts (REITs)',
                        ],
                        9 => [
                            'industryId' => '26_18',
                            'industryName' => 'Real Estate Leasing',
                        ],
                        10 => [
                            'industryId' => '26_19',
                            'industryName' => 'Real Estate Operators',
                        ],
                        11 => [
                            'industryId' => '26_2',
                            'industryName' => 'Health Care REITs',
                        ],
                        12 => [
                            'industryId' => '26_20',
                            'industryName' => 'Real Estate Property Management',
                        ],
                        13 => [
                            'industryId' => '26_21',
                            'industryName' => 'REIT - Residential and Commercial',
                        ],
                        14 => [
                            'industryId' => '26_22',
                            'industryName' => 'Retail REITs',
                        ],
                        15 => [
                            'industryId' => '26_23',
                            'industryName' => 'School and Public Building Authorities',
                        ],
                        16 => [
                            'industryId' => '26_24',
                            'industryName' => 'Title Abstract Offices',
                        ],
                        17 => [
                            'industryId' => '26_3',
                            'industryName' => 'Hotel and Motel REITs',
                        ],
                        18 => [
                            'industryId' => '26_4',
                            'industryName' => 'Housing Authorities',
                        ],
                        19 => [
                            'industryId' => '26_5',
                            'industryName' => 'Industrial Development Authorities',
                        ],
                        20 => [
                            'industryId' => '26_6',
                            'industryName' => 'Industrial REITs',
                        ],
                        21 => [
                            'industryId' => '26_7',
                            'industryName' => 'Land REITs',
                        ],
                        22 => [
                            'industryId' => '26_8',
                            'industryName' => 'Leisure and Entertainment REITs',
                        ],
                        23 => [
                            'industryId' => '26_9',
                            'industryName' => 'Mobile Homes',
                        ],
                    ],
                ],
                26 => [
                    'industry' => [
                        'industryId' => '27',
                        'industryName' => 'Retail',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '27_1',
                            'industryName' => 'Arts, Gifts and Novelties Retail',
                        ],
                        1 => [
                            'industryId' => '27_10',
                            'industryName' => 'Footwear Retailers',
                        ],
                        2 => [
                            'industryId' => '27_11',
                            'industryName' => 'Furniture Retailers',
                        ],
                        3 => [
                            'industryId' => '27_12',
                            'industryName' => 'Gasoline Stations',
                        ],
                        4 => [
                            'industryId' => '27_13',
                            'industryName' => 'Grocery and Food Stores',
                        ],
                        5 => [
                            'industryId' => '27_14',
                            'industryName' => 'Hardware and Garden stores',
                        ],
                        6 => [
                            'industryId' => '27_15',
                            'industryName' => 'Hobby, Toy and Game Stores',
                        ],
                        7 => [
                            'industryId' => '27_16',
                            'industryName' => 'Home Furnishings and Improvement Retail',
                        ],
                        8 => [
                            'industryId' => '27_17',
                            'industryName' => 'Home Improvement and Hardware Retail ',
                        ],
                        9 => [
                            'industryId' => '27_18',
                            'industryName' => 'Hyper and Supermarkets',
                        ],
                        10 => [
                            'industryId' => '27_19',
                            'industryName' => 'Jewelry and Gemstone Retail',
                        ],
                        11 => [
                            'industryId' => '27_2',
                            'industryName' => 'Boat Retail',
                        ],
                        12 => [
                            'industryId' => '27_20',
                            'industryName' => 'Lumber Retailers',
                        ],
                        13 => [
                            'industryId' => '27_21',
                            'industryName' => 'Mail-Order Retailers',
                        ],
                        14 => [
                            'industryId' => '27_22',
                            'industryName' => 'Miscellaneous General Retailers',
                        ],
                        15 => [
                            'industryId' => '27_23',
                            'industryName' => 'Mobile Home Dealers',
                        ],
                        16 => [
                            'industryId' => '27_24',
                            'industryName' => 'Motor Vehicle Dealers',
                        ],
                        17 => [
                            'industryId' => '27_25',
                            'industryName' => 'Music, Video and Book Retail',
                        ],
                        18 => [
                            'industryId' => '27_26',
                            'industryName' => 'Musical Equipment Retail',
                        ],
                        19 => [
                            'industryId' => '27_27',
                            'industryName' => 'Party and Holiday Accessories Retail ',
                        ],
                        20 => [
                            'industryId' => '27_28',
                            'industryName' => 'Restaurants, Bars and Eateries',
                        ],
                        21 => [
                            'industryId' => '27_29',
                            'industryName' => 'Retail - Apparel and Accessories',
                        ],
                        22 => [
                            'industryId' => '27_3',
                            'industryName' => 'Building Materials Retailers',
                        ],
                        23 => [
                            'industryId' => '27_30',
                            'industryName' => 'Retail - Department Stores',
                        ],
                        24 => [
                            'industryId' => '27_31',
                            'industryName' => 'Retail - Discount and Variety Stores',
                        ],
                        25 => [
                            'industryId' => '27_32',
                            'industryName' => 'Retail - Drugs',
                        ],
                        26 => [
                            'industryId' => '27_33',
                            'industryName' => 'Retail - Electronics Products',
                        ],
                        27 => [
                            'industryId' => '27_34',
                            'industryName' => 'Retail - Internet and Catalog Order',
                        ],
                        28 => [
                            'industryId' => '27_35',
                            'industryName' => 'Specialty Retailers',
                        ],
                        29 => [
                            'industryId' => '27_36',
                            'industryName' => 'Sports and Recreational Equipment Retail',
                        ],
                        30 => [
                            'industryId' => '27_37',
                            'industryName' => 'Warehouse Clubs and Superstores',
                        ],
                        31 => [
                            'industryId' => '27_38',
                            'industryName' => 'Women\'s Clothing Stores',
                        ],
                        32 => [
                            'industryId' => '27_4',
                            'industryName' => 'Clock parts and Watch Retailers',
                        ],
                        33 => [
                            'industryId' => '27_5',
                            'industryName' => 'Computer and Software Retail',
                        ],
                        34 => [
                            'industryId' => '27_6',
                            'industryName' => 'Convenience Stores',
                        ],
                        35 => [
                            'industryId' => '27_7',
                            'industryName' => 'Cosmetics and Perfume Retail',
                        ],
                        36 => [
                            'industryId' => '27_8',
                            'industryName' => 'Family and Children\'s Products Stores',
                        ],
                        37 => [
                            'industryId' => '27_9',
                            'industryName' => 'Floral and Florists',
                        ],
                    ],
                ],
                27 => [
                    'industry' => [
                        'industryId' => '28',
                        'industryName' => 'Schools and Education',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '28_1',
                            'industryName' => 'Colleges and Universities',
                        ],
                        1 => [
                            'industryId' => '28_10',
                            'industryName' => 'School Districts',
                        ],
                        2 => [
                            'industryId' => '28_11',
                            'industryName' => 'Training Institutions and Services',
                        ],
                        3 => [
                            'industryId' => '28_12',
                            'industryName' => 'Vocational and Technical Schools',
                        ],
                        4 => [
                            'industryId' => '28_2',
                            'industryName' => 'Community Colleges',
                        ],
                        5 => [
                            'industryId' => '28_3',
                            'industryName' => 'Educational Services',
                        ],
                        6 => [
                            'industryId' => '28_4',
                            'industryName' => 'Elementary and Secondary Schools',
                        ],
                        7 => [
                            'industryId' => '28_5',
                            'industryName' => 'Graduate and Professional Schools',
                        ],
                        8 => [
                            'industryId' => '28_6',
                            'industryName' => 'Internet Educational Services',
                        ],
                        9 => [
                            'industryId' => '28_7',
                            'industryName' => 'Law Schools',
                        ],
                        10 => [
                            'industryId' => '28_8',
                            'industryName' => 'Libraries',
                        ],
                        11 => [
                            'industryId' => '28_9',
                            'industryName' => 'Medical Schools',
                        ],
                    ],
                ],
                28 => [
                    'industry' => [
                        'industryId' => '29',
                        'industryName' => 'Telecommunications',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '29_1',
                            'industryName' => 'Data Network Operators',
                        ],
                        1 => [
                            'industryId' => '29_10',
                            'industryName' => 'Messaging Services Providers',
                        ],
                        2 => [
                            'industryId' => '29_11',
                            'industryName' => 'Miscellaneous Communications Services',
                        ],
                        3 => [
                            'industryId' => '29_12',
                            'industryName' => 'Miscellaneous Telecommunications ',
                        ],
                        4 => [
                            'industryId' => '29_13',
                            'industryName' => 'Optical Switching and Transmission',
                        ],
                        5 => [
                            'industryId' => '29_14',
                            'industryName' => 'Satellite and Broadcasting Equipment',
                        ],
                        6 => [
                            'industryId' => '29_15',
                            'industryName' => 'Satellite System Operators',
                        ],
                        7 => [
                            'industryId' => '29_16',
                            'industryName' => 'Telecom Equipment and Services',
                        ],
                        8 => [
                            'industryId' => '29_17',
                            'industryName' => 'Telecommunications Equipment',
                        ],
                        9 => [
                            'industryId' => '29_18',
                            'industryName' => 'Telecommunications Infra Development',
                        ],
                        10 => [
                            'industryId' => '29_19',
                            'industryName' => 'Telecommunications Resellers',
                        ],
                        11 => [
                            'industryId' => '29_2',
                            'industryName' => 'Data Services',
                        ],
                        12 => [
                            'industryId' => '29_20',
                            'industryName' => 'Telecommunications Services',
                        ],
                        13 => [
                            'industryId' => '29_21',
                            'industryName' => 'Teleconferencing Services Providers',
                        ],
                        14 => [
                            'industryId' => '29_22',
                            'industryName' => 'Telemetry and Telematics Services',
                        ],
                        15 => [
                            'industryId' => '29_23',
                            'industryName' => 'TV and Radio Broadcasting Equipment',
                        ],
                        16 => [
                            'industryId' => '29_24',
                            'industryName' => 'Videoconferencing Equipment',
                        ],
                        17 => [
                            'industryId' => '29_25',
                            'industryName' => 'Web Hosting Services',
                        ],
                        18 => [
                            'industryId' => '29_26',
                            'industryName' => 'Wire Switching and Transmission',
                        ],
                        19 => [
                            'industryId' => '29_27',
                            'industryName' => 'Wired Telecommunications Carriers',
                        ],
                        20 => [
                            'industryId' => '29_28',
                            'industryName' => 'Wireless Broadband Service Equipment',
                        ],
                        21 => [
                            'industryId' => '29_29',
                            'industryName' => 'Wireless Network Operators',
                        ],
                        22 => [
                            'industryId' => '29_3',
                            'industryName' => 'Electronic Communications Networks',
                        ],
                        23 => [
                            'industryId' => '29_30',
                            'industryName' => 'Wireless Switching and Transmission',
                        ],
                        24 => [
                            'industryId' => '29_31',
                            'industryName' => 'Wireless Telecommunications Equipment',
                        ],
                        25 => [
                            'industryId' => '29_32',
                            'industryName' => 'Wireless Telecommunications Resellers',
                        ],
                        26 => [
                            'industryId' => '29_33',
                            'industryName' => 'Wireless Telecommunications Services',
                        ],
                        27 => [
                            'industryId' => '29_34',
                            'industryName' => 'Wireless Telephone Handsets',
                        ],
                        28 => [
                            'industryId' => '29_35',
                            'industryName' => 'Wireline Telecommunications Equipment',
                        ],
                        29 => [
                            'industryId' => '29_4',
                            'industryName' => 'Enterprise Telecommunications Equipment',
                        ],
                        30 => [
                            'industryId' => '29_5',
                            'industryName' => 'Fixed-Line Voice Services Providers',
                        ],
                        31 => [
                            'industryId' => '29_6',
                            'industryName' => 'Internet and Online Services Providers',
                        ],
                        32 => [
                            'industryId' => '29_7',
                            'industryName' => 'Local Exchange Carriers',
                        ],
                        33 => [
                            'industryId' => '29_8',
                            'industryName' => 'Long-Distance Carriers',
                        ],
                        34 => [
                            'industryId' => '29_9',
                            'industryName' => 'Managed Network Services',
                        ],
                    ],
                ],
                29 => [
                    'industry' => [
                        'industryId' => '30',
                        'industryName' => 'Transportation',
                    ],
                    'subIndustries' => [
                        0 => [
                            'industryId' => '30_1',
                            'industryName' => 'Air Freight Transportation',
                        ],
                        1 => [
                            'industryId' => '30_10',
                            'industryName' => 'Highways and Toll Road Management',
                        ],
                        2 => [
                            'industryId' => '30_11',
                            'industryName' => 'Locomotive and Rail Car Manufacturing',
                        ],
                        3 => [
                            'industryId' => '30_12',
                            'industryName' => 'Marine Shipping',
                        ],
                        4 => [
                            'industryId' => '30_13',
                            'industryName' => 'Marine Transportation Support',
                        ],
                        5 => [
                            'industryId' => '30_14',
                            'industryName' => 'Non-Scheduled and Charter Air Transport',
                        ],
                        6 => [
                            'industryId' => '30_15',
                            'industryName' => 'Passenger Railroads',
                        ],
                        7 => [
                            'industryId' => '30_16',
                            'industryName' => 'Ports, Harbors and Marinas',
                        ],
                        8 => [
                            'industryId' => '30_17',
                            'industryName' => 'Postal Services',
                        ],
                        9 => [
                            'industryId' => '30_18',
                            'industryName' => 'Railroad Equipment Manufacturing',
                        ],
                        10 => [
                            'industryId' => '30_19',
                            'industryName' => 'Railroad Terminal Management',
                        ],
                        11 => [
                            'industryId' => '30_2',
                            'industryName' => 'Aircraft Leasing',
                        ],
                        12 => [
                            'industryId' => '30_20',
                            'industryName' => 'Railroads and Rail Tracks Maintenance',
                        ],
                        13 => [
                            'industryId' => '30_21',
                            'industryName' => 'Ship and Boat Manufacturing',
                        ],
                        14 => [
                            'industryId' => '30_22',
                            'industryName' => 'Ship and Boat Parts Manufacturing',
                        ],
                        15 => [
                            'industryId' => '30_23',
                            'industryName' => 'Ship and Boat Repair',
                        ],
                        16 => [
                            'industryId' => '30_24',
                            'industryName' => 'Shipping Equipment',
                        ],
                        17 => [
                            'industryId' => '30_25',
                            'industryName' => 'Storage and Warehousing',
                        ],
                        18 => [
                            'industryId' => '30_26',
                            'industryName' => 'Terminal Facilities For Motor Vehicles',
                        ],
                        19 => [
                            'industryId' => '30_27',
                            'industryName' => 'Transportation Authorities',
                        ],
                        20 => [
                            'industryId' => '30_28',
                            'industryName' => 'Truck Rental and Leasing',
                        ],
                        21 => [
                            'industryId' => '30_29',
                            'industryName' => 'Truck Transportation and Services',
                        ],
                        22 => [
                            'industryId' => '30_3',
                            'industryName' => 'Airlines and Scheduled Air Transport',
                        ],
                        23 => [
                            'industryId' => '30_4',
                            'industryName' => 'Airport and Terminal Management',
                        ],
                        24 => [
                            'industryId' => '30_5',
                            'industryName' => 'Bus Services',
                        ],
                        25 => [
                            'industryId' => '30_6',
                            'industryName' => 'Container Leasing',
                        ],
                        26 => [
                            'industryId' => '30_7',
                            'industryName' => 'Ferries and Water Transport',
                        ],
                        27 => [
                            'industryId' => '30_8',
                            'industryName' => 'Freight Railroads',
                        ],
                        28 => [
                            'industryId' => '30_9',
                            'industryName' => 'Helicopter Services',
                        ],
                    ],
                ],
        ];

        foreach($data as $item){
            $industry['slug'] = Str::slug($item['industry']['industryName']);
            $industry['name'] = $item['industry']['industryName'];

            if(!Industry::where('slug',$industry['slug'])->count())
                Industry::create($industry);
//
//            foreach($item['subIndustries'] as $subindustry){
//
//                $sub['slug'] = Str::slug($subindustry['industryName']);
//                $sub['name'] = $subindustry['industryName'];
//
//                if(!Industry::where('slug',$sub['slug'])->count())
//                    Industry::create($sub);
//
//            }
        }
    }
}
