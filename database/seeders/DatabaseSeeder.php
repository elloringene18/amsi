<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(CountrySeeder::class);
        $this->call(AlumniTableSeeder::class);
        $this->call(MapLocationsSeeder::class);
        $this->call(ArticleSeeder::class);
        $this->call(SkillSeeder::class);
        $this->call(IndustrySeeder::class);
         $this->call(JobSeeder::class);
         $this->call(EventSeeder::class);
         $this->call(PageContentSeeder::class);
        // $this->call(EducationTableSeeder::class);


    }
}
