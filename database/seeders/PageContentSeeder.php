<?php

namespace Database\Seeders;

use App\Models\PageContent;
use Illuminate\Database\Seeder;

class PageContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'page','section','title','title_ar','content','content_ar','type'

        $data = [
            [
                'page' => 'about',
                'content' => '<p>
                        Founded in 2016, the AMSI Alumni Association (AAA) aspires to be a primary link between AMSI’s alumni, students, parents, faculty, and staff, all of whom constitute the AMSI family.
                    </p>
                    <p>
                        With over 7000 alumni, we strive to open doors and connect minds by bringing together those who share a common bond. Family is one of our core values, and we believe in fostering our lifelong relationship as family members of Al Mawakeb.
                    </p>
                    <p>
                        Through our website, we invite you to explore the myriad opportunities available, from events to volunteer work, to help us connect &amp; network together.
                    </p>
                    <p>
                        The AAA is committed to advancing the best interests of our alumni and celebrating each and every one of them. Most of all, we hope that our alumni will stay involved and in touch with each other and with their second home, Al Mawakeb.
                    </p>
                    <hr>
                    <h3>About AMSI (Academia Management Solutions International)</h3>
                    <p>
                        AMSI is a collaboration of academicians and educators who bring together a wealth of experience and international expertise. In our alliance we have learnt to value creativity and the active pursuit of excellence. For over 40 years, AMSI has been dedicated to raising the standards of education in its community and building a foundation for the future of its youth through the provision of a complete and comprehensive educational solution.
                    </p>
                    <p>
                        AMSI schools have graduated more than 7000 students and have been home to over 220,000 students at one time or another.
                    </p>',
                'type' => 'content',
            ],
            [
                'page' => 'about',
                'content' => 'img/news-slide-full.jpg',
                'type' => 'image',
            ],
            [
                'page' => 'privacy-agreement',
                'content' => '<p>This privacy notice (the&nbsp;<strong>“Privacy Notice”</strong>) sets out the prerogatives of the AMSI Alumni Association (<strong>“</strong><strong>AAA</strong><strong>”</strong>) with respect to your personal data when you visit AAA’s alumni network (the&nbsp;<strong>“Platform”</strong>).</p>
                    <p>When you join the Platform, your personal data will be processed by the Platform’s lead administrator, i.e. AAA’s office of development and communication (the&nbsp;<strong>“Lead Administrator”</strong>) acting as data controller, and by the Platform’s developers, i.e. Hatsh (<strong>“Hivebrite”</strong>) acting as data processor under an agreement executed between Hivebrite and AAA. If you join community groups on the Platform, your data will also be processed by the group administrators.</p>
                    <p>AAA will always take all reasonable measures to keep your personal data safe. By using the Platform and accepting the terms and conditions of the User Agreement, you give your consent to the Lead Administrator and Hivebrite to store and process your personal data. This consent is a prerequisite which enables you to access the Platform.&nbsp;You may withdraw your consent at any time, by contacting AAA using the contact details provided below.</p>

                    <br>
                    <p><strong>1. Data collected by AAA</strong></p>
                    <p>When you create an account, AAA collects the following information:</p>
                    <ul>
                        <li>Your name;</li>
                        <li>Your email address;</li>
                        <li>The details of your education;&nbsp;</li>
                        <li>Your contact preferences; and</li>
                        <li>Your location.</li>
                    </ul>
                    <br>
                    <p>You agree to set up your profile using your real name and ascertaining that all other information contained in your profile is accurate and kept up to date. You may add details about yourself but&nbsp;</p>
                    <p>AAA recommends that you do not post personal data you are not willing to publicly share.</p>
                    <p>AAA may collect additional personal data you submit e.g. for the purposes of filling out forms or responding to surveys. AAA will also use cookies to identify you when you visit the Platform and log usage data including the content you click on.&nbsp;</p>
                    <p>When you interact with other users of the Platform, they may post information about you on the Platform.&nbsp;</p>
                    <p>As the Platform develops AAA may add new features which may require the collection of additional data.</p>

                    <br>
                    <p><strong>2. Use of your data</strong></p>
                    <p>AAA uses your data to:</p>
                    <ul>
                        <li>Allow you to engage in a virtual community, and to connect and network with other AAA’s alumni;</li>
                        <li>Keep you up to-date with the Platform’s developments and the Platform users’ news and activities;</li>
                        <li>Administer and monitor the efficient running of the Platform.</li>
                    </ul>
                    <br>

                    <p><strong>3. Sharing your data</strong></p>
                    <p>AAA will not sell, distribute or lease your personal data to third parties unless AAA has your prior written permission, or is required to do so by law.</p>

                    <br>
                    <p><strong>4. Links to other websites</strong></p>
                    <p>This privacy notice does not cover any links to other websites which appear on the Platform. AAA recommends that you read the privacy statements on any other websites you visit.</p>

                    <br>
                    <p><strong>5. Your rights</strong></p>
                    <p>You have the right to request:</p>
                    <ul>
                        <li>Access to your personal data, the correction or deletion of your personal data;</li>
                        <li>AAA to cease or limit the process of your personal data;</li>
                        <li>A copy of your personal data held by AAA in a portable format; and</li>
                    </ul>
                    <ul>
                        <li>Your account closure.</li>
                    </ul>
                    <p>If you intend to exercise any of these rights or if you have any complaints about the way your data has been handled, you can contact the Lead Administrator.</p>
                    <p>AAA retains your personal data as long as you have an account on the Platform. If your account is closed, your personal data should no longer be visible to others within 24 hours as from the time your account has been closed.&nbsp;</p>

                    <br>
                    <p><strong>6. Security</strong></p>
                    <p>AAA takes all reasonable measures to ensure that Platform is safe and secure. Nevertheless, AAA does not guarantee that the Platform’s security will never be breached and/or that the users’ data will not be subject to access, disclosure or destruction. You use the Platform at your own responsibility.</p>

                    <br>
                    <p><strong>7. Cross border data transfers</strong></p>
                    <p>By registering on the Platform, you acknowledge and agree that AAA may access and store your personal data anywhere in the world.</p>


                    <br>
                    <p><strong>8. Legal jurisdiction</strong></p>
                    <p>The use of the Platform is governed by the laws of Lebanon. Any dispute in connection with the Platform shall be exclusively settled by the Lebanese competent courts.</p>

                    <br>
                    <p><strong>9. Changes to this policy</strong></p>
                    <p>The Lead Administrator may change this policy by updating this page. AAA recommends that you &nbsp;check this page from time to time to ensure that you are updated with any changes.&nbsp;</p>

                    <br>
                    <p><strong>10. Contact details</strong></p>
                    <p>All communications shall be sent to&nbsp;xxx</p>
                    <p>&nbsp;</p>',
                'type' => 'content',
            ],
            [
                'page' => 'privacy-agreement',
                'content' => 'img/news-slide-full.jpg',
                'type' => 'image',
            ],
            [
                'page' => 'user-agreement',
                'content' => '<p><strong>USER AGREEMENT</strong></p>
                    <p>Welcome to the AMSI Alumni Association (<strong>“AAA”</strong>) alumni network. This online platform (the&nbsp;<strong>“Platform”</strong>) is made available as a networking and communications tool that allows you to:</p>
                    <ul>
                        <li>Connect with fellow alumni from around the world;</li>
                        <li>Network with like-minded thinkers, practitioners and professionals;</li>
                        <li>Engage with alumni to discover and support their work; and</li>
                        <li>Collaborate on initiatives to build cross-cultural understanding.</li>
                    </ul>
                    <br>
                    <p>By using this service you confirm that (i) you are at least 18 years old, (ii) you agree on the terms and conditions of this user agreement (the&nbsp;<strong>“Agreement”</strong>) and (iii) you are committed to the terms and conditions of this Agreement which is a legally binding document.</p>
                    <p>If you do not agree on any of the terms and conditions of this Agreement, please do not join the site, access or otherwise use the Platform.&nbsp;</p>
                    <br>
                    <p><strong>1. Purposes</strong></p>
                    <p>The Platform’s main purpose is to provide an enhanced online communication and networking tool to AAA’s alumni community.</p>

                    <br>
                    <p><strong>2. The office of development and communication</strong></p>
                    <p>The Platform managed by AAA’s <em>office of development and communication</em> (the&nbsp;<strong>“ODC”</strong>). Please contact the ODC for compliance queries.</p>

                    <br>
                    <p><strong>3. Use of the Platform</strong></p>
                    <p>AAA’s alumni network team shall use its best endeavours to ensure that, save for any hacking, misuse or the like of the Platform, the information available on the Platform is accurate.&nbsp;</p>
                    <p>For security purposes, please comply with the following when using the Platform:</p>
                    <ul>
                        <li>Set up your profile using your real name;</li>
                        <li>Make sure that the information used in your profile is accurate and always updated;</li>
                        <li>Do not breach any legal requirement applicable in your jurisdiction, e.g. the laws on privacy, libel, fraud, intellectual property and anti-spam;</li>
                        <li>Use the Platform in a courteous, honest, truthful and professional manner;</li>
                        <li>Respect other cultures, religions and values;</li>
                        <li>Respect intellectual property rights or copyrights of others;&nbsp;</li>
                        <li>Remember that material posted online is often difficult to entirely remove; therefore, do no post anything if you may regret its content later. Please bear in mind that any content you delete may continue to exist online to the extent that it has been shared or accessed by others.</li>
                    </ul>
                    <br>
                    <p>In light of the above, you agree not to:</p>
                    <ul>
                        <li>Create a profile using a false identity;</li>
                        <li>Create more than one profile;</li>
                        <li>Create a profile for a third party;</li>
                        <li>Share your password or let any person access your account;</li>
                        <li>Misrepresent your academic or professional details;</li>
                        <li>Act dishonestly or inappropriately;</li>
                        <li>Upload a content which may be considered as malicious, intimidating, vulgar, violent, obscene, libelous or discriminatory;</li>
                        <li>Upload any confidential information whose disclosure may be harmful or illegal;</li>
                        <li>Upload a content which contains viruses or other harmful code;</li>
                        <li>Disclose or share any information or content obtained from other users without their express consent. This includes but is not limited to personal email addresses, phone numbers and credit card details;</li>
                        <li>Harass, intimidate, bully, threaten or stalk another user;</li>
                        <li>Use the site to disseminate spam;</li>
                        <li>Use the site to perpetrate any form of fraud or criminal activity.</li>
                    </ul>
                    <br>
                    <p><strong>3. Privacy and confidentiality</strong></p>
                    <p>Your privacy and confidentiality are very important to us. We treat your personal information very seriously and commit to comply with all applicable data protection legislation.</p>
                    <p>We collect data when you register your account and as you use the Platform. This can include your personal data and your interaction with other users.</p>
                    <p>By registering on the Platform, you acknowledge and agree that AAA may access and store your personal information anywhere in the world. AAA shall take all available measures to keep your personal information secure. However, AAA cannot guarantee the Platform against any misuse, hacking or the like and cannot be held liable for any reason whatsoever in the event that your personal information is misused in any of these circumstances. You therefore acknowledge that you use the Platform at your own risk. You also remain the sole person who owns the personal information you provided to access the Platform. As such, you can ask AAA to correct or delete any personal information at any time.</p>
                    <br>
                    <p><strong>4. Copyright and sharing your content</strong></p>
                    <p>You are the sole owner of the content and information you post on the Platform. By registering on the Platform, you grant AAA a non-exclusive, royalty-free, worldwide license to use any content or information you post on the Platform.</p>
                    <p>The information you post on the Platform will be accessible to other users of the Platform. This information shall be related to your identity and profile. The Platform’s users, as well as people not registered on the Platform, can use the information you posted on the Platform. AAA is not responsible for any misuse or misappropriation of any content or information you post on the Platform.</p>
                    <p>When using the Platform, you may wish to join a group page. In this case, the administrator of the group page will have access to your personal data and you may receive communications from the group administrator&nbsp;<em>via</em>&nbsp;the Platform.</p>
                    <br>
                    <p><strong>5. Other users rights</strong></p>
                    <p>You have committed not to breach the rights of the Platform’s other users.&nbsp;AAA reserves the right to&nbsp;remove any content you post if it reasonably believes that such a content infringes the rights of another user of the Platform.</p>
                    <p>In the event that you post an information, a picture or the like on the Platform (a&nbsp;<strong>“Data”</strong>) you collected from users who are not registered on the Platform (a&nbsp;<strong>“Third Party Data”</strong>), you commit to obtain the Third Party Data’s approval prior to posting any Data. You therefore post any Third Party Data at your own responsibility and irrevocably commit to indemnify and hold AAA harmless against any action or complaint related to a post containing a Third Party Data.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                    <br>
                    <p><strong>6. Mobile devices</strong></p>
                    <p>If you use a mobile device to access the Platform, you will incur your carrier’s rates and fees. You give your consent for other users to synchronise the content of your posts to their mobile devices.</p>
                    <br>
                    <p><strong>7. Disclaimer, limitation of liability and indemnification</strong></p>
                    <p>AAA shall not be responsible for, nor shall it have any liability with respect to the use of the Platform. You use the Platform&nbsp;without AAA’s express or implied warranties of merchantability and fitness for purpose. AAA does not guarantee that the Platform will be secure, error-free or that it will function without interruption.</p>
                    <p>Save as expressly set out in this Agreement, AAA shall not bear any liability towards you or any other party for any reason whatsoever as a result of the utilisation of the Platform, including without limitation any liability to repair a prejudice of any nature whatsoever resulting from the loss of data, opportunities and/or reputation.&nbsp;</p>
                    <p>AAA cannot be held liable for third parties’ actions with respect to the Platform, and/or for the content or data uploaded by third parties on the Platform. You irrevocably release AAA, its directors, officers, managers, employees and agents from any claims and damages directly or indirectly connected with the above.&nbsp;You further acknowledge and agree that AAA has no obligation to monitor or take any action with respect to user content which you use on your own responsibility. AAA reserves however the right to discretionarily edit the site and delete any user content for any reason AAA deems appropriate.&nbsp;</p>
                    <p>In the event that AAA’s liability is engaged as a result of your actions or the content or information you uploaded on the Platform, you irrevocably commit to indemnify and hold AAA harmless from and against all damages, losses and expenses (including legal fees and cost) related thereto.</p>

                    <br>
                    <p><strong>8. Miscellaneous</strong></p>

                    <br>
                    <p><strong>8.1 Intellectual property</strong></p>
                    <p>You are not allowed to, and commit not to, use or register AAA’s name, logo or intellectual property rights.</p>

                    <br>
                    <p><strong>8.2 Links to other sites</strong></p>
                    <p>AAA does not endorse or accept responsibility for the content of any third party website. Links to other websites are used solely at your own risk.</p>

                    <br>
                    <p><strong>8.3 Use of cookies</strong></p>
                    <p>You acknowledge and agree that AAA may place cookies on your browser to (i) obtain information concerning your use of the Platform, (ii) facilitate your navigation on the Platform and (iii) store your preferences. You further agree that AAA may collect technical data concerning your usage of the Platform for the purposes of improving the services offered.</p>
                    <br>
                    <p><strong>9. Changes to this Policy or to the level of service</strong></p>
                    <p>AAA reserves the right to amend this Agreement from time to time, including the right to change, extend or reduce the scope of services AAA offers and the right to suspend or end all or part of the services AAA offers. Any changes AAA considers significant will be notified to you by email to your primary email address. You may review this Agreement from time to time in order to be updated with any amendments you have not received by email.</p>
                    <p>AAA does not provide a storage service and does not commit to permanently store or display any information or content you have posted on the Platform. You acknowledge and agree that AAA has no obligation to store your data and/or to provide you with copies of any of the content you have posted on the Platform.</p>
                    <br>
                    <p><strong>10. Termination</strong></p>
                    <p>This Agreement may be terminated by any party at any time. In the event that AAA deletes your account, you will be notified either by email or at the time you attempt to access the Platform.</p>
                    <p>All information you shared on the Platform can be re-shared by other members after the termination of this Agreement between us.</p>
                    <br>
                    <p><strong>12. Dispute resolution</strong></p>
                    <p>Disputes between you and the Platform are very unlikely to occur. However, any dispute in connection with the execution or interpretation of this Agreement shall be governed by the laws of Lebanon and settled by the competent Lebanese courts.</p>
                    <br>
                    <p><strong>13. Contacting Us</strong></p>
                    <p>All notices and communications to AAA shall be sent to xxxx</p>
                    <br>
                    <p>&nbsp;</p>',
                'type' => 'content',
            ],
            [
                'page' => 'user-agreement',
                'content' => 'img/news-slide-full.jpg',
                'type' => 'image',
            ],
        ];

        foreach ($data as $item){
            PageContent::create($item);
        }
    }
}
