<?php

namespace Database\Seeders;

use App\Models\Volunteer;
use Illuminate\Database\Seeder;

class VolunteerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Nisreen Shahin Class of 1998',
                'title' => 'Founder of Hatch Concept Studio',
                'details' => '<p>Volunteering her time with the AAA since May 2020, supporting with website design and development and digital advertising. Nisreen has also volunteered in Amsi Voices</p>',
                'order' => '1',
            ],
            [
                'name' => 'Anna Maria Aoun Class of 2004',
                'title' => 'Founder of Anna Tangles',
                'details' => '<p>Volunteering her time with the AAA since September, supporting with all its digital creative content.
<br/>Anna has also volunteered in Amsi Voices
</p>',
                'order' => '2',
            ],
            [
                'name' => 'Dana Saeed Class of 2005',
                'title' => 'Tech Guru @ Amsi',
                'details' => '<p>Volunteering her time with the AAA since January with all tech related activities</p>',
                'order' => '3',
            ],
            [
                'name' => 'Mansoor Sharaf Class of 2017',
                'title' => 'Student @ King’s College London',
                'details' => '<p>Volunteering his time with the AAA since September Supporting the AAA in all business developments.
<br/>Mansoor has also volunteered in Amsi Voices</p>',
                'order' => '4',
            ],
        ];


        foreach ($data as $item)
        {
            Volunteer::create($item);
        }
    }
}
