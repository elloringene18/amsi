<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Industry;
use App\Models\Job;
use App\Models\JobLocation;
use App\Models\User;
use App\Traits\CanCreateSlug;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;
use Spatie\Geocoder\Geocoder;

class JobSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Generator $faker, Job $model)
    {
        $this->faker = $faker;
        $this->model = $model;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $jobs = Job::get();

        foreach ($jobs as $job)
            $job->delete();

        // 'title','slug','company','industry_id','employment_type','experience','career_level','details','country_id','city_id'

        $employment_types = ['Full-time','Part-time','Contract'];
        $experience = ['Less than a year','1-2 years','3-5 year','5-7 years','8-10 years','More than 10 years'];
        $careers = ['Junior','Mid Level','Senior','Management'];

        for($x=1;$x<8;$x++){

            $country = Country::inRandomOrder()->first();
            $city = $country->cities()->inRandomOrder()->first();
            $industry = Industry::inRandomOrder()->first();
            $user = User::where('email','gene@thisishatch.com')->first();
            $job = $this->faker->jobTitle;

            Job::create([
                'title' => $job,
                'slug' => $this->generateSlug($job),
                'company' => $this->faker->company,
                'employment_type' => $employment_types[rand(0,2)],
                'experience' => $experience[rand(0,5)],
                'career_level' => $careers[rand(0,3)],
                'details' => '<p>'.$this->faker->paragraph('50').'</p>',
                'country_id' => $country->id,
                'city_id' => $city ? $city->id : null,
                'industry_id' => $industry->id,
                'user_id' => $user->id,
            ]);

            $location = JobLocation::where('location',($city ? $city->name.', ' : '').$country->name)->first();

            if($location){
                $location->total = $location->total + 1;
                $location->save();

            } else {
                $client = new \GuzzleHttp\Client();
                $geocoder = new Geocoder($client);
                $geocoder->setApiKey(config('geocoder.key'));
                $geocoder->setCountry(config('geocoder.country', 'AE'));
                $newLocation = $geocoder->getCoordinatesForAddress(($city ? $city->name.', ' : '').$country->name);

                JobLocation::create([
                    'location' => ($city ? $city->name.', ' : '').$country->name,
                    'lat' => $newLocation['lat'],
                    'lng' => $newLocation['lng'],
                    'total' => 1,
                ]);
            }
        }
    }
}
