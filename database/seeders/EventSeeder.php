<?php

namespace Database\Seeders;

use App\Models\Event;
use App\Traits\CanCreateSlug;
use Faker\Generator;
use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Generator $faker, Event $model)
    {
        $this->faker = $faker;
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [];

        for($x=0;$x<10;$x++){
            $data[] = ['title' => $this->faker->text(rand(10,30))];
        }

        foreach ($data as $item)
        {
            $product = [];
            $product['date'] = $this->faker->date('Y-m-d');
            $product['time'] = $this->faker->time('H:i');
            $product['location'] = $this->faker->address();
            $product['title'] = $item['title'];
            $product['slug'] = $this->generateSlug($item['title']);
            $product['content'] = '<p>'.$this->faker->paragraph('30').'</p><p>'.$this->faker->paragraph('40').'</p>';

            Event::create($product);
        }
    }
}
