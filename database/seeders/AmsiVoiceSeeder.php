<?php

namespace Database\Seeders;

use App\Models\PageContent;
use Illuminate\Database\Seeder;

class AmsiVoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'page','section','title','title_ar','content','content_ar','type'

        $data = [
            [
                'page' => 'amsi-voice-7th-conference',
                'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse porta, justo non scelerisque molestie, mauris augue cursus orci, ac suscipit lectus urna vel tellus. Phasellus rhoncus nisl mauris, nec condimentum elit molestie eu. Nunc bibendum finibus urna, vitae ultricies nisl molestie in. Maecenas facilisis non leo id lacinia. In vitae elit gravida, varius dui ut, eleifend nulla. Mauris enim felis, convallis ac velit in, vestibulum lacinia ex. Maecenas sed magna eu enim tristique auctor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus laoreet sapien massa, in dapibus massa posuere rhoncus.</p>
<p>Vivamus eleifend eget tellus id blandit. Nullam id convallis leo. Suspendisse commodo gravida orci nec auctor. Fusce pharetra vitae nisi ac ornare. Curabitur semper mauris a nisi hendrerit, quis suscipit quam lobortis. Proin massa libero, feugiat vitae lacinia nec, efficitur sit amet elit. Mauris vel pulvinar libero. Curabitur nec elit id enim posuere rhoncus vitae eu dui.</p>
<p>Etiam at sollicitudin nunc. Curabitur dignissim nec metus vulputate placerat. Fusce tincidunt ipsum nec erat pulvinar pulvinar. Proin convallis odio nec mollis feugiat. Sed sed ipsum faucibus, efficitur dolor nec, semper ligula. Donec vel interdum diam. Donec mollis nisl tellus, non iaculis magna maximus eu. Nam at turpis et ante aliquet egestas in ut leo. Cras orci leo, eleifend non dolor vel, placerat viverra nunc. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ullamcorper, nulla non eleifend tempus, nunc tellus porta nunc, in lobortis orci nulla sit amet ex. Fusce cursus tristique eros, non pulvinar diam pellentesque at. Maecenas condimentum pharetra tortor id cursus. Vestibulum cursus enim et ligula convallis convallis. Aenean iaculis, sapien vitae feugiat ultricies, diam magna ultrices risus, sit amet laoreet metus libero ut libero.</p>',
                'type' => 'content',
            ],
            [
                'page' => 'our-amsi-voices',
                'content' => '<p>Vestibulum consequat nibh at purus condimentum rhoncus. Cras efficitur lorem neque, ac tempus lectus varius ut. Nullam id enim tempus, imperdiet nulla sed, varius nulla. Donec eget ultricies erat. Sed consectetur magna ac augue pellentesque, in lobortis urna placerat. Integer volutpat hendrerit diam, at suscipit mauris. Vestibulum viverra ipsum non dui blandit, in placerat dolor tempus. Donec iaculis consectetur libero, accumsan ultrices lectus viverra vitae. Sed in neque commodo justo ullamcorper bibendum eget a sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi dictum elit at neque volutpat egestas. Mauris eleifend sollicitudin mi.</p>
<p>Proin in elit id nunc semper auctor. Nam at laoreet elit. Cras a fringilla sapien. Curabitur blandit vulputate dolor, vitae tincidunt est vestibulum in. Praesent feugiat eros nisi, eget tristique turpis lobortis eget. Sed efficitur elit non orci pretium, lobortis tincidunt massa finibus. Vestibulum porttitor leo non ullamcorper tempus. In a dictum felis, a porta purus. Vestibulum bibendum est nec turpis volutpat consequat. Donec vel sollicitudin lacus, ac sagittis sapien. Etiam quis porta quam. Cras sollicitudin, justo sed eleifend convallis, arcu nibh bibendum nisi, nec suscipit enim neque eu felis. Pellentesque dapibus felis in ligula placerat, ut sodales est blandit. Sed facilisis sapien orci, eget efficitur leo mattis ut. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum sit amet faucibus metus.</p>',
                'type' => 'content',
            ],
        ];

        foreach ($data as $item){
            PageContent::create($item);
        }
    }
}
