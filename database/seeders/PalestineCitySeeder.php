<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Country;
use Illuminate\Database\Seeder;

class PalestineCitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'name' => 'Gaza', 'lat' => '31.50161', 'long' => '34.46672' ],
            [ 'name' => 'East Jerusalem', 'lat' => '31.78336', 'long' => '35.23388' ],
            [ 'name' => 'Hebron', 'lat' => '31.52935', 'long' => '35.0938' ],
            [ 'name' => 'Nablus', 'lat' => '32.22111', 'long' => '35.25444' ],
            [ 'name' => 'Khān Yūnis', 'lat' => '31.34018', 'long' => '34.30627' ],
            [ 'name' => 'Jabalya', 'lat' => '31.5272', 'long' => '34.48347' ],
            [ 'name' => 'Ramallah', 'lat' => '31.89964', 'long' => '35.20422' ],
            [ 'name' => 'Bethlehem', 'lat' => '31.70487', 'long' => '35.20376' ],
            [ 'name' => 'Jericho', 'lat' => '31.86667', 'long' => '35.45' ],
            [ 'name' => 'Rafaḩ', 'lat' => '31.29722', 'long' => '34.24357' ],
            [ 'name' => 'Dayr al Balaḩ', 'lat' => '31.41834', 'long' => '34.34933' ],
            [ 'name' => 'Bayt Lāhyā', 'lat' => '31.5464', 'long' => '34.49514' ],
            [ 'name' => 'Battir', 'lat' => '31.7', 'long' => '35.11667' ],
            [ 'name' => 'Ţūlkarm', 'lat' => '32.31156', 'long' => '35.0269' ],
            [ 'name' => 'Qalkilya', 'lat' => '32.18966', 'long' => '34.97063' ],
            [ 'name' => 'Yuta', 'lat' => '31.4459', 'long' => '35.09443' ],
            [ 'name' => 'Ma‘ale Adummim', 'lat' => '31.7774', 'long' => '35.29875' ],
            [ 'name' => 'Al Bira', 'lat' => '31.91001', 'long' => '35.21645' ],
            [ 'name' => 'Beit Hanun', 'lat' => '31.5353', 'long' => '34.53579' ],
            [ 'name' => 'Nuseirat', 'lat' => '31.44861', 'long' => '34.3925' ],
        ];

        $country = Country::where('name','Palestine')->first();

        foreach ($data as $city){
            $city['country_id'] = $country->id;
            City::create($city);
        }
    }
}
