<?php

namespace Database\Seeders;

use App\Models\Involve;
use Illuminate\Database\Seeder;

class InvolveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'Mentorship',
                'details' => 'You have the opportunity to share your professional and personal experience and expertise with current students and other alumni.',
            ],
            [
                'title' => 'Alumni networking events, reunions and workshops',
                'details' => 'Get connected with fellow Alumni at one of our networking events, reunions, or career workshops!',
            ],
            [
                'title' => 'Sponsorship of events',
                'details' => 'Become an individual or corporate sponsor for one of the many events AMSI hosts throughout the year. These events help grow our alumni network and build support for our students.',
            ],
            [
                'title' => 'Support the AAA',
                'details' => 'Volunteering your time and skills to the Alumni Association.',
            ],
        ];

        foreach ($data as $item)
        {
            Involve::create($item);
        }
    }
}
