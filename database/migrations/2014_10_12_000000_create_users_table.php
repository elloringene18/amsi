<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('prefix')->nullable();
            $table->string('firstName')->nullable();
            $table->string('lastName')->nullable();
            $table->string('email')->unique();
            $table->string('gender')->nullable();
            $table->string('photo')->nullable();
            $table->string('cover')->nullable();
            $table->string('resume')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('campus')->nullable();
            $table->string('year_graduated')->nullable();
            $table->dateTime('birthDate')->nullable();
            $table->string('mobileNumber')->nullable();
            $table->string('occupation')->nullable();
            $table->longText('bio')->nullable();
            $table->string('font_color')->default('#4d4d4d');
            $table->tinyInteger('is_active')->default(1);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->tinyInteger('is_admin')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
