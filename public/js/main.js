function myFunction(x) {
  x.classList.toggle("change");
    $('.menus').first().toggleClass('active');
}

$('#loginForm').on('submit', function(e){
    $('#login-errors').empty();
    var form = $(this);

    e.preventDefault();
    e.stopPropagation();

    data = form.serialize();
    url = form.attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function(response){

            if(response.success)
                window.location.href = baseUrl;
            else
                $('#login-errors').append('<p>'+response.message+'</p>');

        },
        statusCode: {
            401: function() {
                window.location.href = baseUrl+'/login'; //or what ever is your login URI
            },
            422: function(response) {
                $('#login-errors').html(response);
            }
        },
        complete : function (event,error){
            // errs = JSON.parse(event.responseText).errors;
            // console.log(errs);
            // Object.keys(errs).map(function(objectKey, index) {
            //     var value = errs[objectKey];
            //     $('#login-errors').append('<p>'+value+'</p>');
            // });
        }
    });
});

$('#profile-badge').on('click',function (e) {
   e.preventDefault();
   $('#user-menu').toggleClass('active');
});
