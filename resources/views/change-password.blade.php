@extends('master')

@section('css')
    <style>
        #register {
            padding: 130px 0;
        }
        #register .row {
            margin-bottom: 20px;
        }
    </style>
@endsection

@section('content')
    <section id="register">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    @if(Session::has('error'))
                        <div class="alert alert-danger">{{ Session::get('error') }}</div>
                    @elseif(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }} <br/>
                        </div>
                    @endif
                    <form method="POST" action="{{ url('change-password') }}">
                    @csrf
                        <h3>Change Password</h3>
                        <br/>
                        <!-- Password -->
                        New Password:
                        <input type="password" class="form-control" name="password" required>
                        <br/>
                        Confirm New Password:
                        <input type="password" class="form-control" name="password-confirm" required>

                        <br/>
                        <input type="submit" class="form-control" value="Update Password">
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>

    </script>
@endsection
