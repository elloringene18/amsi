<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('public/') }}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('public/') }}/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{ asset('public/') }}/css/main.css">
    <link rel="stylesheet" href="{{ asset('public/') }}/css/home.css">

    <script src="{{ asset('public/') }}/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <style>
        .swiper-container {
            width: 100%;
            height: auto;
        }
        .news {
            width: 100%;
            overflow: hidden;
        }
    </style>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                <a href="{{ url('/') }}"><img src="{{ asset('public/') }}/img/mawakeb-alumni-logo.png" alt="mawakeb alumni logo" id="main-logo"/></a>
            </div>
        </div>
    </div>
</header>

<section id="slide">
    <div class="container-fluid">
        <div class="row">
            <!-- Slider main container -->
            <div class="swiper-container">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <div class="swiper-slide home-slide" style="background-image:url({{ asset('public/') }}/img/home-slide.jpg);">
                        <div class="overlay"></div>
                        <div class="content">
                            <div class="container">
                                <h2 class="main">COMING <span class="yellow">SOON</span></h2>
                                <h3 class="sub">#AMSI_Alumni</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- If we need pagination -->
                <div class="swiper-pagination"></div>

                <!-- If we need navigation buttons -->
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>

                <!-- If we need scrollbar -->
                <!--              <div class="swiper-scrollbar"></div>-->
            </div>
        </div>
    </div>
</section>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                © 2021 Al mawakeb. all rights reserved
            </div>
            <div class="col-md-3  text-center">
                Designed and developed by <br/>
                <a href="http://thisishatch.com/" target="_blank" class="font-weight-bolder">
                    <img src="{{asset('public/img/hatch-logo.png')}}" style="margin-top: 10px;">
                </a>.
            </div>
        </div>
    </div>
</footer>

<script src="{{ asset('public/') }}/js/vendor/jquery-1.11.2.min.js"></script>

<script src="{{ asset('public/') }}/js/vendor/bootstrap.min.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

<script src="{{ asset('public/') }}/js/plugins.js"></script>
<script src="{{ asset('public/') }}/js/main.js"></script>

<script>

    var mySwiper = new Swiper('.swiper-container', {
        // Optional parameters
        loop: false,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        // And if we need scrollbar
        scrollbar: {
            el: '.swiper-scrollbar',
        },
    });

    var swiper2 = new Swiper('.news', {
        slidesPerView: 3,
        spaceBetween: 30,
//          centeredSlides: true,
        loop: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },breakpoints: {
            // when window width is >= 320px
            300: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            620: {
                slidesPerView: 2,
                spaceBetween: 20
            },
            // when window width is >= 640px
            991: {
                slidesPerView: 3,
                spaceBetween: 40
            }
        }
    });
</script>
</body>
</html>

