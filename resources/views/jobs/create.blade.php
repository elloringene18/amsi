@extends('master')

@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public') }}/css/profile.css">
@endsection

@section('content')
    <!-- partial -->
<div class="container">
    <div class="row">
        <br/>
        <div class="col-md-12 colm">
            <a href="{{ url('/my-jobs') }}">< Back to Jobs</a>
        </div>
        <br/>
    </div>
    <div class="row">
        <br/>
        <div class="col-md-12 colm">
            <strong>Add a Job</strong>
        </div>
        <br/>
    </div>

    <form class="forms-sample" id="storeJob" action="{{ url('store-job') }}" method="post" enctype="multipart/form-data">
        <input type="hidden" value="{!! csrf_token() !!}" name="_token">
        <br/>
        <div class="row">
            <div class="col-md-12 colm">
                <div class="card">
                    <div class="card-body">
                        @if(Session::has('success'))
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert-success alert">{{ Session::get('success') }}</div>
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Title</label>
                                    <input type="text" class="form-control" id="exampleInputNamea1" required placeholder="Job Title" name="title">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Company</label>
                                    <input type="text" class="form-control" id="exampleInputNamea1" required placeholder="Company Name" name="company">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Industry</label>
                                    <select name="industry_id" id="" class="form-control">
                                        @foreach($industries as $industry)
                                            <option value="{{ $industry->id }}">{{ $industry->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Career Level</label>
                                    <select name="career_level" id="" class="form-control">
                                        @foreach($careers as $car)
                                            <option value="{{ $car }}">{{ $car }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Country</label>
                                    <select name="country_id" id="country" class="form-control">
                                        @foreach($countries as $country)
                                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">City</label>
                                    <select name="city_id" id="city" class="form-control">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Employment Type</label>
                                    <select name="employment_type" id="" class="form-control">
                                        @foreach($employment_types as $type)
                                            <option value="{{ $type }}">{{ $type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputNamea1">Experience</label>
                                    <select name="experience" id="" class="form-control">
                                        @foreach($experience as $exp)
                                            <option value="{{ $exp }}">{{ $exp }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group content">
                                    <label for="exampleInputEmail3">Details</label>
                                    <input type="hidden" name="details" value="" required id="details"/>
                                    <div class="summernote">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br/>
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <button type="submit" class="purple-bt">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <br/>
    <br/>
</div>

@endsection

@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <script>
        $('#datetimepicker4').datepicker();


        $('#country').on('change',function (){
            id = $(this).val();
            $.ajax({
                type: "GET",
                url: baseUrl+"/get-cities-for-country/"+id,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response){
                    console.log(response);
                    $('#city').find('option').remove();


                    Object.keys(response).map(function(objectKey, index) {
                        var value = response[objectKey];
                        $('#city').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('#country').trigger('change');

        $('.summernote').summernote({
            placeholder: 'Add content here...',
            tabsize: 2,
            height: 300,
            callbacks: {
                onPaste: function (e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    document.execCommand('insertText', false, bufferText);
                }
            },
            fontSizes: ['8', '9', '10', '11', '12', '14', '16','18','20','22','24','28','30','32','34','36','38'],
            toolbar: [
                ['fontsize', ['fontsize']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['picture', 'hr','video']],
                ['table', ['table']],
                ['view', ['codeview']],
                ['link', ['link']],
            ],
            popover: {
                image: [
                    ['custom', ['imageAttributes']],
                    ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ],
            },
            imageAttributes:{
                icon:'<i class="note-icon-pencil"/>',
                removeEmpty:false, // true = remove attributes | false = leave empty if present
                disableUpload: false // true = don't display Upload Options | Display Upload Options
            }

        });

        $('.summernote').on('summernote.change', function(we, contents, $editable) {
            $(this).closest('.form-group').find('input').val(contents);
        });

        $('#storeJob').on('submit', function(e) {

            if (!$('#details').val()) {
                alert('Please add job details.');

                // cancel submit
                e.preventDefault();
            }
        });
    </script>

@endsection
