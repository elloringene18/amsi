@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/jobs.css">
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">
    <link rel="stylesheet" href="{{ asset('public') }}/css/profile.css">

    <style>
        .swiper-container {
            width: 100%;
            height: auto;
        }
        .item {
            float: left;
        }
    </style>
@endsection

@section('content')

    <section id="content">
        <div class="container">
            <div class="row other-news">
                <br/>
                <br/>
                <a href="{{ url('/my-jobs') }}">< Back to Jobs</a>
                <br/>
                <hr/>
                <div class="row">
                    <div class="col-md-12 colm">
                        <div class="col-md-6 mb-3">
                            <strong>Title:</strong> {{ $job->title }}
                        </div>
                        <div class="col-md-6">
                            <strong>Company:</strong> {{ $job->company }}
                        </div>
                        <div class="col-md-6">
                            <strong>Location:</strong> {{ $job->country->city ? $job->country->city. ', ' : '' }}{{ $job->country->name }}
                        </div>
                        <div class="col-md-6">
                            <strong>Industry:</strong> {{ $job->industryName }}
                        </div>
                        <div class="col-md-6">
                            <strong>Type:</strong> {{ $job->employment_type }}
                        </div>
                        <div class="col-md-6">
                            <strong>Experience:</strong> {{ $job->experience }}
                        </div>
                        <div class="col-md-6">
                            <strong>Career Level:</strong> {{ $job->career_level }}
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-12 colm">
                        <strong>Applicants</strong>
                        <table class="table">
                            <tr>
                                <td>Name</td>
                                <td>Occupation</td>
                                <td>Current Location</td>
                                <td>Profile</td>
                            </tr>

                            @if(count($data))
                                @foreach($data as $user)
                                    <tr>
                                        <td>{{$user->user->firstName}} {{$user->user->lastName}}</td>
                                        <td>{{$user->user->occupation}}</td>
                                        <td>{{$user->user->city ? $user->user->city.', ' : ''}}{{$user->user->country}}</td>
                                        <td><a target="_blank" href="{{ url('directory/'.$user->user->id) }}">View Profile</a> </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">No Applicants at the moment.</td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            loop: false,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        });

        function vCenter(){
            $('.vcenter').each(function(){
                ht = $(this).height()/2;
                $(this).css('margin-top','-'+ht+'px');
            });
        }
        vCenter();
    </script>
@endsection
