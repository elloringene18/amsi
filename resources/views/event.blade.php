@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/news.css">
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">

    <style>
        .swiper-container {
            width: 100%;
            height: auto;
        }
        .news {
            width: 100%;
            overflow: hidden;
        }

        #slide {
            position: relative;
        }
    </style>
@endsection

@section('content')

    <section id="slide">
        <div class="container-fluid">
            <div class="row">
                <img src="{{ $article->photoUrl }}" width="100%">

                <div class="overlay"></div>
                <div class="content">
                    <div class="container">
                        <h3 class="sub">{{ $article->title }}</h3>
                        <h4 class="sub">EVENT</h4>
                    </div>
                {{--</div>--}}
                {{--<!-- Slider main container -->--}}
                {{--<div class="swiper-container">--}}
                    {{--<!-- Additional required wrapper -->--}}
                    {{--<div class="swiper-wrapper">--}}
                        {{--<!-- Slides -->--}}
                        {{--<div class="swiper-slide home-slide">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- If we need pagination -->--}}
                    {{--<!-- If we need navigation buttons -->--}}
                    {{--<!-- If we need scrollbar -->--}}
                    {{--<!--              <div class="swiper-scrollbar"></div>-->--}}
                {{--</div>--}}
            </div>
        </div>
    </section>

    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    @if(Session::has('success'))
                        <div class="success alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <p><strong>Date:</strong> {{ \Carbon\Carbon::parse($article->date)->format('d m Y') }}</p>
                    <p><strong>Time:</strong> {{ $article->time }}</p>
                    <p><strong>Location:</strong> {{ $article->location }}</p>
                    <br/>
                    {!! $article->content !!}
                    <hr/>


                    @if(Auth::user()->events()->where('event_id', $article->id)->count())
                        <p>YOU ARE REGISTERED TO THIS EVENT</p>
                        @if(\Carbon\Carbon::now() >= $article->date)
                            <strong>EVENT REGISTRATION HAS PASSED</strong>
                        @else
                            <a href="{{ url('delete-event/'.$article->id) }}" class="purple-bt">CANCEL REGISTRATION</a>
                        @endif

                    @else
                        @if(\Carbon\Carbon::now() >= $article->date)
                            <strong>EVENT REGISTRATION HAS PASSED</strong>
                        @else
                            <a href="{{ url('add-event/'.$article->id) }}" class="purple-bt">REGISTER FOR THIS EVENT</a>
                        @endif

                    @endif
                    <hr/>

                    <h2 class="heading">OTHER EVENTS</h2>
                    <br/>
                </div>
            </div>
            <div class="row other-news">
                @foreach($similar as $item)
                    <a href="{{ url('event/'.$item->slug) }}">
                        <div class="col-md-4 new-item">
                            <div class="wrap" style="background-image: url({{ $item->thumbnailUrl }})">
                            </div>
                            <br/>
                            <h3 class="sub">{{ $item->title }}</h3>
                            <h4 class="sub">Event</h4>
                        </div>
                    </a>
                @endforeach

            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            loop: false,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        });

        function vCenter(){
            $('.vcenter').each(function(){
                ht = $(this).height()/2;
                $(this).css('margin-top','-'+ht+'px');
            });
        }
        vCenter();
    </script>
@endsection
