@extends('master')

@section('css')
    <style>
        #register {
            padding: 130px 0;
        }
        #register .row {
            margin-bottom: 20px;
        }
    </style>
@endsection

@section('content')
    <section id="register">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    @if(Session::has('error'))
                        <div class="alert alert-danger">{{ Session::get('error') }}</div>
                    @elseif(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }} <br/>
                            Please click <a href="{{url('login')}}">here</a> to login.
                        </div>
                    @endif

                    <div class="mb-4 text-sm text-gray-600">
                        {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
                        <br/>
                        <br/>
                    </div>

                    @if (session('status') == 'verification-link-sent')
                        <div class="mb-4 font-medium text-sm text-green-600">
                            {{ __('A new verification link has been sent to the email address you provided during registration.') }}
                            <br/>
                            <br/>
                        </div>
                    @endif

                    <div class="mt-4 flex items-center justify-between">
                    <form method="POST" action="{{ route('verification.send') }}">
                        @csrf

                            <div>
                                <x-button>
                                    {{ __('Resend Verification Email') }}
                                </x-button>
                            </div>
                        </form>

                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <input type="submit" class="form-control" value="Logout">
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>

    </script>
@endsection
