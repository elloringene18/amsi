@extends('master')

@inject('locationService', 'App\Services\LocationService')
<?php $countries = $locationService->getAllCountriesWithId(); ?>

@section('css')
    <style>
        #register {
            padding: 30px 0;
        }
        #register .row {
            margin-bottom: 20px;
        }
    </style>
@endsection

@section('content')
    <section id="register">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                        @elseif(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }} <br/>
                            </div>
                        @endif

                        @if(Session::has('linkedInUser'))
                            <?php $linkedInUser = Session::get('linkedInUser'); ?>
                        @endif

                        <form method="POST" action="{{ url('register-user') }}">

                        @if(isset($linkedInUser))
                            <input type="hidden" name="oauth_id" value="{{$linkedInUser->id}}">
                            <input type="hidden" name="oauth_type" value="linkedin">
                        @endif

                        @csrf

                        @if(isset($linkedInUser))
                            <img src="{{$linkedInUser->avatar}}">
                            <input type="hidden" name="photo" value="{{$linkedInUser->avatar}}">
                                <br/>
                                <br/>
                        @endif

                        <!-- Name -->
                            <?php
                            $prefixes = ['Mr','Ms','Mrs','Dr','Engr','Prof']
                            ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Prefix:</label>
                                    <select name="prefix" class="form-control">
                                        @foreach($prefixes as $prefix)
                                            <option {{ isset($user) ? ($user['prefix'] == $prefix ? 'selected' : false) : '' }} value="{{$prefix}}">{{$prefix}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <x-label for="occupation" :value="__('Occupation')" />
                                    @if(isset($user))
                                        <input id="occupation" type="text" class="form-control" name="occupation" required value="{{ $user['occupation'] }}">
                                    @else
                                        <input id="occupation" type="text" class="form-control" name="occupation" required value="">
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <x-label for="firstName" :value="__('First Name')" />
                                    @if(isset($linkedInUser))
                                        <input id="firstName" type="text" class="form-control" name="firstName" required value="{{ $linkedInUser->user['firstName']['localized']['en_US'] }}">
                                    @elseif(isset($user))
                                        <input id="firstName" type="text" class="form-control" name="firstName" required value="{{ $user['firstName'] }}">
                                    @else
                                        <input id="firstName" type="text" class="form-control" name="firstName" required value="">
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <x-label for="lastName" :value="__('Last Name')" />
                                    @if(isset($linkedInUser))
                                        <input id="lastName" type="text" class="form-control" name="lastName" required value="{{ $linkedInUser->user['lastName']['localized']['en_US'] }}">
                                    @elseif(isset($user))
                                        <input id="lastName" type="text" class="form-control" name="lastName" required value="{{ $user['lastName'] }}">
                                    @else
                                        <input id="lastName" type="text" class="form-control" name="lastName" required value="">
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <x-label for="email" :value="__('Email')" />

                                    @if(isset($linkedInUser))
                                        <input id="email" type="text" class="form-control" name="email" required value="{{ $linkedInUser->user['email']['localized']['en_US'] }}" {{ isset($linkedInUser) ? 'readonly' : '' }}>
                                    @elseif(isset($user))
                                        <input id="email" type="text" class="form-control" name="email" required value="{{ $user['email'] }}">
                                    @else
                                        <input id="email" type="text" class="form-control" name="email" required value="">
                                    @endif

                                </div>
                                <div class="col-md-6">
                                    <x-label for="gender" :value="__('Gender')" />
                                    <select name="gender" class="form-control">
                                        <option {{ isset($user) ? ($user['gender'] == 'Male' ? 'selected' : false ) : '' }} value="Male">Male</option>
                                        <option {{ isset($user) ? ($user['gender'] == 'Female' ? 'selected' : false ) : '' }} value="Female">Female</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Email Address -->
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Current Location (Country):</label>
                                    <select name="country" class="form-control" id="country">
                                        @foreach($countries as $country)
                                            <option {{ isset($user) ? ($user['country'] == $country->name ? 'selected' : false ) : '' }} data-id="{{$country->id}}" value="{{$country->name}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Current Location (City):</label>
                                    <select name="city" class="form-control" id="city"></select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <x-label for="campus" :value="__('Campus')" />
                                    <?php
                                    $campuses = [
                                        'Al Mawakeb School-Al Garhoud',
                                        'International School of Arts and Sciences',
                                        'Al Mawakeb School-Al Barsha',
                                        'Al Mawakeb School-Al Khawaneej'
                                    ]
                                    ?>
                                    <select name="campus" class="form-control">
                                        @foreach($campuses as $campus)
                                            <option {{ isset($user) ? ($user['campus'] == $campus ? 'selected' : false ) : '' }} value="{{$campus}}">{{$campus}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <x-label for="year_graduated" :value="__('Year Graduated')" />
                                    <select name="year_graduated" class="form-control">
                                        @for($x=1979;$x<=\Carbon\Carbon::now()->format('Y');$x++)
                                            <option {{ isset($user) ? ($user['year_graduated'] == $x ? 'selected' : false ) : '' }} value="{{$x}}">{{$x}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>

                            <!-- Confirm Password -->
                            <div class="row">
                                <div class="col-md-6">
                                    <x-label for="password" :value="__('Password')" />
                                    <x-input id="password" class="form-control"
                                             type="password"
                                             name="password"
                                             required autocomplete="new-password" />
                                </div>
                                <div class="col-md-6">
                                    <x-label for="password_confirmation" :value="__('Confirm Password')" />
                                    <x-input id="password_confirmation" class="form-control"
                                             type="password"
                                             name="password_confirmation" required />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                   <label>Mobile Number</label>
                                    @if(isset($user))
                                        <input id="email" type="text" class="form-control" name="mobileNumber" required value="{{ $user['mobileNumber'] }}">
                                    @else
                                        <input id="email" type="text" class="form-control" name="mobileNumber" required value="">
                                    @endif
                                </div>
                                <div class="col-md-6">
                                   <label>Do you currently have a child enrolled in Mawakeb school?</label>
                                    <select name="has_child" class="form-control">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                <hr/>
                                   <label>How would you like to get involved? (Select all that apply)</label>
                                    <br/>
                                    <br/>
                                    <?php $involves = \App\Models\Involve::get(); ?>
                                    <div class="row">
                                        @foreach($involves as $involve)
                                            <div class="col-md-6">
                                                <input type="checkbox" name="involvement[]" value="{{$involve->id}}" id="vol-{{$involve->id}}">
                                                <label for="vol-{{$involve->id}}">{{$involve->title}}</label><br/>
                                                {{$involve->details}}
                                                <br/>
                                                <br/>
                                            </div>
                                        @endforeach
                                    </div>

                                <hr/>
                                </div>
                            </div>

                            <div class="flex items-center justify-end mt-4">
                                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                                    {{ __('Already registered?') }}
                                    <br/>
                                    <br/>
                                </a>

                                <x-button class="form-control">
                                    {{ __('Register') }}
                                </x-button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </section>
@endsection

@section('js')
    <script>
        $('#country').on('change',function (){
            id = $(this).find('option:selected').attr('data-id');

            $.ajax({
                type: "GET",
                url: baseUrl+"/get-cities-for-country/"+id,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response){
                    console.log(response);
                    $('#city').find('option').remove();


                    Object.keys(response).map(function(objectKey, index) {
                        var value = response[objectKey];
                        $('#city').append('<option value="'+value.name+'">'+value.name+'</option>');
                    });
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('#country').trigger('change');
    </script>
@endsection


