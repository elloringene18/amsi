@extends('master')

@section('css')
    <style>
        #register {
            padding: 130px 0;
        }
        #register .row {
            margin-bottom: 20px;
        }
    </style>
@endsection

@section('content')
    <section id="register">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    @if(Session::has('error'))
                        <div class="alert alert-danger">{{ Session::get('error') }}</div>
                    @elseif(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }} <br/>
                            Please click <a href="{{url('login')}}">here</a> to login.
                        </div>
                    @endif
                    <div class="mb-4 text-sm text-gray-600">
                        {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
                        <br/>
                        <br/>
                    </div>

                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4" :status="session('status')" />

                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />

                    <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <!-- Email Address -->
                        <div>
                            <br/>
                            <x-label for="email" :value="__('Email')" />

                            <x-input id="email" class="form-control" type="email" name="email" :value="old('email')" required autofocus />
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            <br/>
                            <input type="submit" class="form-control" value="Submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>

    </script>
@endsection
