@extends('master')

@section('css')
    <style>
        #register {
            padding: 130px 0;
        }
        #register .row {
            margin-bottom: 20px;
        }
    </style>
@endsection

@section('content')
    <section id="register">
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    @if(Session::has('error'))
                        <div class="alert alert-danger">{{ Session::get('error') }}</div>
                    @elseif(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }} <br/>
                            Please click <a href="{{url('login')}}">here</a> to login.
                        </div>
                    @endif
                        <div class="mb-4 text-sm text-gray-600">
                            {{ __('This is a secure area of the application. Please confirm your password before continuing.') }}
                            <br/>
                            <br/>
                        </div>

                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />

                        <form method="POST" action="{{ route('password.confirm') }}">
                        @csrf

                        <!-- Password -->
                            <div>
                                <x-label for="password" :value="__('Password')" />

                                <x-input id="password" class="block mt-1 w-full"
                                         type="password"
                                         name="password"
                                         required autocomplete="current-password" />
                            </div>

                            <div class="flex justify-end mt-4">
                                <x-button>
                                    {{ __('Confirm') }}
                                </x-button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>

    </script>
@endsection
