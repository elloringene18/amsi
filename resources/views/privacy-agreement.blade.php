@extends('master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $pageContent = $contentService->getPageContent('privacy-agreement'); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/news.css">
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">

    <style>
        .swiper-container {
            width: 100%;
            height: auto;
        }
        .news {
            width: 100%;
            overflow: hidden;
        }
        p {
            margin-bottom: 20px;
        }
        h3 {
            margin-bottom: 30px;
            text-transform: uppercase;
            font-weight: 900;
        }
    </style>
@endsection

@section('content')

    <section id="slide">
        <div class="container-fluid">
            <div class="row">
                <!-- Slider main container -->
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide home-slide" style="background-image:url({{ asset('public/'.$pageContent['image']) }});">
                            <div class="overlay"></div>
                            <div class="content">
                                <div class="container">
                                    <h3 class="sub">PRIVACY AGREEMENT</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- If we need pagination -->
                    <!-- If we need navigation buttons -->
                    <!-- If we need scrollbar -->
                    <!--              <div class="swiper-scrollbar"></div>-->
                </div>
            </div>
        </div>
    </section>

    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {!! $pageContent['content'] !!}
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            loop: false,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        });

        function vCenter(){
            $('.vcenter').each(function(){
                ht = $(this).height()/2;
                $(this).css('margin-top','-'+ht+'px');
            });
        }
        vCenter();
    </script>
@endsection

