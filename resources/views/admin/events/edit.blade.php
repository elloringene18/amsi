@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit Event</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/events/update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{ $item->id }}" name="id">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Title</label>
                                            <input value="{{$item->title}}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Name" name="title">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Content</label>
                                            <input type="hidden" name="content" value="{{ $item->content }}"/>
                                            <div class="summernote">
                                                {!! $item->content !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Email Content</label>
                                            <input type="hidden" name="email_content" value="{{ $item->email_content }}"/>
                                            <div class="summernote">
                                                {!! $item->email_content !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Date</label>
                                            <input type="text" id='datetimepicker4' class="form-control" placeholder="Name" name="date" value="{{$item->date}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Time</label>
                                            <input type="text" class="form-control" placeholder="13:00" name="time" value="{{$item->time}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Location</label>
                                            <input type="text" class="form-control" placeholder="Dubai, United Arab Emirates" name="location" value="{{$item->location}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Photo</label>
                                            @if($item->photo)
                                                <br/>
                                                <img src="{{asset('public/'.$item->photo)}}" width="200">
                                                <br/>
                                                <br/>
                                                <label>Delete photo: <input type="checkbox" name="delete_photo" value="true"></label>
                                                <br/>
                                                <br/>
                                            @endif
                                            <input type="file" class="form-control" name="photo" data-lang="en">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $('#datetimepicker4').datepicker();
    </script>
@endsection
