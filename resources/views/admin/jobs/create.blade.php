@extends('admin.partials.master')

@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Add a Job</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/jobs/store') }}" method="post" enctype="multipart/form-data" id="storeJob">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Title</label>
                                            <input type="text" class="form-control" id="exampleInputNamea1" required placeholder="Job Title" name="title">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Company</label>
                                            <input type="text" class="form-control" id="exampleInputNamea1" required placeholder="Company Name" name="company">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Industry</label>
                                            <select name="industry_id" id="" class="form-control">
                                                @foreach($industries as $industry)
                                                    <option value="{{ $industry->id }}">{{ $industry->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Details</label>
                                            <input type="hidden" name="details" value="" id="details"/>
                                            <div class="summernote">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Country</label>
                                            <select name="country_id" id="country" class="form-control">
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">City</label>
                                            <select name="city_id" id="city" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Employment Type</label>
                                            <select name="employment_type" id="" class="form-control">
                                                @foreach($employment_types as $type)
                                                    <option value="{{ $type }}">{{ $type }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Experience</label>
                                            <select name="experience" id="" class="form-control">
                                                @foreach($experience as $exp)
                                                    <option value="{{ $exp }}">{{ $exp }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputNamea1">Career Level</label>
                                            <select name="career_level" id="" class="form-control">
                                                @foreach($careers as $car)
                                                    <option value="{{ $car }}">{{ $car }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $('#datetimepicker4').datepicker();


        $('#country').on('change',function (){
            id = $(this).val();
            $.ajax({
                type: "GET",
                url: baseUrl+"/get-cities-for-country/"+id,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response){
                    console.log(response);
                    $('#city').find('option').remove();


                    Object.keys(response).map(function(objectKey, index) {
                        var value = response[objectKey];
                        $('#city').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('#country').trigger('change');


        $('#storeJob').on('submit', function(e) {

            if (!$('#details').val()) {
                alert('Please add job details.');

                // cancel submit
                e.preventDefault();
            }
        });
    </script>

@endsection
