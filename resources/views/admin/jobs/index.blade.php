@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Jobs</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="dataTable">
                                    <tr>
                                        <th onclick="sortTable(0)">Title</th>
                                        {{--<th onclick="sortTable(0)">Company</th>--}}
                                        {{--<th onclick="sortTable(0)">Industry</th>--}}
                                        <th onclick="sortTable(0)">Type</th>
                                        <th onclick="sortTable(0)">Country</th>
                                        {{--<th onclick="sortTable(0)">City</th>--}}
                                        <th onclick="sortTable(0)">Posted On</th>
                                        <th onclick="sortTable(0)">Actions</th>
                                    </tr>
                                    @foreach($data as $item)
                                        <tr>
                                            <td>{{ $item->title }}</td>
                                            {{--<td>{{ $item->company }}</td>--}}
                                            {{--<td>{{ $item->industryName }}</td>--}}
                                            <td>{{ $item->employment_type }}</td>
                                            <td>{{ $item->country->name }}</td>
{{--                                            <td>{{ $item->city ? $item->city->name : '' }}</td>--}}
                                            <td>{{ $item->created_at }}</td>
                                            <td>
                                                <a href="{{ URL('admin/jobs/'.$item->id) }}">Edit</a>
                                                |
                                                <a href="{{ URL('admin/jobs/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card pagi">
                    <div class="card">
                        <div class="card-body">
                            {{ $data->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
