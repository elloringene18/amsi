@extends('admin.partials.master')

@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit Job Email</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/jobs/email/update') }}" method="post" enctype="multipart/form-data" id="">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">
                <input type="hidden" value="jobs-email" name="page">
                <input type="hidden" value="html" name="type">

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                                        </div>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group content">
                                            <label for="exampleInputEmail3">Details</label>
                                            @if(isset($data))
                                                <input type="hidden" name="content" value="{{ $data->content }}" id="details" />
                                                <div class="summernote">
                                                    {!! $data->content !!}
                                                </div>
                                            @else
                                                <input type="hidden" name="content" value="" id="details" />
                                                <div class="summernote">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
@endsection
