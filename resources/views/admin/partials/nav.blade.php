<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{url('/')}}" target="_blank">
                <i class="menu-icon mdi mdi-home"></i>
                <span class="menu-title">Visit Site</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#home-sliders" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-account"></i>
                <span class="menu-title">Home Sliders</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="home-sliders">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/home-sliders') }}">
                            <i class="menu-icon mdi mdi-eye"></i>
                            <span class="menu-title">View All</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/home-sliders/create/') }}">
                            <i class="menu-icon mdi mdi-plus"></i>
                            <span class="menu-title">Add Slider</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/page-contents/about')}}">
                <i class="menu-icon mdi mdi-bulletin-board"></i>
                <span class="menu-title">About Us</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#products" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-account"></i>
                <span class="menu-title">Alumni</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="products">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/alumnis') }}">
                            <i class="menu-icon mdi mdi-eye"></i>
                            <span class="menu-title">View All</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/alumnis/unverified') }}">
                            <i class="menu-icon mdi mdi-lock"></i>
                            <span class="menu-title">View Unverified</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/alumnis/new') }}">
                            <i class="menu-icon mdi mdi-account-alert"></i>
                            <span class="menu-title">New Joiners</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/alumnis/create/') }}">
                            <i class="menu-icon mdi mdi-plus"></i>
                            <span class="menu-title">Add an Alumni</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#events" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-eventbrite"></i>
                <span class="menu-title">Events</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="events">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/events') }}">
                            <i class="menu-icon mdi mdi-eye"></i>
                            <span class="menu-title">View All</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/events/create/') }}">
                            <i class="menu-icon mdi mdi-plus"></i>
                            <span class="menu-title">Add an Event</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#news" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-calendar"></i>
                <span class="menu-title">News</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="news">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/news') }}">
                            <i class="menu-icon mdi mdi-eye"></i>
                            <span class="menu-title">View All</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/news/create/') }}">
                            <i class="menu-icon mdi mdi-plus"></i>
                            <span class="menu-title">Add News</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#business" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-office"></i>
                <span class="menu-title">Businesses</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="business">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/business') }}">
                            <i class="menu-icon mdi mdi-eye"></i>
                            <span class="menu-title">View All</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/business/create/') }}">
                            <i class="menu-icon mdi mdi-plus"></i>
                            <span class="menu-title">Add Business</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#jobs" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-script"></i>
                <span class="menu-title">Jobs</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="jobs">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/jobs') }}">
                            <i class="menu-icon mdi mdi-eye"></i>
                            <span class="menu-title">View All</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/jobs/create/') }}">
                            <i class="menu-icon mdi mdi-plus"></i>
                            <span class="menu-title">Add a Job</span>
                        </a>
                    </li>
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/jobs/email/') }}">--}}
                            {{--<i class="menu-icon mdi mdi-pencil"></i>--}}
                            {{--<span class="menu-title">Edit Job Application Email</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#volunteers" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-group"></i>
                <span class="menu-title">Volunteers</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="volunteers">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/volunteers') }}">
                            <i class="menu-icon mdi mdi-eye"></i>
                            <span class="menu-title">View All</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/volunteers/create/') }}">
                            <i class="menu-icon mdi mdi-plus"></i>
                            <span class="menu-title">Add a Volunteer</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ URL('admin/contacts') }}">
                <i class="menu-icon mdi mdi-comment-question"></i>
                <span class="menu-title">Inquiries</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/page-contents/privacy-agreement')}}">
                <i class="menu-icon mdi mdi-lock"></i>
                <span class="menu-title">Privacy Agreements</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/page-contents/user-agreement')}}">
                <i class="menu-icon mdi mdi-lock-pattern"></i>
                <span class="menu-title">User Agreements</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#voices" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-group"></i>
                <span class="menu-title">AMSI Voices</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="voices">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/amsi-voices/current') }}">
                            <i class="menu-icon mdi mdi-eye"></i>
                            <span class="menu-title">View Highlights</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/amsi-voices/current/create') }}">
                            <i class="menu-icon mdi mdi-plus"></i>
                            <span class="menu-title">Add Highlight</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/amsi-voices/past') }}">
                            <i class="menu-icon mdi mdi-eye"></i>
                            <span class="menu-title">View Past Events</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/amsi-voices/past/create') }}">
                            <i class="menu-icon mdi mdi-plus"></i>
                            <span class="menu-title">Add Past Events</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-toggle="collapse" href="#artists" aria-expanded="true" aria-controls="homepage">--}}
                {{--<i class="menu-icon mdi mdi-content-copy"></i>--}}
                {{--<span class="menu-title">Artists</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
            {{--<div class="collapse in" id="artists">--}}
                {{--<ul class="nav flex-column sub-menu">--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/artists') }}">View All</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/artists/create/') }}">Add an Artists</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-toggle="collapse" href="#administrators" aria-expanded="true" aria-controls="homepage">--}}
                {{--<i class="menu-icon mdi mdi-content-copy"></i>--}}
                {{--<span class="menu-title">About Page</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
            {{--<div class="collapse in" id="administrators">--}}
                {{--<ul class="nav flex-column sub-menu">--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/administrators') }}">View Team Members</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/administrators/create/') }}">Add a Team Member</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/partners') }}">View Partners</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/partners/create/') }}">Add a Partner</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/page-contents/about/') }}">Other Page Contents</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-toggle="collapse" href="#events" aria-expanded="true" aria-controls="homepage">--}}
                {{--<i class="menu-icon mdi mdi-content-copy"></i>--}}
                {{--<span class="menu-title">Calendar Events</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
            {{--<div class="collapse in" id="events">--}}
                {{--<ul class="nav flex-column sub-menu">--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/events') }}">View All Events</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/events/create/') }}">Add an Event</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/events/types') }}">Event Types</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/events/types/create/') }}">Add Event Types</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</li>--}}

        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-toggle="collapse" href="#artist-of-the-month" aria-expanded="true" aria-controls="homepage">--}}
                {{--<i class="menu-icon mdi mdi-content-copy"></i>--}}
                {{--<span class="menu-title">Artist of the month</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
            {{--<div class="collapse in" id="artist-of-the-month">--}}
                {{--<ul class="nav flex-column sub-menu">--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/artist-of-the-month') }}">View All</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/artist-of-the-month/create/') }}">Add an Artist of the month</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" href="{{ URL('admin/subscribers') }}">--}}
                {{--<i class="menu-icon mdi mdi-content-copy"></i>--}}
                {{--<span class="menu-title">Subscribers</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" href="{{ URL('admin/contacts') }}">--}}
                {{--<i class="menu-icon mdi mdi-content-copy"></i>--}}
                {{--<span class="menu-title">Inquiries</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-toggle="collapse" href="#settings" aria-expanded="true" aria-controls="homepage">--}}
                {{--<i class="menu-icon mdi mdi-content-copy"></i>--}}
                {{--<span class="menu-title">Settings</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
            {{--<div class="collapse in" id="settings">--}}
                {{--<ul class="nav flex-column sub-menu">--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/settings/home-items') }}">Home Categories</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/page-contents/header/') }}">Header Settings</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/page-contents/footer/') }}">Footer Settings</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/page-contents/contact/') }}">Contact Settings</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</li>--}}
    </ul>
</nav>
