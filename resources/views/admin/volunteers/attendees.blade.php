@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            @if(Session::has('success'))
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="alert-success success">
                        {{ Session::get('success') }}
                    </div>
                </div>
            </div>
            @endif

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4>Attendees for {{ $item->title }}</h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="dataTable">
                                    <tr>
                                        <th onclick="sortTable(0)">Name</th>
                                        <th onclick="sortTable(0)">Occupation</th>
                                        <th onclick="sortTable(0)">Year Graduated</th>
                                        <th onclick="sortTable(0)">Campus</th>
                                        <th onclick="sortTable(0)">Mobile Number</th>
                                        <th onclick="sortTable(0)">Email</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach($item->attendees as $user)
                                        <tr>
                                            <td>{{ $user->firstName }}, {{ $user->lastName }}</td>
                                            <td>{{ $user->occupation }}</td>
                                            <td>{{ $user->year_graduated }}</td>
                                            <td>{{ $user->campus }}</td>
                                            <td>{{ $user->mobileNumber }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td><a href="{{ url('admin/events/attendees/delete/'.$item->id.'/'.$user->id) }}">Delete</a> </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <a href="{{ url('admin/events/attendees/export/'.$item->id) }}"><button type="button" class="btn btn-success mr-2">Export</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $('#datetimepicker4').datepicker();
    </script>
@endsection
