@extends('admin.partials.master')

@inject('locationService', 'App\Services\LocationService')
<?php $countries = $locationService->getAllCountries(); ?>

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Add an Alumni</h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <br/>
                        <div class="col-md-12">
                            @if(Session::has('error'))
                                <div class="alert alert-danger">{{ Session::get('error') }}</div>
                            @elseif(Session::has('success'))
                                <div class="alert alert-success">
                                    {{ Session::get('success') }} <br/>
                                    Please click <a href="{{url('login')}}">here</a> to login.
                                </div>
                            @endif
                            <form method="POST" action="{{ url('register-user') }}">
                            @csrf

                            <!-- Name -->
                                <?php
                                $prefixes = ['Mr','Ms','Mrs','Dr','Engr','Prof']
                                ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Prefix:</label>
                                        <select name="prefix" class="form-control">
                                            @foreach($prefixes as $prefix)
                                                <option value="{{$prefix}}">{{$prefix}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <x-label for="occupation" :value="__('Occupation')" />
                                        <x-input id="occupation" class="form-control" type="text" name="occupation" :value="old('occupation')" required autofocus />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <x-label for="firstName" :value="__('First Name')" />
                                        <x-input id="name" class="form-control" type="text" name="firstName" :value="old('firstName')" required autofocus />
                                    </div>
                                    <div class="col-md-6">
                                        <x-label for="lastName" :value="__('Last Name')" />
                                        <x-input id="name" class="form-control" type="text" name="lastName" :value="old('lastName')" required autofocus />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <x-label for="gender" :value="__('Gender')" />
                                        <select name="gender" class="form-control">
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Current Location:</label>
                                        <select name="country" class="form-control">
                                            @foreach($countries as $country)
                                                <option value="{{$country}}">{{$country}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <!-- Email Address -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <x-label for="city" :value="__('City')" />
                                        <x-input class="form-control" type="text" name="city" :value="old('city')" required autofocus />
                                    </div>
                                    <div class="col-md-6">
                                        <x-label for="email" :value="__('Email')" />
                                        <x-input id="email" class="form-control" type="email" name="email" :value="old('email')" required />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <x-label for="campus" :value="__('Campus')" />
                                        <?php
                                        $campuses = [
                                            'Al Mawakeb School-Al Garhoud',
                                            'International School of Arts and Sciences',
                                            'Al Mawakeb School-Al Barsha',
                                            'Al Mawakeb School'
                                        ]
                                        ?>
                                        <select name="campus" class="form-control">
                                            @foreach($campuses as $campus)
                                                <option value="{{$campus}}">{{$campus}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <x-label for="year_graduated" :value="__('Year Graduated')" />
                                        <select name="year_graduated" class="form-control">
                                            @for($x=1960;$x<=\Carbon\Carbon::now()->format('Y');$x++)
                                                <option value="{{$x}}">{{$x}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>

                                <!-- Confirm Password -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <x-label for="password" :value="__('Password')" />
                                        <x-input id="password" class="form-control"
                                                 type="password"
                                                 name="password"
                                                 required autocomplete="new-password" />
                                    </div>
                                    <div class="col-md-6">
                                        <x-label for="password_confirmation" :value="__('Confirm Password')" />
                                        <x-input id="password_confirmation" class="form-control"
                                                 type="password"
                                                 name="password_confirmation" required />
                                    </div>
                                </div>

                                <div class="flex items-center justify-end mt-4">

                                    <input type="submit" class="form-control" value="SAVE">
                                    <br/>
                                    <br/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
                </div>
            </div>

@endsection

@section('js')
    <script src="{{ asset('public/admin/js/file-upload.js') }}"></script>
@endsection
