@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>New Alumnis</h5><br/>
                            <form action="{{ url('admin/alumnis/new') }}" method="get">
                            <div class="row">
                                <div class="col-md-4">
                                    Month:
                                    <select name="month" class="form-control">
                                        <option value="01" {{ $month == '01' ? 'selected' : false }}>January</option>
                                        <option value="02" {{ $month == '02' ? 'selected' : false }}>February</option>
                                        <option value="03" {{ $month == '03' ? 'selected' : false  }}>March</option>
                                        <option value="04" {{ $month == '04' ? 'selected' : false  }}>April</option>
                                        <option value="05" {{ $month == '05' ? 'selected' : false  }}>May</option>
                                        <option value="06" {{ $month == '06' ? 'selected' : false  }}>June</option>
                                        <option value="07" {{ $month == '07' ? 'selected' : false  }}>July</option>
                                        <option value="08" {{ $month == '08' ? 'selected' : false  }}>August</option>
                                        <option value="09" {{ $month == '09' ? 'selected' : false  }}>September</option>
                                        <option value="10" {{ $month == '10' ? 'selected' : false  }}>October</option>
                                        <option value="11" {{ $month == '11' ? 'selected' : false  }}>November</option>
                                        <option value="12" {{ $month == '12' ? 'selected' : false  }}>December</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    Year:

                                    <select name="year" class="form-control">
                                        @for($x=1989;$x<2100;$x++)
                                            <option {{ $year == $x ? 'selected' : false  }} value="{{$x}}">{{$x}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    &nbsp;
                                    <input type="submit" value="Filter" class="form-control">
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <br/>
                            <form method="get" action="{{ url('admin/alumnis/new-export') }}">
                                <input type="hidden" name="month" value="{{ $month }}">
                                <input type="hidden" name="year" value="{{ $year }}">
                                <input type="submit" value="EXPORT" class="pull-right">
                            </form>
                            <br/>
                            <br/>
                            <div class="table-responsive">
                                <table class="table" id="dataTable">
                                    <tr>
                                        <th onclick="sortTable(0)">Name</th>
                                        <th onclick="sortTable(0)">Email</th>
                                        <th onclick="sortTable(0)">Occupation</th>
                                        <th onclick="sortTable(0)">Year Graduated</th>
                                        <th onclick="sortTable(0)">Campus</th>
                                        <th onclick="sortTable(0)">Mobile Number</th>
                                        <th onclick="sortTable(0)">Current Location</th>
                                        <th onclick="sortTable(0)">Join Date</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach($data as $item)
                                        <tr>
                                            <td>{{ $item->firstName }} {{ $item->lastName }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->occupation }}</td>
                                            <td>{{ $item->year_graduated }}</td>
                                            <td>{{ $item->campus }}</td>
                                            <td>{{ $item->mobileNumber }}</td>
                                            <td>{{ $item->city ? $item->city.', ' : '' }}{{ $item->country }}</td>
                                            <td>{{ $item->created_at->format('d-m-Y') }}</td>
                                            <td>
                                                <a href="{{ URL('directory/'.$item->id) }}" target="_blank">View</a>
                                                |
                                                @if($item->verified)
                                                    @if($item->is_active)
                                                        <a href="{{ URL('admin/alumnis/deactivate/'.$item->id) }}">Deactivate</a>
                                                    @else
                                                        <a href="{{ URL('admin/alumnis/activate/'.$item->id) }}">Activate</a>
                                                    @endif
                                                @else
                                                    <a href="{{ URL('admin/alumnis/verify/'.$item->id) }}">Verify</a>
                                                @endif
                                                |
                                                <a href="{{ URL('admin/alumnis/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
