@extends('admin.partials.master')

@inject('locationService', 'App\Services\LocationService')
<?php $countries = $locationService->getAllCountries(); ?>


@section('content')
    <link rel="stylesheet" href="{{ asset('public/css/profile.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">

    <?php
        $campuses = [
            'Al Mawakeb School-Al Garhoud',
            'International School of Arts and Sciences',
            'Al Mawakeb School-Al Barsha',
            'Al Mawakeb School'
        ];
    ?>

    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit Alumni</h5>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <section id="profile-cover" class="user-cover" style="background-image: url({{ $data->cover ? asset('public/'.$data->cover) : ''}})">
                                <div class="container-fluid">
                                    <div class="container">
                                        <div class="edit-wrap pull-right">
                                            <div class="hideOnEdit">
                                                <a href="#" class="editBt float-right">Edit Cover</a>
                                            </div>
                                            <div class="showOnEdit">
                                                <form action="{{url('/my-profile/update-cover')}}" class="ajxFormCover">
                                                    @csrf
                                                    <label>Upload Cover Photo:</label>
                                                    <input type="file" name="photo" class="form-control" accept="image/*">

                                                    <input type="submit" value="Update">
                                                </form>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3 text-left edit-wrap">
                                                <div class="hideOnEdit">
                                                    <a href="#" class="editBt">Change Profile Photo</a>
                                                    <img src="{{$data->photoUrl}}" class="photo user-photo">
                                                </div>
                                                <div class="showOnEdit">
                                                    <form action="{{url('/my-profile/update-photo')}}" class="ajxFormPhoto">
                                                        @csrf
                                                        <label>Upload Photo:</label>
                                                        <input type="file" name="photo" class="form-control" accept="image/*">

                                                        <input type="submit" value="Update">
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="col-md-9 relative">
                                                <div class="wrp">
                                                    <div class="vcenter edit-wrap">
                                                        <div class="hideOnEdit">
                                                            <a href="#" class="editBt">Edit</a>
                                                            <span class="name"><span class="data-firstName">{{ $data->firstName }}</span> <span class="data-lastName">{{ $data->lastName }}</span></span>
                                                            <span class="school"><span class="data-campus">{{ $data->campus }}</span> <span class="data-year_graduated">{{ $data->year_graduated }}</span></span>
                                                        </div>
                                                        <div class="showOnEdit">
                                                            <form action="{{url('/my-profile/update-fields')}}" class="ajxForm">
                                                                @csrf
                                                                <label>First Name:</label>
                                                                <input type="text" name="firstName" class="form-control" value="{{ $data->firstName }}">
                                                                <label>Last Name:</label>
                                                                <input type="text" name="lastName" class="form-control" value="{{ $data->lastName }}">
                                                                <label>Campus:</label>
                                                                <select name="campus" class="form-control">
                                                                    @foreach($campuses as $campus)
                                                                        <option {{ $data->campus == $campus ? 'selected' : false}} value="{{$campus}}">{{$campus}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <label>Year Graduated:</label>
                                                                <select name="year_graduated" class="form-control">
                                                                    @for($x=1960;$x<=\Carbon\Carbon::now()->format('Y');$x++)
                                                                        <option {{ $data->year_graduated == $x ? 'selected' : false}} value="{{$x}}">{{$x}}</option>
                                                                    @endfor
                                                                </select>
                                                                <input type="submit" value="Update">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section id="profile-strip">
                                <div class="container-fluid">
                                    <div class="container">
                                        <div class="row">

                                            @if($data->city || $data->country)
                                                <div class="col-md-4 text-left">
                                                    <div class="pull-left">
                                                        <img src="{{ asset('public/img/profile/location-icon.png') }}">
                                                    </div>
                                                    <div class="pull-left ml-3 edit-wrap">
                                                        <div class="hideOnEdit">
                                                            Lives in <br/> <span class="data-city">{{ $data->city ? $data->city.',' : '' }}</span> <span class="data-country">{{ $data->country }}</span>
                                                            <a href="#" class="editBt">Edit</a>

                                                        </div>
                                                        <div class="showOnEdit">
                                                            <form action="{{url('/my-profile/update-location')}}" class="" method="post">
                                                                @csrf
                                                                <label>Country</label>
                                                                <select name="country" class="form-control">
                                                                    @foreach($countries as $country)
                                                                        <option {{  Auth::user()->country == $country ? 'selected' : '' }} value="{{$country}}">{{$country}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <label>City</label>
                                                                <input type="text" class="form-control" name="city" value="{{ $data->city }}">

                                                                <input type="submit" value="Update">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="col-md-4 text-left">
                                                <div class="pull-left">
                                                    <img src="{{ asset('public/img/profile/occupation-icon.png') }}">
                                                </div>
                                                <div class="pull-left ml-3 edit-wrap">
                                                    <div class="hideOnEdit">
                                                        Occupation <br/>
                                                        <span class="data-occupation">{{ $data->jobTitle }}</span>
                                                        <a href="#" class="editBt">Edit</a>
                                                    </div>
                                                    <div class="showOnEdit">
                                                        <form action="{{url('/my-profile/update-fields')}}" class="ajxForm">
                                                            @csrf
                                                            <label>Occupation</label>
                                                            <input type="text" class="form-control" name="occupation" value="{{ $data->occupation }}">
                                                            <input type="submit" value="Update">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 text-right">
                                                <a href="#" id="downloadResume"><img src="{{ asset('public/img/profile/download-icon.png') }}"> DOWNLOAD RESUME</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <section id="profile-details">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="macy-container">
                                                <div class="colm" macy-complete="1">
                                                    <span class="heading">About</span>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <span class="lbl">Summary</span>
                                                        </div>
                                                        <div class="col-md-9 edit-wrap">

                                                            <div class="hideOnEdit">
                                                                <span class="data-bio">{{ $data->bio }}</span>
                                                                @if($data->bio)
                                                                    <br/>
                                                                @endif
                                                                <a href="#" class="editBt">Edit</a>
                                                            </div>
                                                            <div class="showOnEdit">
                                                                <form action="{{url('/my-profile/update-fields')}}" class="ajxForm">
                                                                    @csrf
                                                                    <label>Bio</label>
                                                                    <textarea class="form-control" name="bio">{{ $data->bio }}</textarea>
                                                                    <input type="submit" value="Update">
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <span class="lbl">Skills</span>
                                                        </div>

                                                        <div class="col-md-9">
                                                            <div class="edit-wrap">
                                                                <div class="hideOnEdit">
                                                                    <div id="currentSkills">
                                                                        @foreach($data->skills as $skill)
                                                                            <div class="skill" id="skill-{{$skill->id}}">{{$skill->name}}<span class="deleteTag" data-id="{{$skill->id}}" data-type="skill"></span></div>
                                                                        @endforeach
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <a href="#" class="editBt">Add new Skill</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="showOnEdit">
                                                                    <form action="{{url('/my-profile/add-skills')}}" class="ajxFormSkills">
                                                                        @csrf
                                                                        <select name="id">
                                                                            @foreach($skills as $sk)
                                                                                <option value="{{$sk->id}}">{{$sk->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        <input type="submit" value="Add">
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <span class="lbl">Industries</span>
                                                        </div>

                                                        <div class="col-md-9">
                                                            <div class="edit-wrap">
                                                                <div class="hideOnEdit">
                                                                    <div id="currentIndustries">
                                                                        @foreach($data->industries as $industry)
                                                                            <div class="industry" id="industry-{{$industry->id}}">{{$industry->name}}<span class="deleteTag" data-id="{{$industry->id}}" data-type="industry"></span></div>
                                                                        @endforeach
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <a href="#" class="editBt">Add new Industry</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="showOnEdit">
                                                                    <form action="{{url('/my-profile/add-industry')}}" class="ajxFormIndustries">
                                                                        @csrf
                                                                        <select name="id">
                                                                            @foreach($industries as $sk)
                                                                                <option value="{{$sk->id}}">{{$sk->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        <input type="submit" value="Add">
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="colm edit-wrap">
                                                    <span class="heading">Contact</span>
                                                    <div class="hideOnEdit">
                                                        <a href="#" class="editBt pull-right">Edit</a>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <span class="lbl">Email</span>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <span class="data-email">{{ $data->email }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12"><hr/></div>
                                                            <div class="col-md-3">
                                                                <span class="lbl">Phone Number</span>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <span class="data-mobileNumber">{{ $data->mobileNumber }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12"><hr/></div>
                                                            <div class="col-md-3">
                                                                <span class="lbl">Address</span>
                                                            </div>
                                                            <div class="col-md-9">
                                                                <span class="data-city">{{ $data->city ? $data->city.',' : '' }} {{ $data->country }}</span>
                                                            </div>
                                                        </div>
                                                        @if($data->city || $data->country)
                                                            <div class="row">
                                                                <div class="col-md-12"><hr/></div>
                                                                <div class="col-md-12">
                                                                    <span class="lbl">Location</span>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="map-canvas"></div>
                                                                </div>
                                                            </div>
                                                        @endif

                                                    </div>
                                                    <div class="showOnEdit">
                                                        <form action="{{url('/my-profile/update-fields')}}" class="ajxForm">
                                                            @csrf
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <label>Current Location (Country):</label>
                                                                    <select name="country" class="form-control">
                                                                        @foreach($countries as $country)
                                                                            <option {{  Auth::user()->country == $country ? 'selected' : '' }} value="{{$country}}">{{$country}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label>Current Location (City):</label>
                                                                    <x-input class="form-control" type="text" name="city" :value="old('city')" required autofocus  value="{{Auth::user()->city}}"/>
                                                                </div>
                                                            </div>


                                                            <!-- Email Address -->
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <x-label for="email" :value="__('Email')" />
                                                                    <x-input id="email" class="form-control" type="email" name="email" :value="old('email')" required value="{{Auth::user()->email}}"/>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <x-label for="mobileNumber" :value="__('Contact Number')" />
                                                                    <x-input id="email" class="form-control" type="text" name="mobileNumber" :value="old('mobileNumber')" required value="{{Auth::user()->mobileNumber}}"/>
                                                                </div>
                                                            </div>

                                                            <input type="submit" value="Update">
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="colm edit-wrap">
                                                    <span class="heading">Experience</span>
                                                    <div id="currentCareers">
                                                        @foreach($data->career as $career)
                                                            <div class="row edit-wrap" id="career-{{$career->id}}">
                                                                <div class="hideOnEdit">
                                                                    <div class="col-md-2 icon">
                                                                        <img src="{{asset('public/img/profile/career-icon.png')}}" width="100%">
                                                                    </div>
                                                                    <div class="col-md-9 relative">
                                                                        <p class="title" field="jobTitle">{{ $career->jobTitle }}</p>
                                                                        <p field="employer">{{ $career->employer }}</p>
                                                                        <p field="duration">{{ $career->startYear }}{{ $career->endYear ? ' - ' . $career->endYear : '' }} </p>
                                                                    </div>
                                                                    <div class="col-md-12"><hr/></div>
                                                                    <button class="deleteItem" data-id="{{ $career->id }}" data-type="career">X</button>
                                                                    <a href="#" class="editItem" data-id="{{$career->id}}" data-type="career">Edit</a>
                                                                </div>
                                                                <div class="showOnEdit">
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>

                                                    <div class="edit-wrap">
                                                        <div class="hideOnEdit">
                                                            <a href="#" class="editBt">Add new Career</a>
                                                        </div>
                                                        <div class="showOnEdit">
                                                            <form action="{{url('/my-profile/add-career')}}" class="ajxAddCareer">
                                                                @csrf
                                                                <div class="educ">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Company</label>
                                                                            <input type="text" name="career[0][employer]" class="form-control" value="">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Position</label>
                                                                            <input type="text" name="career[0][jobTitle]" class="form-control" value="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Month Joined</label>
                                                                            <input type="text" name="career[0][startMonth]" class="form-control" value="">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Year Joined</label>
                                                                            <input type="text" name="career[0][startYear]" class="form-control" value="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Month Ended</label>
                                                                            <input type="text" name="career[0][endMonth]" class="form-control" value="">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Year Ended</label>
                                                                            <input type="text" name="career[0][endYear]" class="form-control" value="">
                                                                        </div>
                                                                    </div>
                                                                    <br/>
                                                                </div>

                                                                <input type="submit" value="Add">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="colm edit-wrap">
                                                    <span class="heading">Portfolio</span>
                                                    <div id="currentPortfolio">
                                                        @foreach($data->portfolio as $portfolio)
                                                            <div class="row edit-wrap" id="portfolio-{{$portfolio->id}}">
                                                                <div class="hideOnEdit">
                                                                    <a href="{{ $portfolio->url }}" download="{{ $portfolio->original_name }}" target="_blank">
                                                                        <div class="col-md-2 icon">
                                                                            <img src="{{asset('public/img/profile/portfolio-icon.png')}}" width="100%">
                                                                        </div>
                                                                        <div class="col-md-9 relative">
                                                                            <p class="title" field="name">{{ $portfolio->title }}</p>
                                                                        </div>
                                                                    </a>
                                                                    <div class="col-md-12"><hr/></div>
                                                                    <button class="deleteItem" data-id="{{ $portfolio->id }}" data-type="portfolio">X</button>
                                                                    <a href="#" class="editItem" data-id="{{$portfolio->id}}" data-type="portfolio">Edit</a>
                                                                </div>
                                                                <div class="showOnEdit">
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="edit-wrap">
                                                        <div class="hideOnEdit">
                                                            <a href="#" class="editBt">Add new file</a>
                                                        </div>

                                                        <div class="showOnEdit">
                                                            <form action="{{url('/my-profile/add-portfolio')}}" class="ajxFormPortfolio">
                                                                @csrf
                                                                <div class="educ">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <label>File Name</label>
                                                                            <input type="text" name="title" class="form-control" value="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <label>File</label>
                                                                            <input type="file" name="file" class="form-control" value="" id="fileUpload" accept=".doc,.docx,.pdf">
                                                                        </div>
                                                                    </div>
                                                                    <br/>
                                                                </div>

                                                                <input type="submit" value="Add">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="colm edit-wrap">
                                                    <span class="heading">Education</span>

                                                    <div id="currentEducations">
                                                        @foreach($data->education as $education)
                                                            <div class="row edit-wrap" id="education-{{$education->id}}">
                                                                <div class="hideOnEdit">
                                                                    <div class="col-md-2 icon">
                                                                        <img src="{{asset('public/img/profile/education.png')}}" width="100%">
                                                                    </div>
                                                                    <div class="col-md-9">
                                                                        <p class="title"  field="school">{{ $education->school }}</p>
                                                                        @if($education->degree)
                                                                            <p field="degree">{{ $education->degree }}</p>
                                                                        @endif
                                                                        <p field="duration">{{ $education->yearJoin }}{{ $education->classYear ? ' - ' . $education->classYear : false }}</p>

                                                                    </div>
                                                                    <div class="col-md-12"><hr/></div>
                                                                    <button class="deleteItem" data-id="{{ $education->id }}" data-type="education">X</button>
                                                                    <a href="#" class="editItem" data-id="{{$education->id}}" data-type="education">Edit</a>
                                                                </div>
                                                                <div class="showOnEdit"></div>
                                                            </div>
                                                        @endforeach
                                                    </div>

                                                    <div class="edit-wrap">
                                                        <div class="hideOnEdit">
                                                            <a href="#" class="editBt">Add new Education</a>
                                                        </div>

                                                        <div class="showOnEdit">
                                                            <form action="{{url('/my-profile/add-education')}}" class="ajxAddEducation">
                                                                @csrf
                                                                <div class="educ">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>School</label>
                                                                            <input type="text" name="education[0][school]" class="form-control" value="">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Degree</label>
                                                                            <input type="text" name="education[0][degree]" class="form-control" value="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <label>Year Joined</label>
                                                                            <input type="text" name="education[0][yearJoin]" class="form-control" value="">
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label>Year Graduated</label>
                                                                            <input type="text" name="education[0][classYear]" class="form-control" value="">
                                                                        </div>
                                                                    </div>
                                                                    <br/>
                                                                </div>

                                                                <input type="submit" value="Add">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('js')
    <script src="{{ asset('public/js/macy.js') }}"></script>
    <script>
        var masonry = new Macy({
            container: '#macy-container',
            trueOrder: false,
            waitForImages: false,
            useOwnImageLoader: false,
            debug: true,
            mobileFirst: true,
            columns: 1,
            margin: {
                y: 35,
                x: 15,
            },
            breakAt: {
                1200: 2,
                940: 2,
                520: 1,
                400: 1
            },
        });

        function refreshMacy(){
            setTimeout(function(){
                masonry.recalculate(true);
            },100);
        }

        $('.editBt').on('click',function(e){
            e.preventDefault();
            $(this).closest('.edit-wrap').find('.hideOnEdit').hide();
            $(this).closest('.edit-wrap').find('.showOnEdit').show();

            setTimeout(function(){
                masonry.recalculate(true);
            },100);
        });

        $('.ajxFormPhoto').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = new FormData($(this)[0]);
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function(response){

                    $('.user-photo').each(function(){
                        $(this).attr('src',response.data);
                    });

                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('.ajxFormCover').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = new FormData($(this)[0]);
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function(response){
                    console.log(response);
                    $('#profile-cover').css('background-image','url('+response.data+')');
                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('.ajxForm').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(response){

                    console.log(response);
                    Object.keys(response.data).map(function(objectKey, index) {
                        var value = response.data[objectKey];
                        $('.data-'+objectKey).html(value);
                    });

                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('.ajxAddCareer').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(response){

                    Object.keys(response.data).map(function(objectKey, index) {
                        value = response.data[objectKey];
                        rowId =  value.id;

                        item = '<div class="row edit-wrap" id="career-'+value.id+'">\n' +
                            '    <div class="hideOnEdit">\n' +
                            '        <div class="col-md-2 icon">\n' +
                            '            <img src="{{asset('public/img/profile/career-icon.png')}}" width="100%">\n' +
                            '        </div>\n' +
                            '        <div class="col-md-9 relative">\n' +
                            '            <p class="title" field="jobTitle">'+value.jobTitle+'</p>\n' +
                            '            <p field="employer">'+value.employer+'</p>\n' +
                            '            <p field="duration">'+value.startYear+' - '+value.endYear+'</p>\n' +
                            '        </div>\n' +
                            '        <div class="col-md-12"><hr/></div>\n' +
                            '        <button class="deleteItem" data-id="'+value.id+'" data-type="career">X</button>\n' +
                            '        <a href="#" class="editItem" data-id="'+value.id+'" data-type="career">Edit</a>\n' +
                            '    </div>\n' +
                            '    <div class="showOnEdit">\n' +
                            '    </div>\n' +
                            '</div>';
                    });

                    $('#currentCareers').append(item);

                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                    refreshMacy();
                    addEditItemClick();
                    addDeleteItemClick();
                }
            });
        });

        $('.ajxAddEducation').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(response){

                    console.log(response);

                    Object.keys(response.data).map(function(objectKey, index) {
                        value = response.data[objectKey];
                        rowId =  value.id;

                        item = '<div class="row edit-wrap" id="education-'+value.id+'">\n' +
                            '    <div class="hideOnEdit">\n' +
                            '        <div class="col-md-2 icon">\n' +
                            '            <img src="{{asset('public/img/profile/education.png')}}" width="100%">\n' +
                            '        </div>\n' +
                            '        <div class="col-md-9">\n' +
                            '            <p class="title"  field="school">'+value.school+'</p>\n' +
                            '                <p field="degree">'+value.degree+'</p>\n' +
                            '            <p field="duration">'+value.yearJoin+' - '+value.classYear+'</p>\n' +
                            '\n' +
                            '        </div>\n' +
                            '        <div class="col-md-12"><hr/></div>\n' +
                            '        <button  class="deleteItem" data-id="'+value.id+'" data-type="education">X</button>\n' +
                            '        <a href="#" class="editItem" data-id="'+value.id+'" data-type="education">Edit</a>\n' +
                            '    </div>\n' +
                            '    <div class="showOnEdit"></div>\n' +
                            '</div>';
                    });

                    $('#currentEducations').append(item);

                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                    refreshMacy();
                    addEditItemClick();
                    addDeleteItemClick();
                }
            });
        });

        $('.ajxFormPortfolio').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = new FormData($(this)[0]);
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function(response){

                    if(response.data){

                        item = '<div class="row edit-wrap" id="portfolio-'+response.data.id+'">\n' +
                            '    <div class="hideOnEdit">\n' +
                            '        <a href="'+response.data.file+'" download="'+response.data.original_name+'" target="_blank">\n' +
                            '            <div class="col-md-2 icon">\n' +
                            '                <img src="{{asset('public/img/profile/portfolio-icon.png')}}" width="100%">\n' +
                            '            </div>\n' +
                            '            <div class="col-md-9 relative">\n' +
                            '                <p class="title" field="name">'+response.data.title+'</p>\n' +
                            '            </div>\n' +
                            '        </a>\n' +
                            '        <div class="col-md-12"><hr/></div>\n' +
                            '        <button class="deleteItem" data-id="'+response.data.id+'" data-type="portfolio">X</button>\n' +
                            '        <a href="#" class="editItem" data-id="'+response.data.id+'" data-type="portfolio">Edit</a>\n' +
                            '    </div>\n' +
                            '    <div class="showOnEdit">\n' +
                            '    </div>\n' +
                            '</div>';

                        $('#currentPortfolio').append(item);

                        $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                        $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                    }


                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                    refreshMacy();
                    addEditItemClick();
                    addDeleteItemClick();
                }
            });
        });

        $('.ajxFormSkills').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(response){

                    item = '<div class="skill" id="skill-'+response.data.id+'">'+response.data.name+'<span class="deleteTag" data-id="'+response.data.id+'" data-type="skill"></span></div>';
                    $('#currentSkills').append(item);

                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                    refreshMacy();
                    addDeleteTagClick();
                }
            });
        });

        $('.ajxFormIndustries').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(response){

                    item = '<div class="industry" id="industry-'+response.data.id+'">'+response.data.name+'<span class="deleteTag" data-id="'+response.data.id+'" data-type="industry"></span></div>';
                    $('#currentIndustries').append(item);

                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                    refreshMacy();
                    addDeleteTagClick();
                }
            });
        });

        function addDeleteItemClick(){
            $('.deleteItem').unbind('click');

            $('.deleteItem').on('click',function(e){
                console.log('click');
                e.preventDefault();
                clickedElement = $(this);
                type = clickedElement.attr('data-type');
                id = clickedElement.attr('data-id');

                $.ajax({
                    type: "POST",
                    url: baseUrl+'/my-profile/delete-'+type,
                    data: { id : id , _token : '{{ @csrf_token() }}' },
                    success: function(response){
                        console.log('#'+type+'-'+response.data);
                        $('#'+type+'-'+response.data).remove();
                    },
                    statusCode: {
                        401: function() {
                            window.location.href = '{{url('login')}}'; //or what ever is your login URI
                        }
                    },
                    complete : function (event,error){
                        refreshMacy();
                    }
                });
            });
        }
        addDeleteItemClick();

        function addDeleteTagClick(){
            $('.deleteTag').unbind('click');

            $('.deleteTag').on('click',function(e){
                e.preventDefault();

                clickedElement = $(this);
                type = clickedElement.attr('data-type');
                id = clickedElement.attr('data-id');

                $.ajax({
                    type: "POST",
                    url: baseUrl+'/my-profile/delete-tag-'+type,
                    data: { id : id , _token : '{{ @csrf_token() }}' },
                    success: function(response){
                        $('#'+type+'-'+response.data).remove();
                    },
                    statusCode: {
                        401: function() {
                            window.location.href = '{{url('login')}}'; //or what ever is your login URI
                        }
                    },
                    complete : function (event,error){
                        refreshMacy();
                    }
                });
            });
        }
        addDeleteTagClick();

        function addEditItemClick(){

            $('.editItem').unbind('click');

            $('.editItem').on('click',function(e){
                e.preventDefault();
                clickedElement = $(this);
                type = clickedElement.attr('data-type');
                id = clickedElement.attr('data-id');

                $.ajax({
                    type: "GET",
                    url: baseUrl+'/my-profile/get-'+type+'/'+id,
                    success: function(response){
                        console.log(response);
                        if(response.data){
                            if(type=='career'){
                                item = '                       <form method="post" class="editItemForm">\n' +
                                    '                            @csrf'+
                                    '                           <input type="hidden" value="'+response.data.id+'" name="id">'+
                                    '                            <div class="col-md-12">\n' +
                                    '                                <div class="row">\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Company</label>\n' +
                                    '                                        <input type="text" name="employer" class="form-control" value="'+response.data.employer+'">\n' +
                                    '                                    </div>\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Position</label>\n' +
                                    '                                        <input type="text" name="jobTitle" class="form-control" value="'+response.data.jobTitle+'">\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                                <div class="row">\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Month Joined</label>\n' +
                                    '                                        <input type="text" name="startMonth" class="form-control" value="'+response.data.startMonth+'">\n' +
                                    '                                    </div>\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Year Joined</label>\n' +
                                    '                                        <input type="text" name="startYear" class="form-control" value="'+response.data.startYear+'">\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                                <div class="row">\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Month Ended</label>\n' +
                                    '                                        <input type="text" name="endMonth" class="form-control" value="'+response.data.endMonth+'">\n' +
                                    '                                    </div>\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Year Ended</label>\n' +
                                    '                                        <input type="text" name="endYear" class="form-control" value="'+response.data.endYear+'">\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                                <br/>\n' +
                                    '                               <input type="submit" class="form-control" value="Update">'+
                                    '                                <br/>\n' +
                                    '                            </div>'+
                                    '                       </form>';
                            } else {

                                item = '                       <form method="post" class="editItemForm">\n' +
                                    '                            @csrf'+
                                    '                           <input type="hidden" value="'+response.data.id+'" name="id">'+
                                    '                            \n' +
                                    '                            <div class="col-md-12">\n' +
                                    '                                <div class="row">\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>School</label>\n' +
                                    '                                        <input type="text" name="school" class="form-control" value="'+response.data.school+'">\n' +
                                    '                                    </div>\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Degree</label>\n' +
                                    '                                        <input type="text" name="degree" class="form-control" value="'+response.data.degree+'">\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                                <div class="row">\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Year Joined</label>\n' +
                                    '                                        <input type="text" name="yearJoin" class="form-control" value="'+response.data.yearJoin+'">\n' +
                                    '                                    </div>\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Year Graduated</label>\n' +
                                    '                                        <input type="text" name="classYear" class="form-control" value="'+response.data.classYear+'">\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                                <br/>\n' +
                                    '                           <input type="submit" class="form-control" value="Update">'+
                                    '                                <br/>\n' +
                                    '                            </div>'+
                                    '                       </form>';
                            }

                            wrap = clickedElement.closest('.edit-wrap');
                            wrap.find('.hideOnEdit').hide();
                            wrap.find('.showOnEdit').append(item);
                            wrap.find('.showOnEdit').show();
                        }
                    },
                    statusCode: {
                        401: function() {
                            window.location.href = '{{url('login')}}'; //or what ever is your login URI
                        }
                    },
                    complete : function (event,error){
                        refreshMacy();
                        editItemFormSubmit(type);
                    }
                });
            });
        }
        addEditItemClick();

        function editItemFormSubmit(type){
            $('.editItemForm').unbind('submit');
            $('.editItemForm').on('submit', function(e){
                var form = $(this);

                e.preventDefault();
                e.stopPropagation();

                data = form.serialize();

                console.log(data);
                $.ajax({
                    type: "POST",
                    url: baseUrl+'/my-profile/update-'+type,
                    data: data,
                    success: function(response){

                        if(response.data){

                            d = response.data;
                            target = $('#'+d.type+'-'+d.id);

                            if(type=='career') {
                                target.find('p[field="jobTitle"]').html(response.data.jobTitle);
                                target.find('p[field="employer"]').html(response.data.employer);
                                target.find('p[field="duration"]').html(response.data.startYear + ' - ' + response.data.endYear);
                            }
                            else {
                                target.find('p[field="school"]').html(response.data.school);
                                target.find('p[field="degree"]').html(response.data.degree);
                                target.find('p[field="duration"]').html(response.data.yearJoin + ' - ' + response.data.classYear);
                            }
                            target.find('.hideOnEdit').show();
                            form.remove();
                            target.find('.showOnEdit').hide();
                        }
                    },
                    statusCode: {
                        401: function() {
                            window.location.href = '{{url('login')}}'; //or what ever is your login URI
                        }
                    },
                    complete : function (event,error){
                        refreshMacy();
                    }
                });
            });
        }

        editItemFormSubmit('career');
        editItemFormSubmit('education');

        var uploadField = document.getElementById("fileUpload");

        uploadField.onchange = function() {
            if(this.files[0].size > 2097152){
                alert("File is too big!");
                this.value = "";
            };
        };

    </script>
@endsection
