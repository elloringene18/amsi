@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Alumnis</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="filterbox">
                                Search:
                                <form method="get" action="{{ url('admin/alumnis/') }}">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" name="first_name" placeholder="First Name" class="form-control mb-3" value="{{ isset($_GET['first_name']) ? $_GET['first_name'] : '' }}">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="last_name" placeholder="Last Name" class="form-control mb-3" value="{{ isset($_GET['last_name']) ? $_GET['last_name'] : '' }}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select name="graduate_year" class="form-control mb-3">

                                                <option value="" disabled selected>Year Graduated</option>
                                                @for($x=2100;$x>1900;$x--)
                                                    <option {{ isset($_GET['graduate_year']) ? ($_GET['graduate_year'] == $x ? 'selected' : false ) : false }} value="{{$x}}">{{$x}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select name="campus" class="form-control mb-3">
                                                <option value="" disabled selected>Campus</option>
                                                @foreach($campuses as $campus)
                                                    <option {{ isset($_GET['campus']) ? ($_GET['campus'] == $campus ? 'selected' : false ) : false }} value="{{$campus}}">{{$campus}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select name="country" class="form-control mb-3">
                                                <option value="" disabled selected>Current Location</option>
                                                @foreach($countries as $country)
                                                    <option {{ isset($_GET['country']) ? ($_GET['country'] == $country ? 'selected' : false ) : false }} value="{{$country}}">{{$country}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <input type="submit" value="Search" class="form-control">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" id="dataTable">
                                    <tr>
                                        <th onclick="sortTable(0)">Name</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach($data as $item)
                                        <tr>
                                            <td>{{ $item->firstName }} {{ $item->lastName }}</td>
                                            <td>
                                                <a href="{{ URL('directory/'.$item->id) }}" target="_blank">View</a>
                                                |
                                                @if($item->is_active)
                                                    <a href="{{ URL('admin/alumnis/deactivate/'.$item->id) }}">Deactivate</a>
                                                @else
                                                    <a href="{{ URL('admin/alumnis/activate/'.$item->id) }}">Activate</a>
                                                @endif
                                                |
                                                <a href="{{ URL('admin/alumnis/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card pagi">
                    <div class="card">
                        <div class="card-body"><div class="row">
                                <div class="col-md-12" id="pagination">
                                    <div class="text-center paginate-nums">

                                        <div class="prevs">
                                            @if($page==2)
                                                <a href="{{ url('directory') }}?{{ 'page=1' }}{{ isset($_GET['first_name']) ? '&first_name='.$_GET['first_name'] : '' }}{{ isset($_GET['last_name']) ? '&last_name='.$_GET['last_name'] : '' }}{{ isset($_GET['graduate_year']) ? '&graduate_year='.$_GET['graduate_year'] : '' }}{{ isset($_GET['campus']) ? '&campus='.$_GET['campus'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}">1</a>
                                            @elseif(($page-1)>1)
                                                <?php $links = 0;?>
                                                @for($x=($page-1);$x>0;$x--)
                                                    @if($links<3)
                                                        @if($x==intval($page))
                                                            <span>{{ $x }}</span>
                                                        @else
                                                            <a href="{{ url('directory') }}?{{ 'page='.$x }}{{ isset($_GET['first_name']) ? '&first_name='.$_GET['first_name'] : '' }}{{ isset($_GET['last_name']) ? '&last_name='.$_GET['last_name'] : '' }}{{ isset($_GET['graduate_year']) ? '&graduate_year='.$_GET['graduate_year'] : '' }}{{ isset($_GET['campus']) ? '&campus='.$_GET['campus'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}">{{$x}}</a>
                                                        @endif
                                                        <?php $links++; ?>
                                                    @else
                                                        @break
                                                    @endif
                                                @endfor
                                            @endif

                                            @if($page>1)
                                                <a href="{{ url('directory') }}?page={{$page-1}}{{ isset($_GET['first_name']) ? '&first_name='.$_GET['first_name'] : '' }}{{ isset($_GET['last_name']) ? '&last_name='.$_GET['last_name'] : '' }}{{ isset($_GET['graduate_year']) ? '&graduate_year='.$_GET['graduate_year'] : '' }}{{ isset($_GET['campus']) ? '&campus='.$_GET['campus'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}"><</a>
                                            @endif
                                        </div>
                                        @if($page<=ceil($totalData/$perPage))
                                            <?php $links = 0;?>
                                            <div class="nexts">
                                                @for($x=$page;$x<=ceil($totalData/$perPage);$x++)
                                                    @if($links<4)
                                                        @if($x==intval($page))
                                                            <span>{{ $x }}</span>
                                                        @else
                                                            <a href="{{ url('directory') }}?page={{$x}}{{ isset($_GET['first_name']) ? '&first_name='.$_GET['first_name'] : '' }}{{ isset($_GET['last_name']) ? '&last_name='.$_GET['last_name'] : '' }}{{ isset($_GET['graduate_year']) ? '&graduate_year='.$_GET['graduate_year'] : '' }}{{ isset($_GET['campus']) ? '&campus='.$_GET['campus'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}">{{$x}}</a>
                                                        @endif
                                                        <?php $links++; ?>
                                                    @else
                                                        @break
                                                    @endif
                                                @endfor

                                                @if($perPage*$page<$totalData)
                                                    <a href="{{ url('directory') }}?page={{$page+1}}{{ isset($_GET['first_name']) ? '&first_name='.$_GET['first_name'] : '' }}{{ isset($_GET['last_name']) ? '&last_name='.$_GET['last_name'] : '' }}{{ isset($_GET['graduate_year']) ? '&graduate_year='.$_GET['graduate_year'] : '' }}{{ isset($_GET['campus']) ? '&campus='.$_GET['campus'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}">></a>
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    Showing {{ $totalData > $perPage? ((($page * $perPage) - $perPage) + 1) . ' - ' . $page * $perPage : $totalData }} of {{ $totalData }}<br/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
