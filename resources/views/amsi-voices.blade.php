@extends('master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $pageContent = $contentService->getPageContent('privacy-agreement'); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/news.css">
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">

    <style>
        .swiper-container {
            width: 100%;
            height: auto;
        }
        .news {
            width: 100%;
            overflow: hidden;
        }
        p {
            margin-bottom: 20px;
        }
        h3 {
            margin-bottom: 30px;
            text-transform: uppercase;
            font-weight: 900;
        }

        .tab-content {
            display: none;
        }

        .tab-content.active {
            display: block;
        }

        .tab-nav.active {
            font-weight: bold;
        }
    </style>
@endsection

@section('content')

    <section id="slide">
        <div class="container-fluid">
            <div class="row">
                <!-- Slider main container -->
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide home-slide" style="background-image:url({{ asset('public/'.$pageContent['image']) }});">
                            <div class="overlay"></div>
                            <div class="content">
                                <div class="container">
                                    <h3 class="sub">AMSI VOICES</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- If we need pagination -->
                    <!-- If we need navigation buttons -->
                    <!-- If we need scrollbar -->
                    <!--              <div class="swiper-scrollbar"></div>-->
                </div>
            </div>
        </div>
    </section>

    @inject('contentService', 'App\Services\ContentProvider')
    <?php $seventh = $contentService->getPageContent('amsi-voice-7th-conference'); ?>
    <?php $voices = $contentService->getPageContent('our-amsi-voices'); ?>

    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <?php $ind = 1; ?>
                    @foreach($current as $item)
                        <div class="tab-content {{ $ind==1 ? 'active' : '' }}">
                            {!! $item->content !!}
                        </div>
                        <?php $ind++; ?>
                    @endforeach
                </div>
                <div class="col-md-3">
                    <p><b>Highlights:</b></p>
                    <ul>

                        <?php $ind = 1; ?>
                        @foreach($current as $item)
                            <li><a href="#" class="tab-nav {{ $ind==1 ? 'active' : '' }}">{{ $item->title }}</a></li>
                            <?php $ind++; ?>
                        @endforeach
                    </ul>
                    <hr/>
                    <p><b>Previous Conferences:</b></p>
                    <ul>
                        <?php $ind = 1; ?>
                        @foreach($past as $item)
                            <li><a target="_blank" href="{{ asset('public/'.$item->content) }}" download="{{ $item->title }}.pdf">{{ $item->title }}</a></li>
                            <?php $ind++; ?>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            loop: false,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        });

        function vCenter(){
            $('.vcenter').each(function(){
                ht = $(this).height()/2;
                $(this).css('margin-top','-'+ht+'px');
            });
        }
        vCenter();

        $('.tab-nav').on('click',function (e) {
            e.preventDefault();
            $('.tab-content').removeClass('active');
            $('.tab-nav').removeClass('active');
            $('.tab-content').eq($('.tab-nav').index($(this))).addClass('active');
            $(this).addClass('active');
        })
    </script>
@endsection

