@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/news.css">
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">

    <style>
        .swiper-container {
            width: 100%;
            height: auto;
        }
        input, select, textarea {
            border: 1px solid #ccc;
            font-size: 16px;
            padding: 12px 8px;
            margin-bottom: 20px;
        }
        label {
            font-size: 16px;
        }
        .submit {
            color: #fff;
            background-color: #53284e;
        }

        #contacts p {
            font-size: 14px !important;
        }
    </style>
@endsection

@section('content')

    <section id="slide">
        <div class="container-fluid">
            <div class="row">
                <!-- Slider main container -->
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        <div class="swiper-slide home-slide" style="background-image:url({{ asset('public/img/contact-slide.jpg') }});">
                            <div class="overlay"></div>
                            <div class="content">
                                <div class="container">
                                    <h3 class="sub">CONTACT US</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form class="contact" action="{{url('contact')}}" method="post">
                        @if(Session::has('message'))
                            <div class="alert alert-success">{{ Session::get('message') }}</div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">{{ Session::get('error') }}</div>
                        @endif
                        @csrf
                        <div class="row ">
                            <div class="col-md-12">
                                <label>TYPE OF INQUIRY</label>
                                <select name="subject" class="form-control" required>
                                    <option value="general-inquiry">General Inquiry</option>
                                    <option value="suggestions">Suggestions</option>
                                    <option value="suggestions">Feedback</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>FULL NAME</label>
                                <input type="text" class="form-control" name="name" placeholder="" required>
                            </div>
                            <div class="col-md-6">
                                <label>EMAIL ADDRESS</label>
                                <input type="text" class="form-control" name="email" placeholder="" required>
                            </div>
                            <div class="col-md-12">
                                <label>MESSAGE</label>
                                <textarea name="message" class="form-control" placeholder="Enter message here..." rows="5"></textarea>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="form-control mt-3 submit" value="Send the message">
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <hr/>
                    <h3><strong>AAA Volunteers</strong></h3>
                    <br/>
                </div>
            </div>
            <div class="" id="contacts">
                <?php $row = 1; ?>
                @foreach($volunteers as $volunteer)

                    @if($row==1)
                        <div class="row">
                    @endif

                    <div class="col-md-6">
                        <strong>{{ $volunteer->name }}</strong><br/>
                        <p>{{ $volunteer->title }}</p>
                        {!! $volunteer->details !!}
                        <br/>
                    </div>

                    @if($row==2)
                        </div>
                        <?php $row = 0; ?>
                    @endif

                    <?php $row++; ?>
                @endforeach
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            loop: false,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        });

        function vCenter(){
            $('.vcenter').each(function(){
                ht = $(this).height()/2;
                $(this).css('margin-top','-'+ht+'px');
            });
        }
        vCenter();
    </script>
@endsection


