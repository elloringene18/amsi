@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public/css/profile.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/macy.css') }}">
    <style>
        .skill {
            background-color: #dcddde;
            padding: 3px 12px;
            float: left;
            margin: 5px 10px 5px 0;
            border: 1px solid #ccc;
            position: relative;
        }
        .industry {
            background-color: #dcddde;
            padding: 3px 12px;
            float: left;
            margin: 5px 10px 5px 0;
            border: 1px solid #ccc;
            position: relative;
        }
    </style>
@endsection

@section('content')
    <section id="profile-cover" style="background-image: url({{ $data->cover ? asset('public/'.$data->cover) : ''}})">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-5 col-xs-6 text-left edit-wrap">
                        <div class="hideOnEdit">
                            <img src="{{$data->photoUrl}}" class="photo user-photo">
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-7 col-sm-9 relative">
                        <div class="wrp">
                            <div class="vcenter edit-wrap">
                                <div  style="color:{{$data->font_color}}" id="name-font">
                                    <span class="name"><span class="data-firstName">{{ $data->firstName }}</span> <span class="data-lastName">{{ $data->lastName }}</span></span>
                                    <span class="school"><span class="data-campus">{{ $data->campus }}</span> <span class="data-year_graduated">{{ $data->year_graduated }}</span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="profile-strip">
        <div class="container-fluid">
            <div class="container">
                <div class="row">

                    <div class="col-md-4 text-left stripcol">
                        @if($data->city || $data->country)
                            <div class="pull-left">
                                <img src="{{ asset('public/img/profile/location-icon.png') }}">
                            </div>
                            <div class="pull-left ml-3">
                                Lives in <br/> {{ $data->city ? $data->city.',' : '' }} {{ $data->country }}
                            </div>
                        @endif
                    </div>

                    <div class="col-md-4 text-left stripcol">
                        @if(count($data->career))
                        <div class="pull-left">
                            <img src="{{ asset('public/img/profile/occupation-icon.png') }}">
                        </div>
                        <div class="pull-left ml-3">
                            Occupation <br/>
                            {{ $data->career[0]->jobTitle }}
                        </div>
                        @endif
                    </div>

                    <div class="col-md-4 text-right stripcol">
                        @if($data->resume)
                            <a href="{{ asset('public/'.$data->resume) }}" id="downloadResume" target="_blank"><img src="{{ asset('public/img/profile/download-icon.png') }}"> DOWNLOAD RESUME</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="profile-details">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="macy-container">
                        <div class="colm" macy-complete="1">
                            <span class="heading">About</span>
                            <div class="row">
                                <div class="col-md-3">
                                    <span class="lbl">Summary</span>
                                </div>
                                <div class="col-md-9">
                                    {{ $data->bio ? $data->bio : 'N/A' }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <span class="lbl">Skills</span>
                                </div>

                                <div class="col-md-9">
                                    <div class="edit-wrap">
                                        <div class="hideOnEdit">
                                            <div id="currentSkills">
                                                @foreach($data->skills as $skill)
                                                    <div class="skill" id="skill-{{$skill->id}}">{{$skill->name}}<span class="deleteTag" data-id="{{$skill->id}}" data-type="skill"></span></div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <span class="lbl">Industries</span>
                                </div>

                                <div class="col-md-9">
                                    <div class="edit-wrap">
                                        <div class="hideOnEdit">
                                            <div id="currentIndustries">
                                                @foreach($data->industries as $industry)
                                                    <div class="industry" id="industry-{{$industry->id}}">{{$industry->name}}<span class="deleteTag" data-id="{{$industry->id}}" data-type="industry"></span></div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="colm">
                            <span class="heading">Contact</span>
                            @if(!$data->hide_email)
                            <div class="row">
                                <div class="col-md-3">
                                    <span class="lbl">Email</span>
                                </div>
                                <div class="col-md-9">
                                    {{ $data->email }}
                                </div>
                            </div>
                            @endif
                            @if(!$data->hide_mobile)
                                <div class="row">
                                    <div class="col-md-12"><hr/></div>
                                    <div class="col-md-3">
                                        <span class="lbl">Phone Number</span>
                                    </div>
                                    <div class="col-md-9">
                                        {{ $data->mobileNumber }}
                                    </div>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-12"><hr/></div>
                                <div class="col-md-3">
                                    <span class="lbl">Address</span>
                                </div>
                                <div class="col-md-9">
                                    {{ $data->city ? $data->city.',' : '' }} {{ $data->country }}
                                </div>
                            </div>
                            @if($data->city || $data->country)
                                <div class="row">
                                    <div class="col-md-12"><hr/></div>
                                    <div class="col-md-12">
                                        <span class="lbl">Location</span>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="map-canvas"></div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="colm">
                            <span class="heading">Experience</span>
                            @foreach($data->career as $career)
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-xs-4 icon">
                                        <img src="{{asset('public/img/profile/career-icon.png')}}" width="100%">
                                    </div>
                                    <div class="col-md-9 col-sm-9 col-xs-8">
                                        <p class="title">{{ $career->jobTitle }}</p>
                                        <p>{{ $career->employer }}</p>
                                        <p>{{ $career->startYear }}{{ $career->endYear ? ' - ' . $career->endYear : '' }} </p>
                                    </div>
                                    <div class="col-md-12"><hr/></div>
                                </div>
                            @endforeach
                        </div>
                        <div class="colm">
                            <span class="heading">Portfolio</span>
                            @foreach($data->portfolio as $portfolio)
                                <a href="{{ $portfolio->url }}" download="{{ $portfolio->original_name }}" target="_blank">
                                    <div class="col-md-2 col-sm-2 col-xs-4 icon">
                                        <img src="{{asset('public/img/profile/portfolio-icon.png')}}" width="100%">
                                    </div>
                                    <div class="col-md-9 col-sm-9 col-xs-8 relative">
                                        <p class="title" field="name">{{ $portfolio->title }}</p>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                        <div class="colm">
                            <span class="heading">Education</span>

                            @foreach($data->education as $education)
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-xs-4 icon">
                                        <img src="{{asset('public/img/profile/education.png')}}" width="100%">
                                    </div>
                                    <div class="col-md-9 col-sm-9 col-xs-8">
                                        <p class="title">{{ $education->school }}</p>
                                        @if($education->degree)
                                            <p>{{ $education->degree }}</p>
                                        @endif
                                        <p>{{ $education->yearJoin }}{{ $education->classYear ? ' - ' . $education->classYear : false }}</p>
                                    </div>
                                    <div class="col-md-12"><hr/></div>
                                </div>
                            @endforeach
                        </div>
                        @if($data->hasChild)
                            <div class="colm">
                                <p>This alumni has a child/children in AMSI School</p>
                            </div>
                        @endif


                        @if(Auth::user()->is_admin)
                            @if($data->involvement)
                                <div class="colm">
                                    <label>Involvements:</label>
                                    <br/>
                                    <br/>
                                    <?php $involves = \App\Models\Involve::get(); ?>
                                    <div class="row">
                                        @foreach($involves as $involve)
                                            @if(in_array($involve->id, Auth::user()->involvement))
                                                <div class="col-md-12">
                                                    <label>{{$involve->title}}</label><br/>
                                                    {{$involve->details}}
                                                    <br/>
                                                    <br/>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')

    <script src="{{ asset('public/js/macy.js') }}"></script>
    <script>
        var masonry = new Macy({
            container: '#macy-container',
            trueOrder: false,
            waitForImages: false,
            useOwnImageLoader: false,
            debug: true,
            mobileFirst: true,
            columns: 1,
            margin: {
                y: 35,
                x: 15,
            },
            breakAt: {
                1200: 2,
                940: 2,
                520: 1,
                400: 1
            },
        });

        function refreshMacy(){
            setTimeout(function(){
                masonry.recalculate();
            },300);
        }
    </script>

    <script>
        // Snazzy Map Style - https://snazzymaps.com/style/8097/wy
    @if($data->city || $data->country)
        var geocoder = new google.maps.Geocoder();
        var address = "{{ $data->city }} {{ $data->country }}";

        function init(){
            var mapStyle=[{"elementType":"geometry","stylers":[{"color":"#212121"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#212121"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#757575"}]},{"featureType":"administrative.country","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"administrative.land_parcel","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#181818"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"poi.park","elementType":"labels.text.stroke","stylers":[{"color":"#1b1b1b"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#2c2c2c"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#8a8a8a"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#373737"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#3c3c3c"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry","stylers":[{"color":"#4e4e4e"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"transit","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#3d3d3d"}]}];


            geocoder.geocode( { 'address': address }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK)
                {
                    // do something with the geocoded result
                    //
                    // results[0].geometry.location.latitude
                    // results[0].geometry.location.longitude
                    var loctn = results[0].geometry.location;

                    var myLatlng = new google.maps.LatLng(loctn.lat(),loctn.lng());

                    // Create the map
                    var map = new google.maps.Map($('.map-canvas')[0], {
                        zoom: 4,
                        styles: mapStyle,
                        mapTypeControl: false,
                        streetViewControl: false,
                        center: myLatlng
                    });

                    markerSize = '40';
                    var marker = new google.maps.LatLng(loctn.lat(),loctn.lng());

                    new google.maps.Marker({
                        position: marker,
                        map,
                        title: "Hello World!",
                    });
                }
            });
        }
        google.maps.event.addDomListener(window, 'init', init());
    @endif
    </script>
@endsection
