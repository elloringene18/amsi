@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public/') }}/css/jobs.css">
    <link rel="stylesheet" href="{{ asset('public/') }}/css/directory.css">

    <style>
        .swiper-container {
            width: 100%;
            height: auto;
        }
        .news {
            width: 100%;
            overflow: hidden;
        }

        .filterbox {
            margin-top: 10px;
        }

        #createJobBt {
            background-color: #53284e;
            color: #ffde17;
            padding: 10px 20px;
            text-decoration: none !important;
            text-transform:uppercase;
        }

        .item .wrap {
            position: relative;
            height: 100%;
            width: 100%;
        }

        .item .bottom {
            position: absolute;
            bottom: 0;
            width: 100%;
        }
        .apply {
            margin-bottom: 5px;
        }
    </style>
@endsection

@section('content')

    <section id="map">
        <div class="container-fluid">
            <div class="row">
                <div class="map-canvas" style="height:550px;"></div>

                <?php

                $employment_types = ['Full-time','Part-time','Contract','Internship'];
                $experience = ['Less than a year','1-2 years','3-5 year','5-7 years','8-10 years','More than 10 years'];
                $careers = ['Junior','Mid Level','Senior','Management'];

                ?>
                <div class="filterbox">
                    <h4>Search for a job opportunity posted by our network</h4>
                    <form method="get" action="{{ url('jobs/') }}">

                        <select name="country" class="form-control mb-3" id="country">
                            <option value="" disabled selected>Country</option>
                            @foreach($countries as $country)
                                <option data-id="{{$country['id']}}" {{ isset($_GET['country']) ? ($_GET['country'] == $country['name'] ? 'selected' : false ) : false }} value="{{$country['name']}}">{{$country['name']}}</option>
                            @endforeach
                        </select>

                        <select name="city" class="form-control mb-3" id="city">
                            <option value="" disabled selected>City</option>
                        </select>

                        <select name="employment_type" class="form-control mb-3">
                            <option value="" disabled selected>Employment Type</option>
                            @foreach($employment_types as $employment_type)
                                <option {{ isset($_GET['employment_type']) ? ($_GET['employment_type'] == $employment_type ? 'selected' : false ) : false }} value="{{$employment_type}}">{{$employment_type}}</option>
                            @endforeach
                        </select>

                        <select name="experience" class="form-control mb-3">
                            <option value="" disabled selected>Experience</option>
                            @foreach($experience as $experienc)
                                <option {{ isset($_GET['experience']) ? ($_GET['experience'] == $experienc ? 'selected' : false ) : false }} value="{{$experienc}}">{{$experienc}}</option>
                            @endforeach
                        </select>

                        <select name="career_level" class="form-control mb-3">
                            <option value="" disabled selected>Career Level</option>
                            @foreach($careers as $career)
                                <option {{ isset($_GET['career_level']) ? ($_GET['career_level'] == $career ? 'selected' : false ) : false }} value="{{$career}}">{{$career}}</option>
                            @endforeach
                        </select>

                        <input type="submit" value="Search" class="form-control">
                    </form>
                </div>

            </div>
        </div>
    </section>

    <section id="directory">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a id="createJobBt" href="{{ url('create-job') }}">Create job opportunity +</a>
                    <br/>
                    <br/>
                    <br/>
                    @if(count($data))
                        @foreach($data as $job)

                            <div class="item">
                                <div class="wrap">
                                    <span class="name">{{ \Illuminate\Support\Str::words($job->title,6,'...') }}</span>
                                    <span class="school">{{ $job->company }}</span>
                                    <p class="desc">{{ \Illuminate\Support\Str::words(strip_tags($job->details),18,'...') }}</p>
                                    <a href="{{ url('jobs/'.$job->id) }}" class="readmore">Read More</a>

                                    <div class="bottom">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <span class="location">{{ $job->location }}</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"><a href="{{ url('jobs/'.$job->id) }}" class="apply">Apply</a></div>
                                            <div class="col-md-8 text-right"><img src="{{ $job->poster->photoUrl }}" class="poster-img"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">5 days ago</div>
                                            <div class="col-md-8 text-right">Posted by: {{ $job->posterName }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    @else
                        <h4>
                            Looking to hire the right candidate for your job opening?  Be the first to post.
                        </h4>
                    @endif

                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="pagination">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="text-center paginate-nums">

                                <div class="prevs">
                                    @if($page==2)
                                        <a href="{{ url('jobs') }}?{{ 'page=1' }}{{ isset($_GET['city']) ? '&city='.$_GET['city'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}{{ isset($_GET['industy_id']) ? '&industy_id='.$_GET['industy_id'] : '' }}{{ isset($_GET['employment_type']) ? '&employment_type='.$_GET['employment_type'] : '' }}{{ isset($_GET['experience']) ? '&experience='.$_GET['experience'] : '' }}{{ isset($_GET['career_level']) ? '&career_level='.$_GET['career_level'] : '' }}">1</a>
                                    @elseif(($page-1)>1)
                                        <?php $links = 0;?>
                                        @for($x=($page-1);$x>0;$x--)
                                            @if($links<3)
                                                @if($x==intval($page))
                                                    <span>{{ $x }}</span>
                                                @else
                                                    <a href="{{ url('jobs') }}?{{ 'page='.$x }}{{ isset($_GET['city']) ? '&city='.$_GET['city'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}{{ isset($_GET['industy_id']) ? '&industy_id='.$_GET['industy_id'] : '' }}{{ isset($_GET['employment_type']) ? '&employment_type='.$_GET['employment_type'] : '' }}{{ isset($_GET['experience']) ? '&experience='.$_GET['experience'] : '' }}{{ isset($_GET['career_level']) ? '&career_level='.$_GET['career_level'] : '' }}">{{$x}}</a>
                                                @endif
                                                <?php $links++; ?>
                                            @else
                                                @break
                                            @endif
                                        @endfor
                                    @endif

                                    @if($page>1)
                                        <a href="{{ url('jobs') }}?page={{$page-1}}{{ isset($_GET['city']) ? '&city='.$_GET['city'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}{{ isset($_GET['industy_id']) ? '&industy_id='.$_GET['industy_id'] : '' }}{{ isset($_GET['employment_type']) ? '&employment_type='.$_GET['employment_type'] : '' }}{{ isset($_GET['experience']) ? '&experience='.$_GET['experience'] : '' }}{{ isset($_GET['career_level']) ? '&career_level='.$_GET['career_level'] : '' }}"><</a>
                                    @endif
                                </div>
                                @if($page<=ceil($totalData/$perPage))
                                    <?php $links = 0;?>
                                    <div class="nexts">
                                        @for($x=$page;$x<=ceil($totalData/$perPage);$x++)
                                            @if($links<4)
                                                @if($x==intval($page))
                                                    <span>{{ $x }}</span>
                                                @else
                                                    <a href="{{ url('jobs') }}?page={{$x}}{{ isset($_GET['city']) ? '&city='.$_GET['city'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}{{ isset($_GET['industy_id']) ? '&industy_id='.$_GET['industy_id'] : '' }}{{ isset($_GET['employment_type']) ? '&employment_type='.$_GET['employment_type'] : '' }}{{ isset($_GET['experience']) ? '&experience='.$_GET['experience'] : '' }}{{ isset($_GET['career_level']) ? '&career_level='.$_GET['career_level'] : '' }}">{{$x}}</a>
                                                @endif
                                                <?php $links++; ?>
                                            @else
                                                @break
                                            @endif
                                        @endfor

                                        @if($perPage*$page<$totalData)
                                            <a href="{{ url('jobs') }}?page={{$page+1}}{{ isset($_GET['city']) ? '&city='.$_GET['city'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}{{ isset($_GET['industy_id']) ? '&industy_id='.$_GET['industy_id'] : '' }}{{ isset($_GET['employment_type']) ? '&employment_type='.$_GET['employment_type'] : '' }}{{ isset($_GET['experience']) ? '&experience='.$_GET['experience'] : '' }}{{ isset($_GET['career_level']) ? '&career_level='.$_GET['career_level'] : '' }}">></a>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            Showing {{ $totalData > $perPage? ((($page * $perPage) - $perPage) + 1) . ' - ' . $page * $perPage : $totalData }} of {{ $totalData }}<br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>

        function CustomMarker(latlng, map, args) {
            this.latlng = latlng;
            this.args = args;
            this.setMap(map);
        }

        CustomMarker.prototype = new google.maps.OverlayView();

        CustomMarker.prototype.draw = function() {

            var self = this;

            var div = this.div;

            if (!div) {
                div = this.div = document.createElement('div');

                div.className = 'marker';

                div.style.position = 'absolute';
                div.style.cursor = 'pointer';
                div.style.width = this.args.size+'px';
                div.style.height = this.args.size+'px';
                div.style.borderRadius = '50%';
                div.style.background = '#53284e';
                div.style.lineHeight = this.args.size+'px';
                div.style.textAlign = 'center';
                div.style.color = '#fff';

                var textnode = document.createTextNode(this.args.count);
                div.appendChild(textnode);

                if (typeof(self.args.marker_id) !== 'undefined') {
                    div.dataset.marker_id = self.args.marker_id;
                }

                google.maps.event.addDomListener(div, "click", function(event) {
                    window.location = baseUrl+'/jobs?country='+self.args.country;
                    google.maps.event.trigger(self, "click");
                });

                var panes = this.getPanes();
                panes.overlayImage.appendChild(div);
            }

            var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

            if (point) {
                div.style.left = (point.x - 10) + 'px';
                div.style.top = (point.y - 20) + 'px';
            }
        };

        CustomMarker.prototype.remove = function() {
            if (this.div) {
                this.div.parentNode.removeChild(this.div);
                this.div = null;
            }
        };

        CustomMarker.prototype.getPosition = function() {
            return this.latlng;
        };

        $('#country').on('change',function (){
            id = $(this).find('option:selected').attr('data-id');
            $.ajax({
                type: "GET",
                url: baseUrl+"/get-cities-for-country/"+id,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response){
                    console.log(response);
                    $('#city').find('option').remove();


                    Object.keys(response).map(function(objectKey, index) {
                        var value = response[objectKey];
                        $('#city').append('<option value="'+value.id+'">'+value.name+'</option>');
                    });
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });
    </script>
    @include('partials.map-scripts-job')
@endsection
