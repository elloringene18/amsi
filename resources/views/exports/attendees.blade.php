<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Occupation</th>
        <th>Year Graduated</th>
        <th>Campus</th>
        <th>Mobile Number</th>
        <th>Email</th>
    </tr>
    </thead>
    <tbody>
    @foreach($attendees as $attendee)
        <tr>
            <td>{{ $attendee->firstName }}, {{ $attendee->lastName }}</td>
            <td>{{ $attendee->occupation }}</td>
            <td>{{ $attendee->year_graduated }}</td>
            <td>{{ $attendee->campus }}</td>
            <td>{{ $attendee->mobileNumber }}</td>
            <td>{{ $attendee->email }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
