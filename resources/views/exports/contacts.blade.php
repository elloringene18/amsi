@if(count($data))
    <table>
        <thead>
        <tr>
            @foreach($data[count($data)-1]['items'] as $key=>$item)
                <th>{{$key}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($data as $entry)
            <tr>
                @foreach($entry['items'] as $item)
                    <td>{{ $item }}</td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <table>
        <thead>
        <tr>
            <th>No Entries</th>
        </tr>
        </thead>
    </table>
@endif
