@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public/') }}/css/directory.css">
    <style>
        .item {
            height: auto;
        }
    </style>
@endsection

@section('content')
    <section id="map">
        <div class="container-fluid">
            <div class="row">
                <div class="map-canvas" style="height:600px;"></div>
                <div class="filterbox">
                    <h4>Find the alumni you are looking for by seaching below:</h4>
                    <form method="get" action="{{ url('directory/') }}">
                        <input type="text" name="first_name" placeholder="First Name" class="form-control mb-3" value="{{ isset($_GET['first_name']) ? $_GET['first_name'] : '' }}">
                        <input type="text" name="last_name" placeholder="Last Name" class="form-control mb-3" value="{{ isset($_GET['last_name']) ? $_GET['last_name'] : '' }}">

                        <select name="graduate_year" class="form-control mb-3">

                            <option value="" disabled selected>Year Graduated</option>
                            @for($x=2100;$x>1900;$x--)
                                <option {{ isset($_GET['graduate_year']) ? ($_GET['graduate_year'] == $x ? 'selected' : false ) : false }} value="{{$x}}">{{$x}}</option>
                            @endfor
                        </select>

                        <select name="campus" class="form-control mb-3">
                            <option value="" disabled selected>Campus</option>
                            @foreach($campuses as $campus)
                                <option {{ isset($_GET['campus']) ? ($_GET['campus'] == $campus ? 'selected' : false ) : false }} value="{{$campus}}">{{$campus}}</option>
                            @endforeach
                        </select>

                        <select name="occupation" class="form-control mb-3">
                            <option value="" disabled selected>Occupation</option>
                            @foreach($occupations as $occupation)
                                @if($occupation)
                                    <option {{ isset($_GET['occupation']) ? ($_GET['occupation'] == $occupation ? 'selected' : false ) : false }} value="{{$occupation}}">{{$occupation}}</option>
                                @endif
                            @endforeach
                        </select>
                        <select name="country" class="form-control mb-3">
                            <option value="" disabled selected>Current Location</option>
                            @foreach($countries as $country)
                                <option {{ isset($_GET['country']) ? ($_GET['country'] == $country ? 'selected' : false ) : false }} value="{{$country}}">{{$country}}</option>
                            @endforeach
                        </select>

                        <input type="submit" value="Search" class="form-control">
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="directory">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        @foreach($data as $alumni)
                            <div class="item">
                                <div class="col-md-4 img">
                                    <a href="{{ url('/directory/'.$alumni->id) }}">
                                    <img src="{{ $alumni->photo ? $alumni->photoUrl : asset('public/img/placeholder-user.png') }}" width="100%">
                                    </a>
                                </div>
                                <div class="col-md-8 details">
                                    <a href="{{ url('/directory/'.$alumni->id) }}">
                                        <div class="nameschool">
                                            <span class="name">{{ $alumni->firstName }} {{ $alumni->lastName }}</span>
                                            @if(isset($alumni->education[0]))
                                                <span class="school">{{ $alumni->education[0]->campus }}</span>
                                            @endif
                                        </div>
                                        @if($alumni->city || $alumni->country)
                                            <span class="location">{{ $alumni->city ? $alumni->city.',' : '' }} {{ $alumni->country }}</span>
                                        @else
                                            <span class="location">N/A</span>
                                        @endif
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="pagination">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="text-center paginate-nums">

                                <div class="prevs">
                                    @if($page==2)
                                        <a href="{{ url('directory') }}?{{ 'page=1' }}{{ isset($_GET['first_name']) ? '&first_name='.$_GET['first_name'] : '' }}{{ isset($_GET['last_name']) ? '&last_name='.$_GET['last_name'] : '' }}{{ isset($_GET['graduate_year']) ? '&graduate_year='.$_GET['graduate_year'] : '' }}{{ isset($_GET['campus']) ? '&campus='.$_GET['campus'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}">1</a>
                                    @elseif(($page-1)>1)
                                        <?php $links = 0;?>
                                        @for($x=($page-1);$x>0;$x--)
                                            @if($links<3)
                                                @if($x==intval($page))
                                                    <span>{{ $x }}</span>
                                                @else
                                                    <a href="{{ url('directory') }}?{{ 'page='.$x }}{{ isset($_GET['first_name']) ? '&first_name='.$_GET['first_name'] : '' }}{{ isset($_GET['last_name']) ? '&last_name='.$_GET['last_name'] : '' }}{{ isset($_GET['graduate_year']) ? '&graduate_year='.$_GET['graduate_year'] : '' }}{{ isset($_GET['campus']) ? '&campus='.$_GET['campus'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}">{{$x}}</a>
                                                @endif
                                                <?php $links++; ?>
                                            @else
                                                @break
                                            @endif
                                        @endfor
                                    @endif

                                    @if($page>1)
                                        <a href="{{ url('directory') }}?page={{$page-1}}{{ isset($_GET['first_name']) ? '&first_name='.$_GET['first_name'] : '' }}{{ isset($_GET['last_name']) ? '&last_name='.$_GET['last_name'] : '' }}{{ isset($_GET['graduate_year']) ? '&graduate_year='.$_GET['graduate_year'] : '' }}{{ isset($_GET['campus']) ? '&campus='.$_GET['campus'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}"><</a>
                                    @endif
                                </div>
                                @if($page<=ceil($totalData/$perPage))
                                    <?php $links = 0;?>
                                    <div class="nexts">
                                        @for($x=$page;$x<=ceil($totalData/$perPage);$x++)
                                            @if($links<4)
                                                @if($x==intval($page))
                                                    <span>{{ $x }}</span>
                                                @else
                                                    <a href="{{ url('directory') }}?page={{$x}}{{ isset($_GET['first_name']) ? '&first_name='.$_GET['first_name'] : '' }}{{ isset($_GET['last_name']) ? '&last_name='.$_GET['last_name'] : '' }}{{ isset($_GET['graduate_year']) ? '&graduate_year='.$_GET['graduate_year'] : '' }}{{ isset($_GET['campus']) ? '&campus='.$_GET['campus'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}">{{$x}}</a>
                                                @endif
                                                <?php $links++; ?>
                                            @else
                                                @break
                                            @endif
                                        @endfor

                                        @if($perPage*$page<$totalData)
                                            <a href="{{ url('directory') }}?page={{$page+1}}{{ isset($_GET['first_name']) ? '&first_name='.$_GET['first_name'] : '' }}{{ isset($_GET['last_name']) ? '&last_name='.$_GET['last_name'] : '' }}{{ isset($_GET['graduate_year']) ? '&graduate_year='.$_GET['graduate_year'] : '' }}{{ isset($_GET['campus']) ? '&campus='.$_GET['campus'] : '' }}{{ isset($_GET['country']) ? '&country='.$_GET['country'] : '' }}">></a>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            Showing {{ $totalData > $perPage? ((($page * $perPage) - $perPage) + 1) . ' - ' . $page * $perPage : $totalData }} of {{ $totalData }}<br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>

        function CustomMarker(latlng, map, args) {
            this.latlng = latlng;
            this.args = args;
            this.setMap(map);
        }

        CustomMarker.prototype = new google.maps.OverlayView();

        CustomMarker.prototype.draw = function() {

            var self = this;

            var div = this.div;

            if (!div) {
                div = this.div = document.createElement('div');

                div.className = 'marker';

                div.style.position = 'absolute';
                div.style.cursor = 'pointer';
                div.style.width = this.args.size+'px';
                div.style.height = this.args.size+'px';
                div.style.borderRadius = '50%';
                div.style.background = '#53284e';
                div.style.lineHeight = this.args.size+'px';
                div.style.textAlign = 'center';
                div.style.color = '#fff';

                var textnode = document.createTextNode(this.args.count);
                div.appendChild(textnode);

                if (typeof(self.args.marker_id) !== 'undefined') {
                    div.dataset.marker_id = self.args.marker_id;
                }

                google.maps.event.addDomListener(div, "click", function(event) {
                    window.location = baseUrl+'/directory?country='+self.args.country;
                    google.maps.event.trigger(self, "click");
                });

                var panes = this.getPanes();
                panes.overlayImage.appendChild(div);
            }

            var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

            if (point) {
                div.style.left = (point.x - 10) + 'px';
                div.style.top = (point.y - 20) + 'px';
            }
        };

        CustomMarker.prototype.remove = function() {
            if (this.div) {
                this.div.parentNode.removeChild(this.div);
                this.div = null;
            }
        };

        CustomMarker.prototype.getPosition = function() {
            return this.latlng;
        };
    </script>
    @include('partials.map-scripts')
@endsection
