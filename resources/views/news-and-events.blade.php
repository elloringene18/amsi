@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/news.css?v=2">
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">

    <style>
        .swiper-container {
            width: 100%;
            height: auto;
        }

        .tab {
            display: none;
        }

        .tab.active {
            display: block;
        }

        .tab-switch.active {
            color: #fff !important;
            cursor: not-allowed;
        }

        .tab-switch {
            text-transform: uppercase;
            text-decoration: none !important;
            font-size: 12px;
            font-weight: bold;
        }
    </style>
@endsection

@section('content')

    <section id="content">
        <div class="container">
            <div class="other-news">
                <h1 class="heading">News and Events</h1>
                <div class="row">
                    <div class="col-md-12">
                        <hr/>
                        <a href="#" class="tab-switch purple-bt active" data-type="news">Alumni News & Updates</a>
                        <a href="#" class="tab-switch purple-bt" data-type="events">Events</a>
                        <br/>
                        <br/>
                    </div>
                </div>
                <div class="row tab active" id="news">
                    @foreach($news as $item)
                        <a href="{{ url('article/'.$item->slug) }}">
                            <div class="col-md-4 new-item">
                                <div class="wrap" style="background-image: url({{ $item->thumbnailUrl }})">
                                </div>
                                <h4 class="sub">{{ $item->event_type }}</h4>
                                <h3 class="sub">{{ $item->title }}</h3>
                                <p class="sub">{{ \Carbon\Carbon::parse($item->date)->format('d M Y') }}</p>
                            </div>
                        </a>
                    @endforeach
                </div>
                <div class="row tab" id="events">
                    @if(count($upcomingevents))
                        <br/>
                        <br/>
                        <div class="col-md-12"><h3>Upcoming events</h3></div>
                        @foreach($upcomingevents as $item)
                            <a href="{{ url('event/'.$item->slug) }}">
                                <div class="col-md-4 new-item">
                                    <div class="wrap" style="background-image: url({{ $item->thumbnailUrl }})">
                                    </div>
                                    <h4 class="sub">EVENT</h4>
                                    <h3 class="sub">{{ $item->title }}</h3>
                                    <p class="sub">{{ \Carbon\Carbon::parse($item->date)->format('d M Y') }}</p>
                                </div>
                            </a>
                        @endforeach
                    @endif

                    <div class="col-md-12"><h3>Past events</h3></div>
                    @foreach($events as $item)
                        <a href="{{ url('event/'.$item->slug) }}">
                            <div class="col-md-4 new-item">
                                <div class="wrap" style="background-image: url({{ $item->thumbnailUrl }})">
                                </div>
                                <h4 class="sub">EVENT</h4>
                                <h3 class="sub">{{ $item->title }}</h3>
                                <p class="sub">{{ \Carbon\Carbon::parse($item->date)->format('d M Y') }}</p>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            loop: false,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        });

        function vCenter(){
            $('.vcenter').each(function(){
                ht = $(this).height()/2;
                $(this).css('margin-top','-'+ht+'px');
            });
        }
        vCenter();

        $('.tab-switch').on('click',function(e){
            e.preventDefault();

            $('.tab-switch').removeClass('active');
            $('.tab').removeClass('active');
            $(this).addClass('active');

            if($(this).attr('data-type')=="news")
                $('#news').addClass('active');
            else if($(this).attr('data-type')=="events")
                $('#events').addClass('active');
            else if($(this).attr('data-type')=="businesses")
                $('#businesses').addClass('active');

            vCenter();
        });
    </script>
@endsection
