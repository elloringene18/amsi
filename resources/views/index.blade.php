@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public/') }}/css/home.css?v=2">
    <style>
        .swiper-button-next:after,.swiper-button-prev:after{
            color: #fff;
        }

        .swiper-container {
            position: relative;
        }
    </style>
@endsection

@section('content')
    <section id="slide">
      <div class="container-fluid">
          <div class="row">
                <!-- Slider main container -->
                <div class="swiper-container">
                  <!-- Additional required wrapper -->
                  <div class="swiper-wrapper">
                    <!-- Slides -->
                    @if(count($sliders))
                        @foreach($sliders as $slide)
                        <div class="swiper-slide home-slide" style="background-image:url({{ asset('public/') }}/{{$slide->image}});">
                            <div class="overlay"></div>
                            <div class="content">
                                <div class="container">
                                    <h2 class="main">{{$slide->heading}}</h2>
                                    <h3 class="sub">{{$slide->subheading}}</h3>
                                    @if(!Auth::user())
                                        <a class="link" href="{{ url('register') }}">JOIN OUR NETWORK</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @else
                    <div class="swiper-slide home-slide" style="background-image:url({{ asset('public/') }}/img/home-3.jpg);">
                        <div class="overlay"></div>
                        <div class="content">
                            <div class="container">
                                <h2 class="main">Welcome <span class="yellow">home</span></h2>
                                <h3 class="sub">#AMSI_Alumni</h3>
                                @if(!Auth::user())
                                    <a class="link" href="{{ url('register') }}">JOIN OUR NETWORK</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- Slides -->
                    <div class="swiper-slide home-slide" style="background-image:url({{ asset('public/') }}/img/home-2.jpg);">
                        <div class="overlay"></div>
                        <div class="content">
                            <div class="container">
                                <h2 class="main">Welcome <span class="yellow">home</span></h2>
                                <h3 class="sub">#AMSI_Alumni</h3>
                                @if(!Auth::user())
                                    <a class="link" href="{{ url('register') }}">JOIN OUR NETWORK</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- Slides -->
                    <div class="swiper-slide home-slide" style="background-image:url({{ asset('public/') }}/img/home-1.jpg);">
                        <div class="overlay"></div>
                        <div class="content">
                            <div class="container">
                                <h2 class="main">Welcome <span class="yellow">home</span></h2>
                                <h3 class="sub">#AMSI_Alumni</h3>
                                @if(!Auth::user())
                                    <a class="link" href="{{ url('register') }}">JOIN OUR NETWORK</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- Slides -->
                    <div class="swiper-slide home-slide" style="background-image:url({{ asset('public/') }}/img/home-4.jpg);">
                        <div class="overlay"></div>
                        <div class="content">
                            <div class="container">
                                <h2 class="main">Welcome <span class="yellow">home</span></h2>
                                <h3 class="sub">#AMSI_Alumni</h3>
                                @if(!Auth::user())
                                    <a class="link" href="{{ url('register') }}">JOIN OUR NETWORK</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endif
                  </div>
                  <!-- If we need pagination -->
                  <div class="swiper-pagination"></div>

                  <!-- If we need navigation buttons -->
                  <div class="swiper-button-prev"></div>
                  <div class="swiper-button-next"></div>

                  <!-- If we need scrollbar -->
    <!--              <div class="swiper-scrollbar"></div>-->
                </div>
            </div>
        </div>
    </section>

    <section class="purple-bar">
        <div class="container">
            FIND ALUMNI NEAR YOU
        </div>
    </section>

    <section id="map">
        <div class="container-fluid">
            <div class="row">
                    <div class="map-canvas" style="height:550px;"></div>
            </div>
        </div>
    </section>

    <section class="purple-bar">
        <div class="container">
            NEWS & UPDATES
        </div>
    </section>

    <section id="news">
        <!-- Swiper -->
          <div class="news">
            <div class="swiper-wrapper">
                @foreach($news as $item)
                  <div class="swiper-slide">
                      <a href="{{ url('article/'.$item->slug) }}">
                      <img src="{{ $item->thumbnailUrl }}" width="100%">
                      <div class="overlay"></div>
                        <div class="content">
                            <h2 class="main">{{ $item->title }}</h2>
                            <h3 class="sub">{{ $item->event_type }}</h3>
                        </div>
                      </a>
                  </div>
                @endforeach
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
          </div>
    </section>


    <section class="purple-bar">
        <div class="container">
            EVENTS
        </div>
    </section>

    <section id="events">
        <!-- Swiper -->
          <div class="news">
            <div class="swiper-wrapper">
                @foreach($events as $item)
                  <div class="swiper-slide">
                      <a href="{{ url('article/'.$item->slug) }}">
                      <img src="{{ $item->thumbnailUrl }}" width="100%">
                      <div class="overlay"></div>
                        <div class="content">
                            <h2 class="main">{{ $item->title }}</h2>
                            <h3 class="sub">{{ \Carbon\Carbon::parse($item->date)->format('M, d Y') }}</h3>
                        </div>
                      </a>
                  </div>
                @endforeach
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
          </div>
    </section>
@endsection


@section('js')

    <script>

        function CustomMarker(latlng, map, args) {
            this.latlng = latlng;
            this.args = args;
            this.setMap(map);
        }

        CustomMarker.prototype = new google.maps.OverlayView();

        CustomMarker.prototype.draw = function() {

            var self = this;

            var div = this.div;

            if (!div) {
                div = this.div = document.createElement('div');

                div.className = 'marker';

                div.style.position = 'absolute';
                div.style.cursor = 'pointer';
                div.style.width = this.args.size+'px';
                div.style.height = this.args.size+'px';
                div.style.borderRadius = '50%';
                div.style.background = '#53284e';
                div.style.lineHeight = this.args.size+'px';
                div.style.textAlign = 'center';
                div.style.color = '#fff';

                var textnode = document.createTextNode(this.args.count);
                div.appendChild(textnode);

                if (typeof(self.args.marker_id) !== 'undefined') {
                    div.dataset.marker_id = self.args.marker_id;
                }

                google.maps.event.addDomListener(div, "click", function(event) {
                    window.location = baseUrl+'/directory?country='+self.args.country;
                    google.maps.event.trigger(self, "click");
                });

                var panes = this.getPanes();
                panes.overlayImage.appendChild(div);
            }

            var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

            if (point) {
                div.style.left = (point.x - 10) + 'px';
                div.style.top = (point.y - 20) + 'px';
            }
        };

        CustomMarker.prototype.remove = function() {
            if (this.div) {
                this.div.parentNode.removeChild(this.div);
                this.div = null;
            }
        };

        CustomMarker.prototype.getPosition = function() {
            return this.latlng;
        };

        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            loop: false,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        });

        var swiper2 = new Swiper('.news', {
            slidesPerView: 3,
            spaceBetween: 30,
//          centeredSlides: true,
            loop: false,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },breakpoints: {
                // when window width is >= 320px
                300: {
                    slidesPerView: 1,
                    spaceBetween: 20
                },
                620: {
                    slidesPerView: 2,
                    spaceBetween: 20
                },
                // when window width is >= 640px
                991: {
                    slidesPerView: 3,
                    spaceBetween: 40
                }
            }
        });
    </script>
    @include('partials.map-scripts')
@endsection
