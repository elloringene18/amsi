@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public/css/profile.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/macy.css') }}">
    <style>
        .skill {
            background-color: #dcddde;
            padding: 3px 12px;
            float: left;
            margin: 5px 10px 5px 0;
            border: 1px solid #ccc;
            position: relative;
        }
        .industry {
            background-color: #dcddde;
            padding: 3px 12px;
            float: left;
            margin: 5px 10px 5px 0;
            border: 1px solid #ccc;
            position: relative;
        }
        .colm {
            margin-bottom: 20px;
        }
    </style>
@endsection

@section('content')

    <section id="profile-strip">
        <div class="container-fluid">
            <div class="container">
                <div class="row">

                    @if(Session::has('success'))
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert-success alert">{{ Session::get('success') }}</div>
                            </div>
                        </div>
                    @endif

                    <div class="col-md-4 text-left">
                        @if($job->location)
                            <div class="pull-left">
                                <img src="{{ asset('public/img/profile/location-icon.png') }}">
                            </div>
                            <div class="pull-left ml-3">
                                {{ $job->location }}
                            </div>
                        @endif
                    </div>

                    <div class="col-md-4 text-left">
                        @if($job->title)
                            <div class="pull-left">
                                <img src="{{ asset('public/img/profile/occupation-icon.png') }}">
                            </div>
                            <div class="pull-left ml-3">
                                {{ $job->title }}
                            </div>
                        @endif
                    </div>

                    <div class="col-md-4 text-right">
                        @if(\App\Models\JobApplicant::where('user_id',Auth::user()->id)->where('job_id',$job->id)->count())
                            <a href="{{ url('/cancel-apply-job/'.$job->id) }}" id="downloadResume">CANCEL APPLICATION</a>
                        @else
                            <a href="{{ url('/apply-job/'.$job->id) }}" id="downloadResume">APPLY NOW</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="profile-details">
        <div class="container">
            <div class="row">
                <div class="colm col-md-12">
                    <span class="heading">Details</span>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3">
                                    <span class="lbl">Posted By</span>
                                </div>
                                <div class="col-md-9">
                                    {{ $job->posterName }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12"><hr/></div>
                                <div class="col-md-3">
                                    <span class="lbl">Company</span>
                                </div>
                                <div class="col-md-9">
                                    {{ $job->company }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12"><hr/></div>
                                <div class="col-md-3">
                                    <span class="lbl">Industry</span>
                                </div>

                                <div class="col-md-9">
                                    {{ $job->industryName }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12"><hr/></div>
                                <div class="col-md-3">
                                    <span class="lbl">Employment Type</span>
                                </div>

                                <div class="col-md-9">
                                    {{ $job->employment_type }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12"><hr/></div>
                                <div class="col-md-3">
                                    <span class="lbl">Experience</span>
                                </div>

                                <div class="col-md-9">
                                    {{ $job->experience }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12"><hr/></div>
                                <div class="col-md-3">
                                    <span class="lbl">Career Level</span>
                                </div>

                                <div class="col-md-9">
                                    {{ $job->career_level }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="map-canvas"></div>
                        </div>
                    </div>
                </div>
                <div class="colm col-md-12" >
                    <span class="heading">Job description</span>
                    <div class="row">
                        <div class="col-md-12">
                            {!! $job->details ? $job->details : 'N/A'  !!}
                        </div>
                    </div>
                </div>
                <div class="colm col-md-12" >
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ url('jobs') }}" id="downloadResume">< BACK TO JOB LISTINGS</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')

    <script>
        // Snazzy Map Style - https://snazzymaps.com/style/8097/wy
        var geocoder = new google.maps.Geocoder();
        var address = "{{ $job->location }}";

        function init(){
            var mapStyle=[{"elementType":"geometry","stylers":[{"color":"#212121"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#212121"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#757575"}]},{"featureType":"administrative.country","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"administrative.land_parcel","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#181818"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"poi.park","elementType":"labels.text.stroke","stylers":[{"color":"#1b1b1b"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#2c2c2c"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#8a8a8a"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#373737"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#3c3c3c"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry","stylers":[{"color":"#4e4e4e"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"transit","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#3d3d3d"}]}];


            geocoder.geocode( { 'address': address }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK)
                {
                    // do something with the geocoded result
                    //
                    // results[0].geometry.location.latitude
                    // results[0].geometry.location.longitude
                    var loctn = results[0].geometry.location;

                    var myLatlng = new google.maps.LatLng(loctn.lat(),loctn.lng());

                    // Create the map
                    var map = new google.maps.Map($('.map-canvas')[0], {
                        zoom: 4,
                        styles: mapStyle,
                        mapTypeControl: false,
                        streetViewControl: false,
                        center: myLatlng
                    });

                    markerSize = '40';
                    var marker = new google.maps.LatLng(loctn.lat(),loctn.lng());

                    new google.maps.Marker({
                        position: marker,
                        map,
                        title: "Hello World!",
                    });
                }
            });
        }
        google.maps.event.addDomListener(window, 'init', init());
    </script>
@endsection
