@extends('master')

@section('css')
@endsection

@section('content')
    <section id="directory">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>{{ $data->firstName }} {{ $data->lastName }}</h1>
                    <p>{{ $data->city }}, {{ $data->country }}</p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>

    </script>
@endsection
