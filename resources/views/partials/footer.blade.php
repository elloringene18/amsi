

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-10 left">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="mnu">
                            <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                            <li><a href="{{ url('privacy-agreement') }}">Privacy Agreement</a></li>
                            <li><a href="{{ url('user-agreement') }}">User Agreement</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                    </div>
                </div>
            </div>
            <div class="col-md-2 copyright right">
                <span>
                © 2020 Al mawakeb.
                all rights reserved
                </span>
                <br/>
                <ul class="socials">
                    <li><a href="https://www.facebook.com/AmsiAlumniAssociation" target="_blank" class="fa fa-facebook"></a></li>
                    {{--<li><a href="#"><a href="#" class="fa fa-twitter"></a></a></li>--}}
                    <li><a href="https://www.instagram.com/amsialumni/" target="_blank" class="fa fa-instagram"></a></li>
                    <li><a href="https://twitter.com/amsi_alumni" target="_blank" class="fa fa-twitter"></a></li>
                </ul>
                {{--Designed and developed by <br/>--}}
                {{--<a href="http://thisishatch.com/" target="_blank" class="font-weight-bolder">--}}
                    {{--<img src="{{asset('public/img/hatch-logo.png')}}" style="margin-top: 10px;">--}}
                {{--</a>.--}}

            </div>
        </div>
    </div>
</footer>

<script src="{{ asset('public/') }}/js/vendor/jquery-1.11.2.min.js"></script>

<script src="{{ asset('public/') }}/js/vendor/bootstrap.min.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChdoqnSnfKQL3byDY_Ju6MvoUD0Xds3Tk"></script>

<script>
    var baseUrl = '{{url('/')}}';
</script>

<script src="{{ asset('public/') }}/js/plugins.js"></script>
<script src="{{ asset('public/') }}/js/main.js"></script>

@yield('js')

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-H31LJPWZ29"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-H31LJPWZ29');
</script>
</body>
</html>
