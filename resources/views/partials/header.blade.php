<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>AMSI Alumni Website</title>
    <meta name="description" content="Founded in 2016, the AMSI Alumni Association (AAA) aspires to be a primary link between AMSI’s alumni, students, parents, faculty, and staff, all of whom constitute the AMSI family.">
    <meta property="og:image" content="{{ asset('public/') }}/img/mawakeb-alumni-logo.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{ asset('public/') }}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('public/') }}/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{ asset('public/') }}/css/main.2.css?v=1.2">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('public/img/favicons') }}/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('public/img/favicons') }}//apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('public/img/favicons') }}//apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('public/img/favicons') }}//apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('public/img/favicons') }}//apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('public/img/favicons') }}//apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('public/img/favicons') }}//apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('public/img/favicons') }}//apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/img/favicons') }}//apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('public/img/favicons') }}//android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/img/favicons') }}//favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('public/img/favicons') }}//favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/img/favicons') }}//favicon-16x16.png">
    <link rel="manifest" href="{{ asset('public/img/favicons') }}//manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('public/img/favicons') }}//ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    @yield('css')

    <script src="{{ asset('public/') }}/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <style>
        .swiper-container {
            width: 100%;
            height: auto;
            overflow: hidden;
        }
        .news {
            width: 100%;
            overflow: hidden;
        }
    </style>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

@if(!Auth::user())
<!-- The modal -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h2>Have an account? Login</h2>
                <form method="POST" action="{{ url('/user-login') }}" id="loginForm">
                    @csrf
                    <input type="text" name="email" placeholder="Email" class="form-control mb-3">
                    <input type="password" name="password" placeholder="Password" class="form-control mb-3">
                    <a href="{{ url('/forgot-password') }}" class="pull-right">Forgot Password</a>
                    <br/>
                    <br/>
                    <div id="login-errors"></div>

                    <input type="submit" value="Login" class="form-control">
                </form>
                <div class="linebr">
                    <span class="line"></span>
                    <span class="word">OR</span>
                    <span class="line"></span>
                </div>
                <p class="text-center">Login in with</p>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="{{ url('auth/linkedin') }}" class="fa fa-linkedin" ></a>
                        <br/>
                        <hr/>
                        <p>Not a member yet?<br/><a href="{{ url('register') }}">Register now</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                <a href="{{ url('/') }}"><img src="{{ asset('public/') }}/img/mawakeb-alumni-logo.png" alt="mawakeb alumni logo" id="main-logo"/></a>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="menubar" onclick="myFunction(this)" >
                            <div class="bar1"></div>
                            <div class="bar2"></div>
                            <div class="bar3"></div>
                        </div>
                    </div>
                </div>
                <div class="menus">
                    <div class="row">
                        <div class="col-md-12">
                            @if(!Auth::user())
                                <ul id="login-nav">
                                    <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a></li>
                                    <li><a href="{{ url('register') }}">Join our network</a></li>
{{--                                    <li><a href="{{ url('amsi-voices') }}">AMSI Voices</a></li>--}}
                                </ul>
                            @else
                                <div id="profile-badge">
                                    <div class="left">
                                        <p class="name"><span class="data-firstName">{{ Auth::user()->firstName }}</span> <span class="data-lastName">{{ Auth::user()->lastName }}</span></p>
                                        <p class="campus"><span class="data-campus">{{ Auth::user()->campus }}</span></p>
                                    </div>
                                    <div class="right">
                                        <img src="{{ Auth::user()->photoUrl }}" width="100%" class="user-photo">
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <nav role="navigation">
                                <ul id="main-nav">
                                    <li><a href="{{ url('about-us') }}">About us</a></li>
                                    <li><a href="{{ url('directory') }}">Alumni Directory</a></li>
                                    <li><a href="{{ url('jobs') }}">Job Opportunities</a></li>
                                    <li>
                                        <a href="#">Media</a>
                                        <ul class="sub-menu">
                                            <li><a href="{{ url('news-and-events') }}">News & Events</a></li>
                                            <li><a href="{{ url('alumni-owned-businesses') }}">Alumni Owned Businesses</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Our Campuses</a>
                                        <ul class="sub-menu">
                                            <li><a href="https://almawakeb.sch.ae/our-schools/al-mawakeb-school-al-garhoud" target="_blank">Al Mawakeb Al Garhoud</a></li>
                                            <li><a href="https://almawakeb.sch.ae/our-schools/al-mawakeb-school-al-barsha" target="_blank">Al Mawakeb Al Barsha</a></li>
                                            <li><a href="https://almawakeb.sch.ae/our-schools/al-mawakeb-school-al-khawaneej" target="_blank">Al Mawakeb Al Khawaneej</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">The Archives</a>
                                        <ul class="sub-menu">
                                            <li><a href="https://media.amb.sch.ae/" target="_blank">Al Mawakeb Al Barsha</a></li>
                                            <li><a href="https://media.amg.sch.ae/" target="_blank">Al Mawakeb Al Garhoud</a></li>
                                            <li><a href="https://media.amk.sch.ae/" target="_blank">Al Mawakeb Al Khawaneej</a></li>
                                            <li><a href="https://media.isas.sch.ae/" target="_blank">International School of Arts and Sciences</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{ url('amsi-voices') }}">AMSI Voices</a></li>
                                    <li><a href="{{ url('contact-us') }}">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    @if(Auth::user())
                        <div id="user-menu">
                            <div class="menu-badge">
                                <div class="left">
                                    <img src="{{ Auth::user()->photoUrl }}" width="100%" class="user-photo">
                                </div>
                                <div class="right">
                                    <p class="name"><span class="data-firstName">{{ Auth::user()->firstName }}</span> <span class="data-lastName">{{ Auth::user()->lastName }}</span></p>
                                    <a href="{{ url('my-profile') }}">View Profile</a><br/>
                                    <a href="{{ url('change-password') }}">Change Password</a>
                                    @if(Auth::user()->oauth_id)
                                        <br/>
                                        Your account is linked to LinkedIn
                                    @else
                                        <br/><a href="{{ url('auth/linkedin') }}">Link account with LinkedIn</a>
                                    @endif
                                </div>
                            </div>
                            <ul>
                                <li><a href="{{ url('my-jobs') }}">My Jobs</a></li>
                                <li><a href="{{ url('my-events') }}">My Events</a></li>
                                @if(\Illuminate\Support\Facades\Auth::user())
                                    @if(\Illuminate\Support\Facades\Auth::user()->is_admin)
                                        <li><a href="{{ url('admin/alumnis') }}">Admin Section</a></li>
                                    @endif
                                @endif
{{--                                <li><a href="{{ url('amsi-voices') }}">AMSI Voices</a></li>--}}
                                {{-- <li><a href="https://myschoolshop.ae" target="_blank">My School Shop</a></li> --}}
                                <li><a href="{{ url('logout') }}">Logout</a></li>
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</header>

@if(Auth::user())
    @if(Auth::user()->created_at < \Carbon\Carbon::parse('01-04-2021') && Auth::user()->created_at == Auth::user()->updated_at)
        <div class="alert alert-warning text-center" style="margin-bottom: 0">
            It seems that you haven't updated your password yet. Please update your password <a href="{{ url('change-password') }}"><strong>here</strong></a> to secure your account.
        </div>
    @endif
    @if(isset($accountLinked))
        <div class="alert alert-success text-center" style="margin-bottom: 0">
            Your account has been linked to linkedIn. You may now login using linkedIn the next time you visit.
        </div>
    @endif
@endif
