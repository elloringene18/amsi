
<script>
    // Snazzy Map Style - https://snazzymaps.com/style/8097/wy
    var mapStyle=[{"elementType":"geometry","stylers":[{"color":"#212121"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#212121"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#757575"}]},{"featureType":"administrative.country","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"administrative.land_parcel","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#181818"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"poi.park","elementType":"labels.text.stroke","stylers":[{"color":"#1b1b1b"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#2c2c2c"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#8a8a8a"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#373737"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#3c3c3c"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry","stylers":[{"color":"#4e4e4e"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"transit","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#3d3d3d"}]}];

    var myLatlng = new google.maps.LatLng(25.3391626,55.4051522);

    // // Set up handle bars
    // // var template = Handlebars.compile($('#marker-content-template').html());
    // var closeDelayed = false;
    //
    // // Create the map
    var map = new google.maps.Map($('.map-canvas')[0], {
        zoom: 4,
        styles: mapStyle,
        mapTypeControl: false,
        streetViewControl: false,
        center: myLatlng
    });

    markerSize = '40';

        {{--@foreach($mapData as $index=>$item)--}}
        {{--markerSize = '40';--}}

        {{--var marker{{$index}} = new google.maps.LatLng({{$item['lat']}},{{$item['long']}});--}}

        {{--if(parseInt("{{$item['total']}}") > 1000)--}}
        {{--markerSize = '80';--}}
        {{--else if(parseInt("{{$item['total']}}") > 500)--}}
        {{--markerSize = '60';--}}
        {{--else if(parseInt("{{$item['total']}}") > 100)--}}
        {{--markerSize = '50';--}}

        {{--new CustomMarker( marker{{$index}}, map, { count: '{{$item['total']}}', size: markerSize  });--}}
        {{--@endforeach--}}



    var locations = [];
    function init(){
        $.ajax({
            type: "GET",
            url: baseUrl+'/get-locations-for-map',
            async: false,
            cache: false,
            success: function(response){

                Object.keys(response).map(function(objectKey, index) {
                    value = response[objectKey];

                    myLatlng = new google.maps.LatLng(value.lat,value.lng);

                    if (value.total < 100)
                        markerSize = '30';
                    else if (value.total < 300)
                        markerSize = '50';
                    else if (value.total < 600)
                        markerSize = '60';
                    else if (value.total < 900)
                        markerSize = '70';
                    else if (value.total > 1000)
                        markerSize = '80';

                    new CustomMarker( myLatlng, map, { count: value.total, size: markerSize, country : value.country  });
                });

            },
            statusCode: {
                401: function() {
                    window.location.href = '{{url('login')}}'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    }

    google.maps.event.addDomListener(window, 'init', init());
</script>
