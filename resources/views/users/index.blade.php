@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public/css/profile.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/macy.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" integrity="sha256-jKV9n9bkk/CTP8zbtEtnKaKf+ehRovOYeKoyfthwbC8=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
    <style>
        .skill {
            background-color: #dcddde;
            padding: 3px 12px;
            float: left;
            margin: 5px 10px 5px 0;
            border: 1px solid #ccc;
            position: relative;
        }
        .industry {
            background-color: #dcddde;
            padding: 3px 12px;
            float: left;
            margin: 5px 10px 5px 0;
            border: 1px solid #ccc;
            position: relative;
        }
        .deleteTag {
            position: absolute;
            width: 15px;
            height: 15px;
            top: -5px;
            right: -5px;
            cursor: pointer;
        }
        .deleteTag::before {
            display: block;
            content: "x";
            line-height: 15px;
            text-align: center;
            width: 15px;
            height: 15px;
            font-size: 10px;
            background-color: #53284e;
            border-radius: 50%;
            color: #ffcc00 !important;
        }
        .lbl {
            margin-bottom: 10px;
            display: inline-block;
        }
    </style>
@endsection

@section('content')
    <?php $data = Auth::user(); ?>

    <?php
        $months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ];

        $color = [
            '#084B83',
            '#BBE6E4',
            '#F0F6F6',
            '#FF66B3',
            '#264653',
            '#2a9d8f',
            '#f4a261',
            '#e76f51',
            '#22223b',
            '#4a4e69',
            '#9a8c98',
            '#c9ada7',
            '#f2e9e4',
            '#4d4d4d',
        ]
    ?>
    <?php
        $campuses = [
            'Al Mawakeb School-Al Garhoud',
            'International School of Arts and Sciences',
            'Al Mawakeb School-Al Barsha',
            'Al Mawakeb Al Khawaneej',
            'Al Mawakeb'
        ];
    ?>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="img-container">
                        <div class="row">
                            <div class="col-md-12">
                                <img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="purple-bt" id="crop">Save</button>
                </div>
            </div>
        </div>
    </div>

    <section id="profile-cover" class="user-cover" style="background-image: url({{ $data->cover ? asset('public/'.$data->cover) : ''}})">
        <div class="container-fluid">
            <div class="container">
                <div class="edit-wrap pull-right">
                    <div class="hideOnEdit">
                        <a href="#" class="editBt float-right">Edit Cover</a>
                    </div>
                    <div class="showOnEdit">
                        <form action="{{url('/my-profile/update-cover')}}" class="ajxFormCover">
                            @csrf
                            <label>Upload Cover Photo:</label>
                            <input type="file" name="photo" class="form-control" accept="image/*">

                            <input type="submit" value="Update">
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-5 col-xs-6 text-left edit-wrap">
                        <div class="hideOnEdit">
                            <a href="#" class="editBt">Change Profile Photo</a>
                            <img src="{{$data->photoUrl}}" class="photo user-photo">
                        </div>
                        <div class="showOnEdit">
                            <form action="{{url('/my-profile/update-photo')}}" class="ajxFormPhoto">
                                @csrf
                                <label>Upload Photo:</label>
                                <input type="file" name="photo" class="form-control" accept="image/*" id="profile-photo">
                            </form>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-7 col-sm-9 relative">
                        <div class="wrp">
                            <div class="vcenter edit-wrap">
                                <div class="hideOnEdit" style="color:{{$data->font_color}}" id="name-font">
                                    <a href="#" class="editBt">Edit</a>
                                    <span class="name"><span class="data-firstName">{{ $data->firstName }}</span> <span class="data-lastName">{{ $data->lastName }}</span></span>
                                    <span class="school"><span class="data-campus">{{ $data->campus }}</span> <span class="data-year_graduated">{{ $data->year_graduated }}</span></span>
                                </div>
                                <div class="showOnEdit">
                                    <form action="{{url('/my-profile/update-fields')}}" class="ajxForm">
                                        @csrf
                                        <label>First Name:</label>
                                        <input type="text" name="firstName" class="form-control" value="{{ $data->firstName }}">
                                        <label>Last Name:</label>
                                        <input type="text" name="lastName" class="form-control" value="{{ $data->lastName }}">
                                        <label>Campus:</label>
                                        <select name="campus" class="form-control">
                                            @foreach($campuses as $campus)
                                                <option {{ $data->campus == $campus ? 'selected' : false}} value="{{$campus}}">{{$campus}}</option>
                                            @endforeach
                                        </select>
                                        <label>Year Graduated:</label>
                                        <select name="year_graduated" class="form-control">
                                            @for($x=1960;$x<=\Carbon\Carbon::now()->format('Y');$x++)
                                                <option {{ $data->year_graduated == $x ? 'selected' : false}} value="{{$x}}">{{$x}}</option>
                                            @endfor
                                        </select>
                                        <label>Font Color:</label>
                                        <input type="color" id="head" name="font_color" value="{{ $data->font_color }}">
                                        <br/>
                                        <input type="submit" value="Update">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="profile-strip">
        <div class="container-fluid">
            <div class="container">
                <div class="row">

                    <div class="col-md-4 text-left stripcol">
                        <div class="pull-left">
                            <img src="{{ asset('public/img/profile/location-icon.png') }}">
                        </div>
                        <div class="pull-left ml-3 edit-wrap">
                            <div class="hideOnEdit">
                                Lives in <br/> <span class="data-city">{{ $data->city ? $data->city.',' : '' }}</span> <span class="data-country">{{ $data->country }}</span>
                                <a href="#" class="editBt">Edit</a>

                            </div>
                            <div class="showOnEdit">
                                <form action="{{url('/my-profile/update-location')}}" class="ajxForm" method="post">
                                    @csrf
                                    <label>Country</label>
                                    <select name="country" class="form-control" id="country">
                                        @foreach($countries as $country)
                                            <option {{  Auth::user()->country == $country->name ? 'selected' : '' }} value="{{$country->name}}" data-id="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    <label>City</label>
                                    <select class="form-control" name="city" id="city">
                                    </select>

                                    <input type="submit" value="Update">
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 text-left stripcol">
                        <div class="pull-left">
                            <img src="{{ asset('public/img/profile/occupation-icon.png') }}">
                        </div>
                        <div class="pull-left ml-3 edit-wrap">
                            <div class="hideOnEdit">
                                Occupation <br/>
                                <span class="data-occupation">{{ $data->jobTitle }}</span>
                                <a href="#" class="editBt">Edit</a>
                            </div>
                            <div class="showOnEdit">
                                <form action="{{url('/my-profile/update-fields')}}" class="ajxForm">
                                    @csrf
                                    <label>Occupation</label>
                                    <input type="text" class="form-control" name="occupation" value="{{ $data->occupation }}">
                                    <input type="submit" value="Update">
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 text-right edit-wrap stripcol">
                        <div class="hideOnEdit">
                            <a href="#" class="editBt purple-bt">
                                {{--<img src="{{ asset('public/img/profile/download-icon.png') }}"> --}}
                                UPLOAD RESUME
                            </a>
                            <a href="{{ asset('public/'.$data->resume) }}" id="downloadResume" class="purple-bt" target="_blank" style="{{!$data->resume?'display:none':''}}">
                                <img src="{{ asset('public/img/profile/download-icon.png') }}">
                                DOWNLOAD RESUME
                            </a>
                        </div>
                        <div class="showOnEdit">
                            <form action="{{url('/my-profile/update-resume')}}" class="ajxFormResume">
                                @csrf
                                <label>Resume (PDF/DOC/DOCX)</label>
                                <input type="file" class="form-control" name="resume" value="" accept=".pdf,.doc,.docx">
                                <input type="submit" value="Upload">
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="profile-details">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="macy-container">
                        <div class="colm" macy-complete="1">
                            <span class="heading">About</span>
                            <div class="row">
                                <div class="col-md-3">
                                    <span class="lbl">Summary</span>
                                </div>
                                <div class="col-md-9 edit-wrap">

                                    <div class="hideOnEdit">
                                        <span class="data-bio">{{ $data->bio }}</span>
                                        @if($data->bio)
                                            <br/>
                                        @endif
                                        <a href="#" class="editBt">Edit</a>
                                    </div>
                                    <div class="showOnEdit">
                                        <form action="{{url('/my-profile/update-fields')}}" class="ajxForm">
                                            @csrf
                                            <label>Bio</label>
                                            <textarea class="form-control" name="bio">{{ $data->bio }}</textarea>
                                            <input type="submit" value="Update">
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <span class="lbl">Skills</span>
                                </div>

                                <div class="col-md-9">
                                    <div class="edit-wrap">
                                        <div class="hideOnEdit">
                                            <div id="currentSkills">
                                                @foreach($data->skills as $skill)
                                                    <div class="skill" id="skill-{{$skill->id}}">{{$skill->name}}<span class="deleteTag" data-id="{{$skill->id}}" data-type="skill"></span></div>
                                                @endforeach
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a href="#" class="editBt">Add new Skill</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="showOnEdit">
                                            <form action="{{url('/my-profile/add-skills')}}" class="ajxFormSkills">
                                                @csrf
                                                <select name="id">
                                                    @foreach($skills as $sk)
                                                        <option value="{{$sk->id}}">{{$sk->name}}</option>
                                                    @endforeach
                                                </select>
                                                <input type="submit" value="Add">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <span class="lbl">Industries</span>
                                </div>

                                <div class="col-md-9">
                                    <div class="edit-wrap">
                                        <div class="hideOnEdit">
                                            <div id="currentIndustries">
                                                @foreach($data->industries as $industry)
                                                    <div class="industry" id="industry-{{$industry->id}}">{{$industry->name}}<span class="deleteTag" data-id="{{$industry->id}}" data-type="industry"></span></div>
                                                @endforeach
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a href="#" class="editBt">Add new Industry</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="showOnEdit">
                                            <form action="{{url('/my-profile/add-industry')}}" class="ajxFormIndustries">
                                                @csrf
                                                <select name="id">
                                                    @foreach($industries as $sk)
                                                        <option value="{{$sk->id}}">{{$sk->name}}</option>
                                                    @endforeach
                                                </select>
                                                <input type="submit" value="Add">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="colm edit-wrap">
                            <span class="heading">Contact</span>
                            <div class="hideOnEdit">
                                <a href="#" class="editBt pull-right">Edit</a>
                                <div class="row">
                                    <div class="col-md-3">
                                        <span class="lbl">Email</span>
                                    </div>
                                    <div class="col-md-9">
                                        <span class="data-email">{{ $data->email }}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12"><hr/></div>
                                    <div class="col-md-3">
                                        <span class="lbl">Phone Number</span>
                                    </div>
                                    <div class="col-md-9">
                                        <span class="data-mobileNumber">{{ $data->mobileNumber }}</span>
                                    </div>
                                </div>
                                {{--<div class="row">--}}
                                    {{--<div class="col-md-12"><hr/></div>--}}
                                    {{--<div class="col-md-3">--}}
                                        {{--<span class="lbl">Address</span>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-9">--}}
                                        {{--<span class="data-city">{{ $data->city ? $data->city.',' : '' }} {{ $data->country }}</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                @if($data->city || $data->country)
                                    <div class="row">
                                        <div class="col-md-12"><hr/></div>
                                        <div class="col-md-12">
                                            <span class="lbl">Location</span>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="map-canvas"></div>
                                        </div>
                                    </div>
                                @endif

                            </div>
                            <div class="showOnEdit">
                                <form action="{{url('/my-profile/update-fields')}}" class="ajxForm">
                                    @csrf
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-12">--}}
                                            {{--<label>Current Location (Country):</label>--}}
                                            {{--<select name="country" class="form-control">--}}
                                                {{--@foreach($countries as $country)--}}
                                                    {{--<option {{  Auth::user()->country == $country ? 'selected' : '' }} value="{{$country}}">{{$country}}</option>--}}
                                                {{--@endforeach--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-12">--}}
                                            {{--<label>Current Location (City):</label>--}}
                                            {{--<x-input class="form-control" type="text" name="city" :value="old('city')" required autofocus  value="{{Auth::user()->city}}"/>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}


                                    <!-- Email Address -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <x-label for="email" :value="__('Email')" />
                                            <x-input id="email" class="form-control" type="email" name="email" :value="old('email')" required value="{{Auth::user()->email}}"/>
                                        </div>
                                        <div class="col-md-12">
                                            <x-label for="email" :value="__('Show email to public?')" />
                                            <select name="hide_email" class="form-control">
                                                <option {{ $data->hide_email == 0 ? 'selected' : '' }} value="0">Yes</option>
                                                <option {{ $data->hide_email == 1 ? 'selected' : '' }} value="1">No</option>
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                            <x-label for="mobileNumber" :value="__('Contact Number')" />
                                            <x-input id="email" class="form-control" type="text" name="mobileNumber" :value="old('mobileNumber')" value="{{Auth::user()->mobileNumber}}"/>
                                        </div>
                                        <div class="col-md-12">
                                            <x-label for="email" :value="__('Show contact number to public?')" />
                                            <select name="hide_mobile" class="form-control">
                                                <option value="0" {{ $data->hide_mobile == 0 ? 'selected' : '' }} >Yes</option>
                                                <option value="1" {{ $data->hide_mobile == 1 ? 'selected' : '' }} >No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br/>
                                    <input type="submit" value="Update">
                                </form>
                            </div>
                        </div>
                        <div class="colm edit-wrap">
                            <span class="heading">Experience</span>
                            <div id="currentCareers">
                                @foreach($data->career as $career)
                                    <div class="row edit-wrap" id="career-{{$career->id}}">
                                        <div class="hideOnEdit">
                                            <div class="col-md-2 col-sm-2 col-xs-4 icon">
                                                <img src="{{asset('public/img/profile/career-icon.png')}}" width="100%">
                                            </div>
                                            <div class="col-md-9 col-sm-9 col-xs-8 relative">
                                                <p class="title" field="jobTitle">{{ $career->jobTitle }}</p>
                                                <p field="employer">{{ $career->employer }}</p>
                                                <p field="duration">{{ $career->startYear }}{{ $career->endYear ? ' - ' . $career->endYear : '' }} </p>
                                            </div>
                                            <div class="col-md-12"><hr/></div>
                                            <button class="deleteItem" data-id="{{ $career->id }}" data-type="career">X</button>
                                            <a href="#" class="editItem" data-id="{{$career->id}}" data-type="career">Edit</a>
                                        </div>
                                        <div class="showOnEdit">
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div class="edit-wrap">
                                <div class="hideOnEdit">
                                    <a href="#" class="editBt">Add new Career</a>
                                </div>
                                <div class="showOnEdit">
                                    <form action="{{url('/my-profile/add-career')}}" class="ajxAddCareer">
                                        @csrf
                                        <div class="educ">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Company</label>
                                                    <input type="text" name="career[0][employer]" class="form-control" value="">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Position</label>
                                                    <input type="text" name="career[0][jobTitle]" class="form-control" value="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Month Joined</label>
                                                    <select name="career[0][startMonth]" class="form-control">
                                                        @foreach($months as $month)
                                                            <option value="{{$month}}">{{$month}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Year Joined</label>
                                                    <select name="career[0][startYear]" class="form-control">
                                                        @for($x=1900;$x<2100;$x++)
                                                            <option value="{{$x}}">{{$x}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Month Ended</label>

                                                    <select name="career[0][endMonth]" class="form-control">
                                                        <option value="Current">Current</option>
                                                        @foreach($months as $month)
                                                            <option value="{{$month}}">{{$month}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Year Ended</label>
                                                    <select name="career[0][endYear]" class="form-control">
                                                        <option value="Current">Current</option>
                                                        @for($x=1900;$x<2100;$x++)
                                                            <option value="{{$x}}">{{$x}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <br/>
                                        </div>

                                        <input type="submit" value="Add">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="colm edit-wrap">
                            <span class="heading">Portfolio</span>
                            <div id="currentPortfolio">
                                @foreach($data->portfolio as $portfolio)
                                    <div class="row edit-wrap" id="portfolio-{{$portfolio->id}}">
                                        <div class="hideOnEdit">
                                            <a href="{{ $portfolio->url }}" download="{{ $portfolio->original_name }}" target="_blank">
                                                <div class="col-md-2 col-sm-2 col-xs-4 icon">
                                                    <img src="{{asset('public/img/profile/portfolio-icon.png')}}" width="100%">
                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-8 relative">
                                                    <p class="title" field="name">{{ $portfolio->title }}</p>
                                                </div>
                                            </a>
                                            <div class="col-md-12"><hr/></div>
                                            <button class="deleteItem" data-id="{{ $portfolio->id }}" data-type="portfolio">X</button>
                                            <a href="#" class="editItem" data-id="{{$portfolio->id}}" data-type="portfolio">Edit</a>
                                        </div>
                                        <div class="showOnEdit">
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="edit-wrap">
                                <div class="hideOnEdit">
                                    <a href="#" class="editBt">Add new file</a>
                                </div>

                                <div class="showOnEdit">
                                    <form action="{{url('/my-profile/add-portfolio')}}" class="ajxFormPortfolio">
                                        @csrf
                                        <div class="educ">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>File Name</label>
                                                    <input type="text" name="title" class="form-control" value="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label>File</label>
                                                    <input type="file" name="file" class="form-control" value="" id="fileUpload" accept=".doc,.docx,.pdf">
                                                </div>
                                            </div>
                                            <br/>
                                        </div>

                                        <input type="submit" value="Add">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="colm edit-wrap">
                            <span class="heading">Education</span>

                            <div id="currentEducations">
                                @foreach($data->education as $education)
                                    <div class="row edit-wrap" id="education-{{$education->id}}">
                                        <div class="hideOnEdit">
                                            <div class="col-md-2 col-sm-2 col-xs-4 icon">
                                                <img src="{{asset('public/img/profile/education.png')}}" width="100%">
                                            </div>
                                            <div class="col-md-9 col-sm-9 col-xs-8">
                                                <p class="title"  field="school">{{ $education->school }}</p>
                                                @if($education->degree)
                                                    <p field="degree">{{ $education->degree }}</p>
                                                @endif
                                                <p field="duration">{{ $education->yearJoin }}{{ $education->classYear ? ' - ' . $education->classYear : false }}</p>

                                            </div>
                                            <div class="col-md-12"><hr/></div>
                                            <button class="deleteItem" data-id="{{ $education->id }}" data-type="education">X</button>
                                            <a href="#" class="editItem" data-id="{{$education->id}}" data-type="education">Edit</a>
                                        </div>
                                        <div class="showOnEdit"></div>
                                    </div>
                                @endforeach
                            </div>

                            <div class="edit-wrap">
                                <div class="hideOnEdit">
                                    <a href="#" class="editBt">Add new Education</a>
                                </div>

                                <div class="showOnEdit">
                                    <form action="{{url('/my-profile/add-education')}}" class="ajxAddEducation">
                                        @csrf
                                        <div class="educ">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>School</label>
                                                    <input type="text" name="education[0][school]" class="form-control" value="">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Degree</label>
                                                    <input type="text" name="education[0][degree]" class="form-control" value="">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Year Joined</label>
                                                    <input type="text" name="education[0][yearJoin]" class="form-control" value="">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Year Graduated</label>
                                                    <input type="text" name="education[0][classYear]" class="form-control" value="">
                                                </div>
                                            </div>
                                            <br/>
                                        </div>

                                        <input type="submit" value="Add">
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="colm edit-wrap">
                            <label>Do you have a child/children in AMSI Schools?</label>
                            <select name="has_child" class="form-control" id="childSelect">
                                <option value="0" {{ Auth::user()->hasChild == 0 ? 'selected' : '' }}>No</option>
                                <option value="1" {{ Auth::user()->hasChild == 1 ? 'selected' : '' }}>Yes</option>
                            </select>
                        </div>

                        <div class="colm">
                            <label>How would you like to get involved? (Select all that apply)</label>
                            <br/>
                            <br/>
                            <?php $involves = \App\Models\Involve::get(); ?>
                            <div class="row">
                                @foreach($involves as $involve)
                                    <div class="col-md-6">
                                        @if(count(Auth::user()->involvement))
                                            <label for="vol-{{$involve->id}}"><input class="involvement-check" {{ in_array($involve->id, Auth::user()->involvement) ? 'checked' : '' }} type="checkbox" name="involvement[]" value="{{$involve->id}}" id="vol-{{$involve->id}}"> {{$involve->title}}</label><br/>
                                        @else
                                            <label for="vol-{{$involve->id}}"><input class="involvement-check" type="checkbox" name="involvement[]" value="{{$involve->id}}" id="vol-{{$involve->id}}"> {{$involve->title}}</label><br/>
                                        @endif
                                        {{$involve->details}}
                                        <br/>
                                        <br/>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('public/js/macy.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js" integrity="sha256-CgvH7sz3tHhkiVKh05kSUgG97YtzYNnWt6OXcmYzqHY=" crossorigin="anonymous"></script>
    <script>
        var masonry = new Macy({
            container: '#macy-container',
            trueOrder: false,
            waitForImages: false,
            useOwnImageLoader: false,
            debug: true,
            mobileFirst: true,
            columns: 1,
            margin: {
                y: 35,
                x: 15,
            },
            breakAt: {
                1200: 2,
                940: 2,
                520: 1,
                400: 1
            },
        });

        function refreshMacy(){
            setTimeout(function(){
                masonry.recalculate(true);
            },100);
        }

        // Snazzy Map Style - https://snazzymaps.com/style/8097/wy
        @if($data->city || $data->country)
            var geocoder = new google.maps.Geocoder();
            var address = "{{ $data->city }} {{ $data->country }}";

            function init(){
                var mapStyle=[{"elementType":"geometry","stylers":[{"color":"#212121"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#212121"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#757575"}]},{"featureType":"administrative.country","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"administrative.land_parcel","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#181818"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"poi.park","elementType":"labels.text.stroke","stylers":[{"color":"#1b1b1b"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#2c2c2c"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#8a8a8a"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#373737"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#3c3c3c"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry","stylers":[{"color":"#4e4e4e"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"transit","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#3d3d3d"}]}];
                geocoder.geocode( { 'address': address}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK)
                    {
                        // do something with the geocoded result
                        //
                        // results[0].geometry.location.latitude
                        // results[0].geometry.location.longitude
                        var loctn = results[0].geometry.location;

                        var myLatlng = new google.maps.LatLng(loctn.lat(),loctn.lng());

                        // Create the map
                        var map = new google.maps.Map($('.map-canvas')[0], {
                            zoom: 4,
                            styles: mapStyle,
                            mapTypeControl: false,
                            streetViewControl: false,
                            center: myLatlng
                        });

                        markerSize = '40';
                        var marker = new google.maps.LatLng(loctn.lat(),loctn.lng());

                        new google.maps.Marker({
                            position: marker,
                            map,
                        });
                    }
                });
            }
            google.maps.event.addDomListener(window, 'init', init());
        @endif
            
        $('.editBt').on('click',function(e){
            e.preventDefault();
            $(this).closest('.edit-wrap').find('.hideOnEdit').hide();
            $(this).closest('.edit-wrap').find('.showOnEdit').show();

            setTimeout(function(){
                masonry.recalculate(true);
            },100);
        });

        $('.ajxFormPhoto').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = new FormData($(this)[0]);
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function(response){

                    $('.user-photo').each(function(){
                        $(this).attr('src',response.data);
                    });

                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('.ajxFormCover').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = new FormData($(this)[0]);
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function(response){
                    console.log(response);
                    $('#profile-cover').css('background-image','url('+response.data+')');
                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('.involvement-check').on('change', function(e){

            id = $(this).val();
            checked = $(this).is(":checked");

            $.ajax({
                type: "POST",
                url: baseUrl+'/involvements/update',
                data: { involve_id : id, checked : checked, _token : '{{ @csrf_token() }}'  },
                success: function(response){
                    console.log(response);
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('.ajxFormResume').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = new FormData($(this)[0]);
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function(response){
                    $('#downloadResume').attr('href',response.data).show();
                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('.ajxForm').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(response){

                    console.log(response);
                    Object.keys(response.data).map(function(objectKey, index) {
                        var value = response.data[objectKey];
                        $('.data-'+objectKey).html(value);

                        if(objectKey=='font_color'){
                            $('#name-font').css('color',value);
                        }
                    });

                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('.ajxAddCareer').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(response){

                    Object.keys(response.data).map(function(objectKey, index) {
                        value = response.data[objectKey];
                        rowId =  value.id;

                        item = '<div class="row edit-wrap" id="career-'+value.id+'">\n' +
                            '    <div class="hideOnEdit">\n' +
                            '        <div class="col-md-2 col-sm-2 col-xs-4  icon">\n' +
                            '            <img src="{{asset('public/img/profile/career-icon.png')}}" width="100%">\n' +
                            '        </div>\n' +
                            '        <div class="col-md-9 col-sm-9 col-xs-8  relative">\n' +
                            '            <p class="title" field="jobTitle">'+value.jobTitle+'</p>\n' +
                            '            <p field="employer">'+value.employer+'</p>\n' +
                            '            <p field="duration">'+value.startYear+' - '+value.endYear+'</p>\n' +
                            '        </div>\n' +
                            '        <div class="col-md-12"><hr/></div>\n' +
                            '        <button class="deleteItem" data-id="'+value.id+'" data-type="career">X</button>\n' +
                            '        <a href="#" class="editItem" data-id="'+value.id+'" data-type="career">Edit</a>\n' +
                            '    </div>\n' +
                            '    <div class="showOnEdit">\n' +
                            '    </div>\n' +
                            '</div>';
                    });

                    $('#currentCareers').append(item);

                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                    refreshMacy();
                    addEditItemClick();
                    addDeleteItemClick();
                }
            });
        });

        $('.ajxAddEducation').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(response){

                    console.log(response);

                    Object.keys(response.data).map(function(objectKey, index) {
                        value = response.data[objectKey];
                        rowId =  value.id;

                        item = '<div class="row edit-wrap" id="education-'+value.id+'">\n' +
                            '    <div class="hideOnEdit">\n' +
                            '        <div class="col-md-2 col-sm-2 col-xs-4 icon">\n' +
                            '            <img src="{{asset('public/img/profile/education.png')}}" width="100%">\n' +
                            '        </div>\n' +
                            '        <div class="col-md-9 col-sm-9 col-xs-8">\n' +
                            '            <p class="title"  field="school">'+value.school+'</p>\n' +
                            '                <p field="degree">'+value.degree+'</p>\n' +
                            '            <p field="duration">'+value.yearJoin+' - '+value.classYear+'</p>\n' +
                            '\n' +
                            '        </div>\n' +
                            '        <div class="col-md-12"><hr/></div>\n' +
                            '        <button  class="deleteItem" data-id="'+value.id+'" data-type="education">X</button>\n' +
                            '        <a href="#" class="editItem" data-id="'+value.id+'" data-type="education">Edit</a>\n' +
                            '    </div>\n' +
                            '    <div class="showOnEdit"></div>\n' +
                            '</div>';
                    });

                    $('#currentEducations').append(item);

                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                    refreshMacy();
                    addEditItemClick();
                    addDeleteItemClick();
                }
            });
        });

        $('.ajxFormPortfolio').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = new FormData($(this)[0]);
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function(response){

                    if(response.data){

                        item = '<div class="row edit-wrap" id="portfolio-'+response.data.id+'">\n' +
                            '    <div class="hideOnEdit">\n' +
                            '        <a href="'+response.data.file+'" download="'+response.data.original_name+'" target="_blank">\n' +
                            '            <div class="col-md-2 col-sm-2 col-xs-4 icon">\n' +
                            '                <img src="{{asset('public/img/profile/portfolio-icon.png')}}" width="100%">\n' +
                            '            </div>\n' +
                            '            <div class="col-md-9 col-sm-9 col-xs-8 relative">\n' +
                            '                <p class="title" field="name">'+response.data.title+'</p>\n' +
                            '            </div>\n' +
                            '        </a>\n' +
                            '        <div class="col-md-12"><hr/></div>\n' +
                            '        <button class="deleteItem" data-id="'+response.data.id+'" data-type="portfolio">X</button>\n' +
                            '        <a href="#" class="editItem" data-id="'+response.data.id+'" data-type="portfolio">Edit</a>\n' +
                            '    </div>\n' +
                            '    <div class="showOnEdit">\n' +
                            '    </div>\n' +
                            '</div>';

                        $('#currentPortfolio').append(item);

                        $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                        $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                    }


                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                    refreshMacy();
                    addEditItemClick();
                    addDeleteItemClick();
                }
            });
        });

        $('.ajxFormSkills').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(response){

                    item = '<div class="skill" id="skill-'+response.data.id+'">'+response.data.name+'<span class="deleteTag" data-id="'+response.data.id+'" data-type="skill"></span></div>';
                    $('#currentSkills').append(item);

                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                    refreshMacy();
                    addDeleteTagClick();
                }
            });
        });

        $('.ajxFormIndustries').on('submit', function(e){
            var form = $(this);

            e.preventDefault();
            e.stopPropagation();

            data = form.serialize();
            url = form.attr('action');

            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function(response){

                    item = '<div class="industry" id="industry-'+response.data.id+'">'+response.data.name+'<span class="deleteTag" data-id="'+response.data.id+'" data-type="industry"></span></div>';
                    $('#currentIndustries').append(item);

                    $(form).closest('.edit-wrap').find('.hideOnEdit').show();
                    $(form).closest('.edit-wrap').find('.showOnEdit').hide();
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                    refreshMacy();
                    addDeleteTagClick();
                }
            });
        });

        function addDeleteItemClick(){
            $('.deleteItem').unbind('click');

            $('.deleteItem').on('click',function(e){
                console.log('click');
                e.preventDefault();
                clickedElement = $(this);
                type = clickedElement.attr('data-type');
                id = clickedElement.attr('data-id');

                $.ajax({
                    type: "POST",
                    url: baseUrl+'/my-profile/delete-'+type,
                    data: { id : id , _token : '{{ @csrf_token() }}' },
                    success: function(response){
                        console.log('#'+type+'-'+response.data);
                        $('#'+type+'-'+response.data).remove();
                    },
                    statusCode: {
                        401: function() {
                            window.location.href = '{{url('login')}}'; //or what ever is your login URI
                        }
                    },
                    complete : function (event,error){
                        refreshMacy();
                    }
                });
            });
        }
        addDeleteItemClick();

        function addDeleteTagClick(){
            $('.deleteTag').unbind('click');

            $('.deleteTag').on('click',function(e){
                e.preventDefault();

                clickedElement = $(this);
                type = clickedElement.attr('data-type');
                id = clickedElement.attr('data-id');

                $.ajax({
                    type: "POST",
                    url: baseUrl+'/my-profile/delete-tag-'+type,
                    data: { id : id , _token : '{{ @csrf_token() }}' },
                    success: function(response){
                        $('#'+type+'-'+response.data).remove();
                    },
                    statusCode: {
                        401: function() {
                            window.location.href = '{{url('login')}}'; //or what ever is your login URI
                        }
                    },
                    complete : function (event,error){
                        refreshMacy();
                    }
                });
            });
        }
        addDeleteTagClick();

        function addEditItemClick(){

            $('.editItem').unbind('click');

            $('.editItem').on('click',function(e){
                e.preventDefault();
                clickedElement = $(this);
                type = clickedElement.attr('data-type');
                id = clickedElement.attr('data-id');

                $.ajax({
                    type: "GET",
                    url: baseUrl+'/my-profile/get-'+type+'/'+id,
                    success: function(response){
                        console.log(response);
                        if(response.data){
                            if(type=='career'){
                                item = '                       <form method="post" class="editItemForm">\n' +
                                    '                            @csrf'+
                                    '                           <input type="hidden" value="'+response.data.id+'" name="id">'+
                                    '                            <div class="col-md-12">\n' +
                                    '                                <div class="row">\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Company</label>\n' +
                                    '                                        <input type="text" name="employer" class="form-control" value="'+response.data.employer+'">\n' +
                                    '                                    </div>\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Position</label>\n' +
                                    '                                        <input type="text" name="jobTitle" class="form-control" value="'+response.data.jobTitle+'">\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                                <div class="row">\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Month Joined</label>\n' +
                                    '                                        <input type="text" name="startMonth" class="form-control" value="'+response.data.startMonth+'">\n' +
                                    '                                    </div>\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Year Joined</label>\n' +
                                    '                                        <input type="text" name="startYear" class="form-control" value="'+response.data.startYear+'">\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                                <div class="row">\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Month Ended</label>\n' +
                                    '                                        <input type="text" name="endMonth" class="form-control" value="'+response.data.endMonth+'">\n' +
                                    '                                    </div>\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Year Ended</label>\n' +
                                    '                                        <input type="text" name="endYear" class="form-control" value="'+response.data.endYear+'">\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                                <br/>\n' +
                                    '                               <input type="submit" class="form-control" value="Update">'+
                                    '                                <br/>\n' +
                                    '                            </div>'+
                                    '                       </form>';
                            } else {

                                item = '                       <form method="post" class="editItemForm">\n' +
                                    '                            @csrf'+
                                    '                           <input type="hidden" value="'+response.data.id+'" name="id">'+
                                    '                            \n' +
                                    '                            <div class="col-md-12">\n' +
                                    '                                <div class="row">\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>School</label>\n' +
                                    '                                        <input type="text" name="school" class="form-control" value="'+response.data.school+'">\n' +
                                    '                                    </div>\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Degree</label>\n' +
                                    '                                        <input type="text" name="degree" class="form-control" value="'+response.data.degree+'">\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                                <div class="row">\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Year Joined</label>\n' +
                                    '                                        <input type="text" name="yearJoin" class="form-control" value="'+response.data.yearJoin+'">\n' +
                                    '                                    </div>\n' +
                                    '                                    <div class="col-md-6">\n' +
                                    '                                        <label>Year Graduated</label>\n' +
                                    '                                        <input type="text" name="classYear" class="form-control" value="'+response.data.classYear+'">\n' +
                                    '                                    </div>\n' +
                                    '                                </div>\n' +
                                    '                                <br/>\n' +
                                    '                           <input type="submit" class="form-control" value="Update">'+
                                    '                                <br/>\n' +
                                    '                            </div>'+
                                    '                       </form>';
                            }

                            wrap = clickedElement.closest('.edit-wrap');
                            wrap.find('.hideOnEdit').hide();
                            wrap.find('.showOnEdit').append(item);
                            wrap.find('.showOnEdit').show();
                        }
                    },
                    statusCode: {
                        401: function() {
                            window.location.href = '{{url('login')}}'; //or what ever is your login URI
                        }
                    },
                    complete : function (event,error){
                        refreshMacy();
                        editItemFormSubmit(type);
                    }
                });
            });
        }
        addEditItemClick();

        function editItemFormSubmit(type){
            $('.editItemForm').unbind('submit');
            $('.editItemForm').on('submit', function(e){
                var form = $(this);

                e.preventDefault();
                e.stopPropagation();

                data = form.serialize();

                console.log(data);
                $.ajax({
                    type: "POST",
                    url: baseUrl+'/my-profile/update-'+type,
                    data: data,
                    success: function(response){

                        if(response.data){

                            d = response.data;
                            target = $('#'+d.type+'-'+d.id);

                            if(type=='career') {
                                target.find('p[field="jobTitle"]').html(response.data.jobTitle);
                                target.find('p[field="employer"]').html(response.data.employer);
                                target.find('p[field="duration"]').html(response.data.startYear + ' - ' + response.data.endYear);
                            }
                            else {
                                target.find('p[field="school"]').html(response.data.school);
                                target.find('p[field="degree"]').html(response.data.degree);
                                target.find('p[field="duration"]').html(response.data.yearJoin + ' - ' + response.data.classYear);
                            }
                                target.find('.hideOnEdit').show();
                                form.remove();
                                target.find('.showOnEdit').hide();
                        }
                    },
                    statusCode: {
                        401: function() {
                            window.location.href = '{{url('login')}}'; //or what ever is your login URI
                        }
                    },
                    complete : function (event,error){
                        refreshMacy();
                    }
                });
            });
        }

        editItemFormSubmit('career');
        editItemFormSubmit('education');

        var uploadField = document.getElementById("fileUpload");

        uploadField.onchange = function() {
            if(this.files[0].size > 2097152){
                alert("File is too big!");
                this.value = "";
            };
        };


        currentCity = '{{$data->city}}';

        $('#childSelect').on('change',function (){
            val = $(this).val();

            $.ajax({
                type: "GET",
                url: baseUrl+"/my-profile/update-child/"+val,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response){
                    console.log(response);
                    $('#city').find('option').remove();


                    Object.keys(response).map(function(objectKey, index) {
                        var value = response[objectKey];

                        if(currentCity == value.name)
                            $('#city').append('<option selected value="'+value.name+'">'+value.name+'</option>');
                        else
                            $('#city').append('<option value="'+value.name+'">'+value.name+'</option>');
                    });
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('#country').on('change',function (){
            id = $(this).find('option:selected').attr('data-id');

            $.ajax({
                type: "GET",
                url: baseUrl+"/get-cities-for-country/"+id,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response){
                    console.log(response);
                    $('#city').find('option').remove();


                    Object.keys(response).map(function(objectKey, index) {
                        var value = response[objectKey];

                        if(currentCity == value.name)
                            $('#city').append('<option selected value="'+value.name+'">'+value.name+'</option>');
                        else
                            $('#city').append('<option value="'+value.name+'">'+value.name+'</option>');
                    });
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('#country').trigger('change');


        var $modal = $('#modal');
        var image = document.getElementById('image');
        var cropper;

        $("body").on("change", "#profile-photo", function(e){
            var files = e.target.files;
            var done = function (url) {
                image.src = url;
                $modal.modal('show');
            };
            var reader;
            var file;
            var url;

            if (files && files.length > 0) {
                file = files[0];

                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function (e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        });

        $modal.on('shown.bs.modal', function () {
            cropper = new Cropper(image, {
                aspectRatio: 1,
                viewMode: 3,
                preview: '.preview'
            });
        }).on('hidden.bs.modal', function () {
            cropper.destroy();
            cropper = null;
        });

        $("#crop").click(function(){
            canvas = cropper.getCroppedCanvas({
                width: 600,
                height: 600,
            });

            canvas.toBlob(function(blob) {
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function() {
                    var base64data = reader.result;

                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "my-profile/update-photo",
                        data: {'_token': '{{ @csrf_token() }}', 'image': base64data },
                        success: function(response){
                            $modal.modal('hide');

                            $('.user-photo').each(function(){
                                $(this).attr('src',response.data);
                            });

                            $(".ajxFormPhoto").first().closest('.edit-wrap').find('.hideOnEdit').show();
                            $(".ajxFormPhoto").first().closest('.edit-wrap').find('.showOnEdit').hide();
                        }
                    });
                }
            });
        })

    </script>
@endsection


