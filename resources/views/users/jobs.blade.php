@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/jobs.css">
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">

    <style>
        .swiper-container {
            width: 100%;
            height: auto;
        }
        .item {
            float: left;
        }
    </style>
@endsection

@section('content')

    <section id="content">
        <div class="container">
            <div class="row other-news">
                <h1>My Jobs</h1>
                <hr/>
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ url('create-job') }}">+ Create a Job Opportunity</a>
                    </div>
                </div>
                {{--<div class="row">--}}
                    {{--<h3>Applied Jobs</h3>--}}
                    {{--@foreach($events as $item)--}}
                        {{--<a href="{{ url('article/'.$item->slug) }}">--}}
                            {{--<div class="col-md-4 new-item">--}}
                                {{--<div class="wrap" style="background-image: url({{ $item->thumbnailUrl }})">--}}
                                    {{--<div class="overlay"></div>--}}
                                    {{--<div class="vcenter">--}}
                                        {{--<h3 class="sub">{{ $item->title }}</h3>--}}
                                        {{--<h4 class="sub">{{ $item->event_type }}</h4>--}}
                                        {{--<p class="sub">{{ \Carbon\Carbon::parse($item->date)->format('d M Y') }}</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    {{--@endforeach--}}
                {{--</div>--}}
                <hr/>
                <div class="row">
                    <div class="col-md-12">
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Sesssion::get('success') }}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-success">
                                {{ Sesssion::get('error') }}
                            </div>
                        @endif
                        <h3>Posted Jobs</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">

                        @if(count($posted))
                            @foreach($posted as $job)
                                <div class="item">
                                    <span class="name">{{ $job->title }}</span>
                                    <span class="school">{{ $job->company }}</span>
                                    <p class="desc">{{ \Illuminate\Support\Str::words(strip_tags($job->details),20,'...') }}</p>

                                    <div class="bottom">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span class="location">{{ $job->location }}</span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="{{ url('see-applicants/'.$job->id) }}" class="bold"><strong>SEE APPLICANTS</strong></a> |
                                                <a href="{{ url('edit-job/'.$job->id) }}" class=" font-weight-bold"><strong>EDIT</strong></a> |
                                                <a target="_blank" href="{{ url('jobs/'.$job->id) }}" class=" font-weight-bold"><strong>VIEW</strong></a> |
                                                <a href="{{ url('delete-job/'.$job->id) }}" class=" font-weight-bold"><strong>DELETE</strong></a>
                                            </div>
                                        </div>

                                        @if(Carbon\Carbon::parse($job->created_at)->diffInDays(Carbon\Carbon::now())<=30)
                                            <div class="row">
                                                <br/>
                                                <div class="col-md-12">Posted On: {{ $job->created_at->format('d-m-Y') }}</div>
                                                <div class="col-md-12">Expiring On: {{ \Carbon\Carbon::parse($job->created_at)->addMonth()->format('d-m-Y') }}</div>
                                                <div class="col-md-12">&nbsp;</div>
                                            </div>
                                        @else
                                            <div class="row">
                                                <br/>
                                                <div class="col-md-12">Posted On: {{ $job->created_at->format('d-m-Y') }}</div>
                                                <div class="col-md-12" style="color:red;">Expired On: {{ \Carbon\Carbon::parse($job->created_at)->addMonth()->format('d-m-Y') }}</div>
                                                <div class="col-md-12"><a href="{{ url('my-jobs/renew/'.$job->id) }}">Click here to renew</a></div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <br/>
                            <p>You haven't posted any jobs.</p>
                            <br/>
                            <br/>
                        @endif

                    </div>
                </div>
                <hr/>
                <h3>Applied Jobs</h3>
                @if(count($applied))
                    @foreach($applied as $job)
                        <div class="item">
                            <span class="name">{{ $job->title }}</span>
                            <span class="school">{{ $job->company }}</span>
                            <p class="desc">{{ \Illuminate\Support\Str::words(strip_tags($job->details),20,'...') }}</p>
                            <a href="{{ url('jobs/'.$job->id) }}" class="readmore">Read More</a>
                            <span class="location">{{ $job->location }}</span>

                            <div class="bottom">
                                <div class="row">
                                    <div class="col-md-8 text-right"><img src="{{ $job->photo }}" class="poster-img"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">5 days ago</div>
                                    <div class="col-md-8 text-right">Posted by: {{ $job->posterName }}</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <br/>
                                        <a href="{{ url('jobs/'.$job->id) }}" class="apply">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <br/>
                    <p>You haven't applied to any job posts.</p>
                    <br/>
                    <br/>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            loop: false,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        });

        function vCenter(){
            $('.vcenter').each(function(){
                ht = $(this).height()/2;
                $(this).css('margin-top','-'+ht+'px');
            });
        }
        vCenter();
    </script>
@endsection
