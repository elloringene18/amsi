@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/news.css">
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">

    <style>
        .swiper-container {
            width: 100%;
            height: auto;
        }
    </style>
@endsection

@section('content')

    <section id="content">
        <div class="container">
            <div class="row other-news">
                <h1>My Events</h1>
                <hr/>
                <div class="row">
                    @foreach($events as $item)
                        <a href="{{ url('article/'.$item->slug) }}">
                            <div class="col-md-4 new-item">
                                <div class="wrap" style="background-image: url({{ $item->thumbnailUrl }})">
                                    <div class="overlay"></div>
                                    <div class="vcenter">
                                        <h3 class="sub">{{ $item->title }}</h3>
                                        <h4 class="sub">{{ $item->event_type }}</h4>
                                        <p class="sub">{{ \Carbon\Carbon::parse($item->date)->format('d M Y') }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            loop: false,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        });

        function vCenter(){
            $('.vcenter').each(function(){
                ht = $(this).height()/2;
                $(this).css('margin-top','-'+ht+'px');
            });
        }
        vCenter();
    </script>
@endsection
