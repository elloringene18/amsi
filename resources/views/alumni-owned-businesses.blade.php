@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/news.css">
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">

    <style>
        .swiper-container {
            width: 100%;
            height: auto;
        }

        .tab {
            display: none;
        }

        .tab.active {
            display: block;
        }

        .tab-switch.active {
            color: #fff !important;
            cursor: not-allowed;
        }

        .tab-switch {
            text-transform: uppercase;
            text-decoration: none !important;
            font-size: 12px;
            font-weight: bold;
        }
    </style>
@endsection

@section('content')

    <section id="content">
        <div class="container">
            <div class="other-news">
                <h1 class="heading">Alumni Owned Businesses</h1>
                <hr/>
                <div class="row tab active" id="businesses">
                    @if(count($businesses))
                        @foreach($businesses as $bs)
                            <a href="{{ url('article/'.$bs->slug) }}">
                                <div class="col-md-4 new-item">
                                    <div class="wrap" style="background-image: url({{ $bs->thumbnailUrl }})">
                                    </div>
                                    <h4 class="sub">{{ $bs->event_type }}</h4>
                                    <h3 class="sub">{{ $bs->title }}</h3>
                                    <p class="sub">{{ \Carbon\Carbon::parse($bs->date)->format('d M Y') }}</p>
                                </div>
                            </a>
                        @endforeach
                    @else
                        <div class="col-md-12">Nothing listed. Please come back later.</div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            loop: false,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            // And if we need scrollbar
            scrollbar: {
                el: '.swiper-scrollbar',
            },
        });

        function vCenter(){
            $('.vcenter').each(function(){
                ht = $(this).height()/2;
                $(this).css('margin-top','-'+ht+'px');
            });
        }
        vCenter();

        $('.tab-switch').on('click',function(e){
            e.preventDefault();

            $('.tab-switch').removeClass('active');
            $('.tab').removeClass('active');
            $(this).addClass('active');

            if($(this).attr('data-type')=="news")
                $('#news').addClass('active');
            else if($(this).attr('data-type')=="events")
                $('#events').addClass('active');
            else if($(this).attr('data-type')=="businesses")
                $('#businesses').addClass('active');

            vCenter();
        });
    </script>
@endsection
