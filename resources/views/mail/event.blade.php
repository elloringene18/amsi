<h3>You have successfully registered to the below event from AMSI website:</h3>

<h4>Title: {{$data['title']}}</h4>
<h4>Date: {{$data['date']}}</h4>
<h4>Time: {{$data['time']}}</h4>
{!! $data['content'] !!}

<p>Thank you for your interest.</p>
