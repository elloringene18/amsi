<h3>There has been a new registration for AMSI Alumni Website</h3>

<h4>First Name: {{$data['firstName']}}</h4>
<h4>Last Name: {{$data['lastName']}}</h4>
<h4>Email: {{$data['email']}}</h4>
<h4>Mobile Number: {!! $data['mobileNumber'] !!}</h4>
<h4>Campus: {!! $data['campus'] !!}</h4>
<h4>Year Graduated: {!! $data['year_graduated'] !!}</h4>
<h4>Current Current: {!! $data['country'] !!}</h4>
<h4>Current City: {!! $data['city'] !!}</h4>
</br>
<p>Please login to the admin panel to verify this user.</p>
