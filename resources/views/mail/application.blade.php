<h3>You have successfully applied to the below job from AMSI website:</h3>

<h4>Title: {{$data['title']}}</h4>
<h4>Company: {{$data['company']}}</h4>
<h4>Employment Type: {{$data['employment_type']}}</h4>
<h4>Experience: {{$data['experience']}}</h4>
<h4>Career Level: {{$data['career_level']}}</h4>
<h4>Details:</h4>
{!! $data['details'] !!}
<br/>
