<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home',function(){
    return redirect('/');
});
Route::get('/','DirectoryController@home');
Route::post('/user-login','LoginController@authenticate');
Route::post('/user-login-main','LoginController@authenticateMain');

Route::get('auth/linkedin', 'LinkedinController@linkedinRedirect');
Route::get('auth/linkedin/callback', 'LinkedinController@linkedinCallback');

Route::middleware(['auth'])->group(function () {

    Route::get('/directory','DirectoryController@alumnis');
    Route::get('/directory/{id}','DirectoryController@alumni');

    Route::get('/my-profile','UserController@profile');
    Route::post('/my-profile','UserController@update');
    Route::post('/my-profile/update-photo','UserController@updatePhoto');
    Route::post('/my-profile/update-cover','UserController@updateCover');
    Route::post('/my-profile/update-resume','UserController@updateResume');
    Route::post('/my-profile/update-fields','UserController@updateFields');
    Route::post('/my-profile/update-location','UserController@updateLocation');

    Route::post('/my-profile/add-career','CareerController@add');
    Route::post('/my-profile/delete-career','CareerController@delete');
    Route::get('/my-profile/get-career/{id}','CareerController@get');
    Route::post('/my-profile/update-career','CareerController@update');

    Route::post('/my-profile/add-education','EducationController@add');
    Route::post('/my-profile/delete-education','EducationController@delete');
    Route::get('/my-profile/get-education/{id}','EducationController@get');
    Route::post('/my-profile/update-education','EducationController@update');

    Route::post('/my-profile/add-portfolio','PortfolioController@add');
    Route::post('/my-profile/delete-portfolio','PortfolioController@delete');
    Route::get('/my-profile/get-portfolio/{id}','PortfolioController@get');
    Route::post('/my-profile/update-portfolio','PortfolioController@update');

    Route::post('/my-profile/add-skills','UserController@addSkill');
    Route::post('/my-profile/delete-tag-skill','UserController@deleteSkill');
    Route::post('/my-profile/add-industry','UserController@addIndustry');
    Route::post('/my-profile/delete-tag-industry','UserController@deleteIndustry');

    Route::get('/my-profile/update-child/{value}','UserController@updateChild');

    Route::get('/add-event/{id}','UserController@addEvent');
    Route::get('/delete-event/{id}','UserController@deleteEvent');

    Route::get('/my-events','UserController@myEvents');
    Route::get('/my-jobs','UserController@myJobs');
    Route::get('/my-jobs/renew/{id}','UserController@myJobsRenew');

    Route::get('/create-job','UserJobController@create');
    Route::get('/edit-job/{id}','UserJobController@edit');
    Route::get('/delete-job/{id}','UserJobController@delete');
    Route::post('/store-job','UserJobController@store');
    Route::post('/update-job','UserJobController@update');
    Route::get('/see-applicants/{id}','UserJobController@applicants');
    Route::get('/apply-job/{id}','UserJobController@apply');
    Route::get('/cancel-apply-job/{id}','UserJobController@cancel');

    Route::get('/news-and-events','ArticleController@index');
    Route::get('/alumni-owned-businesses','ArticleController@businesses');
    Route::get('/article/{slug}','ArticleController@showNews');
    Route::get('/event/{slug}','ArticleController@showEvent');
    Route::post('/involvements/update','UserController@updateInvolvement');

    Route::get('/jobs','JobsController@index');
    Route::get('/jobs/{id}','JobsController@single');
    Route::get('/change-password',function(){ return view('change-password'); });
    Route::post('/change-password','UserController@updatePassword');
});

Route::get('admin/login', function () {
    return view('auth.login');
});

Route::group(['prefix' => 'admin','middleware' => 'admin'], function() {

    Route::get('/',function(){
        return redirect('admin/alumnis');
    });

    Route::group(['prefix' => 'alumnis','middleware' => 'auth'], function() {
        Route::get('/','AlumniController@index');
        Route::get('/unverified','AlumniController@unverified');
        Route::get('/new','AlumniController@newUsers');
        Route::get('/new-export','AlumniController@newUsersExport');
        Route::get('/create','AlumniController@create');
        Route::post('/store','AlumniController@store');
        Route::post('/update','AlumniController@update');
        Route::get('/delete/{id}','AlumniController@delete');
        Route::get('/deactivate/{id}','AlumniController@deactivate');
        Route::get('/activate/{id}','AlumniController@activate');
        Route::get('/verify/{id}','AlumniController@verify');
        Route::get('/{id}','AlumniController@edit');
    });

    Route::group(['prefix' => 'home-sliders','middleware' => 'auth'], function() {
        Route::get('/','HomesliderController@index');
        Route::get('/create','HomesliderController@create');
        Route::post('/store','HomesliderController@store');
        Route::post('/update','HomesliderController@update');
        Route::get('/delete/{id}','HomesliderController@delete');
        Route::get('/{id}','HomesliderController@edit');
    });

    Route::group(['prefix' => 'events','middleware' => 'auth'], function() {
        Route::get('/','EventController@index');
        Route::post('/store','EventController@store');
        Route::get('/create','EventController@create');
        Route::post('/update','EventController@update');
        Route::get('/month','EventController@month');
        Route::get('/attendees/{id}','EventController@attendees');
        Route::get('/attendees/delete/{id}/{user_id}','EventController@deleteAttendee');
        Route::get('/attendees/export/{id}','EventController@exportAttendees');
        Route::get('/delete/{id}','EventController@delete');
        Route::get('/{id}','EventController@edit');
    });

    Route::group(['prefix' => 'news','middleware' => 'auth'], function() {
        Route::get('/','NewsController@index');
        Route::post('/store','NewsController@store');
        Route::get('/create','NewsController@create');
        Route::post('/update','NewsController@update');
        Route::get('/delete/{id}','NewsController@delete');
        Route::get('/{id}','NewsController@edit');
    });

    Route::group(['prefix' => 'business','middleware' => 'auth'], function() {
        Route::get('/','BusinessController@index');
        Route::post('/store','BusinessController@store');
        Route::get('/create','BusinessController@create');
        Route::post('/update','BusinessController@update');
        Route::get('/delete/{id}','BusinessController@delete');
        Route::get('/{id}','BusinessController@edit');
    });

    Route::group(['prefix' => 'volunteers','middleware' => 'auth'], function() {
        Route::get('/','VolunteerController@index');
        Route::post('/store','VolunteerController@store');
        Route::get('/create','VolunteerController@create');
        Route::post('/update','VolunteerController@update');
        Route::get('/delete/{id}','VolunteerController@delete');
        Route::get('/{id}','VolunteerController@edit');
    });

    Route::group(['prefix' => 'jobs','middleware' => 'auth'], function() {
        Route::get('/','JobsController@adminIndex');
        Route::get('/email','JobsController@email');
        Route::post('/email/update','JobsController@emailUpdate');
        Route::post('/store','JobsController@store');
        Route::get('/create','JobsController@create');
        Route::post('/update','JobsController@update');
        Route::get('/delete/{id}','JobsController@delete');
        Route::get('/{id}','JobsController@edit');
    });

    Route::group(['prefix' => 'contacts','middleware' => 'auth'], function() {
        Route::get('/','ContactController@index');
        Route::get('/export','ContactController@export');
        Route::get('/view/{id}','ContactController@view');
        Route::get('/delete/{id}','ContactController@delete');
    });

    Route::group(['prefix' => 'settings','middleware' => 'auth'], function() {
        Route::group(['prefix' => 'home-items','middleware' => 'auth'], function() {
            Route::get('/','Admin\HomeItemController@index');
            Route::post('/store','Admin\HomeItemController@store');
            Route::get('/create','Admin\HomeItemController@create');
            Route::post('/update','Admin\HomeItemController@update');
            Route::get('/{id}','Admin\HomeItemController@edit');
            Route::get('/delete/{id}','Admin\HomeItemController@delete');
        });
    });

    Route::group(['prefix' => 'amsi-voices','middleware' => 'auth'], function() {
        Route::group(['prefix' => 'current','middleware' => 'auth'], function() {
            Route::get('/','AmsiVoiceController@current');
            Route::post('/store','AmsiVoiceController@storeCurrent');
            Route::get('/create','AmsiVoiceController@createCurrent');
            Route::post('/update','AmsiVoiceController@updateCurrent');
            Route::get('/{id}','AmsiVoiceController@editCurrent');
            Route::get('/delete/{id}','AmsiVoiceController@deleteCurrent');
        });
        Route::group(['prefix' => 'past','middleware' => 'auth'], function() {
            Route::get('/','AmsiVoiceController@past');
            Route::post('/store','AmsiVoiceController@storePast');
            Route::get('/create','AmsiVoiceController@createPast');
            Route::post('/update','AmsiVoiceController@updatePast');
            Route::get('/{id}','AmsiVoiceController@editPast');
            Route::get('/delete/{id}','AmsiVoiceController@deletePast');
        });
    });

    Route::group(['prefix' => 'page-contents','middleware' => 'auth'], function() {
        Route::post('/update','PageContentController@update');
        Route::get('/{page}','PageContentController@edit');
    });

});

Route::get('/logout',function(){
    \Illuminate\Support\Facades\Auth::logout();
    return redirect('/');
});

Route::post('/register-user','UserController@register')->name('register');
Route::get('/get-locations-for-map','DirectoryController@getLocationsForMap');
Route::get('/get-locations-for-map-job','JobsController@getLocationsForMap');
Route::get('/get-cities-for-country/{id}','DirectoryController@getCitiesForCountry');
Route::get('/get-cities-for-country-name/{name}','DirectoryController@getCitiesForCountryByName');
Route::post('/contact','FormController@contact');
Route::get('/contact-us','FormController@contactPage');
Route::get('/amsi-voices','AmsiVoiceController@index');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/checkphpinfo1111',function(){
    return phpinfo();
});

Route::get('/{page}',function($page){
    return view($page);
});
