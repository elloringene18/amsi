<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'prefix',
        'firstName',
        'lastName',
        'gender',
        'city',
        'country',
        'birthDate',
        'mobileNumber',
        'campus',
        'year_graduated',
        'photo',
        'resume',
        'occupation',
        'font_color',
        'bio',
        'cover',
        'is_active',
        'verified',
        'hide_mobile',
        'hide_email',
        'oauth_id',
        'oauth_type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $dates = ['birthDate'];

    public function career(){
        return $this->hasMany('App\Models\Career');
    }

    public function education(){
        return $this->hasMany('App\Models\Education');
    }

    public function portfolio(){
        return $this->hasMany('App\Models\Portfolio');
    }

    public function children(){
        return $this->hasMany('App\Models\UserChild');
    }

    public function skills(){
        return $this->belongsToMany('App\Models\Skill','user_skills','user_id','skill_id');
    }

    public function involvements(){
        return $this->belongsToMany('App\Models\Involve','user_involves','user_id','involve_id');
    }

    public function industries(){
        return $this->belongsToMany('App\Models\Industry','user_industries','user_id','industry_id');
    }

    public function events(){
        return $this->belongsToMany('App\Models\Event','user_events','user_id','event_id');
    }

    public function getEducationAttribute(){
        return $this->education()->get();
    }

    public function getInvolvementAttribute(){
        $data = $this->involvements()->get();
        $returns = [];
        foreach ($data as $item)
            $returns[] = $item->id;

        return $returns;
    }

    public function getHasChildAttribute(){
        return $this->children()->count() ? 1 : 0;
    }

    public function getPortfolioAttribute(){
        return $this->portfolio()->get();
    }

    public function getEventsAttribute(){
        return $this->events()->get();
    }

    public function getCareerAttribute(){
        return $this->career()->orderBy('startYear','DESC')->get();
    }

    public function getJobTitleAttribute(){
        if($this->occupation)
            return $this->occupation;

        if(count($this->career))
            return $this->career[0]->jobTitle;

        return 'N/A';
    }
//
//    public function getCampusAttribute(){
//        return $this->education()->first() ? $this->education()->first()->campus : null;
//    }

    public function getPhotoUrlAttribute(){
        if($this->photo){
            if(strpos($this->photo, 'https://media') !== false)
                return $this->photo;
            else
                return asset('public/'.$this->photo);
        }

        else
            return asset('public/img/placeholder-user.png');
    }
}
