<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','employer','jobTitle','startMonth','startYear','endMonth','endYear','description','is_current'];
}
