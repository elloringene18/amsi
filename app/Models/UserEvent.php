<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserEvent extends Model
{
    use HasFactory;

    protected $fillable = ['event_id','user_id'];

    public function event(){
        return $this->hasOne('App\Models\Event','id','event_id');
    }

    public function getEventAttribute(){
        return $this->event()->first();
    }

    public function user(){
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function getUserAttribute(){
        return $this->user()->first();
    }
}
