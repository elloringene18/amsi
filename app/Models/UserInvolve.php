<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserInvolve extends Model
{
    protected $fillable = ['user_id','involve_id'];
    use HasFactory;
}
