<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    use HasFactory;
    protected $fillable = ['title','slug','company','industry_id','employment_type','experience','career_level','details','country_id','city_id','user_id'];

    public function country(){
        return $this->hasOne('App\Models\Country','id','country_id');
    }

    public function city(){
        return $this->hasOne('App\Models\City','id','city_id');
    }

    public function getLocationAttribute(){
        $countryId = $this->country_id;
        $cityId = $this->city_id;

        $country = null;
        $city = null;

        if($countryId)
            $country = Country::find($countryId)->name;

        if($cityId)
            $city = City::find($cityId)->name;

        return ($city ? $city. ', ' : ''). $country;
    }

    public function getPosterAttribute(){
        return User::find($this->user_id);
    }

    public function getPosterNameAttribute(){
        $user = User::find($this->user_id);

        return $user->firstName. ', '. $user->lastName;
    }

    public function getIndustryNameAttribute(){
        $data = Industry::find($this->industry_id);

        return $data->name;
    }

}
