<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    use HasFactory;

    protected $fillable = ['title','file_name','original_name','mime_type','path'];

    public function getUrlAttribute(){
        return asset('public/'.$this->path.'/'.$this->file_name);
    }
}
