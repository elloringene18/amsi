<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserIndustry extends Model
{
    use HasFactory;

    protected $fillable = ['industry_id','user_id'];
}
