<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alumni extends Model
{
    use HasFactory;

    protected $fillable = ['prefix','firstName','lastName','emailAddress','gender','city','country','birthDate','mobileNumber'];

    public $dates = ['birthDate'];

    public function careers(){
        return $this->hasMany('App\Models\Career');
    }

    public function education(){
        return $this->hasMany('App\Models\Education');
    }

    public function getEducationAttribute(){
        return $this->education()->get();
    }

    public function getCareersAttribute(){
        return $this->careers();
    }
}
