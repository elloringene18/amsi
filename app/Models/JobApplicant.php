<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobApplicant extends Model
{
    use HasFactory;
    protected $fillable = ['job_id','user_id'];

    public function job(){
        return $this->hasOne('App\Models\Job','id','job_id');
    }

    public function getJobAttribute(){
        return $this->job()->first();
    }

    public function user(){
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function getUserAttribute(){
        return $this->user()->first();
    }
}
