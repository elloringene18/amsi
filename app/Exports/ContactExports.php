<?php
namespace App\Exports;

use App\Models\ContactEntry;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ContactExports implements FromView
{
    use Exportable;

    public function view(): View
    {
        $results = ContactEntry::with('items')->get();

        foreach ($results as $id=>$result){
            foreach ($result->items as $valId=>$item)
                $data[$id]['items'][$item->key] = $item->value;

            $data[$id]['id'] = $result->id;
            $data[$id]['date'] = $result->created_at;
        }

        return view('exports.contacts', [
            'data' => $data
        ]);
    }
}
