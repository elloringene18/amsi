<?php
namespace App\Exports;

use App\Models\Alumni;
use App\Models\Article;
use App\Models\Event;
use App\Models\User;
use App\Models\UserEvent;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class NewJoinerExports implements FromView
{

    public function __construct($month, $year)
    {
        $this->month = $month;
        $this->year = $year;
    }

    public function view(): View
    {
        $attendees = User::with('education','career','portfolio')->whereMonth('created_at',$this->month)->whereYear('created_at',$this->year)->get();

        return view('exports.new-joiners', [
            'attendees' => $attendees
        ]);
    }
}
