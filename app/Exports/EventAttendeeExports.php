<?php
namespace App\Exports;

use App\Models\Article;
use App\Models\Event;
use App\Models\UserEvent;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class EventAttendeeExports implements FromView
{

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function view(): View
    {
        $item = Event::find($this->id);
        $data = $item->attendees;

        return view('exports.attendees', [
            'attendees' => $data
        ]);
    }
}
