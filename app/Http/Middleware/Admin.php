<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Admin extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request,\Closure $next)
    {
        if (Auth::user()) {
            if (Auth::user()->is_admin==1) {
                return $next($request);
            }
        }

        return redirect()->back();
    }
}
