<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Exception;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;

class LinkedinController extends Controller
{
    public function linkedinRedirect()
    {
        return Socialite::driver('linkedin')->redirect();
    }


    public function linkedinCallback()
    {
        try {

            $user = Socialite::driver('linkedin')->user();

            $linkedinUser = User::where('email', $user->email)->first();
            $accountLinked = false;

            if($linkedinUser){

                if(!$linkedinUser->oauth_id){
                    $linkedinUser->oauth_id = $user->id;
                    $linkedinUser->oauth_type = 'linkedin';
                    $linkedinUser->save();
                    $accountLinked = true;
                }

                Auth::login($linkedinUser);

                return redirect('/')->with('accountLinked',$accountLinked);

            } else {
                $linkedinUser = $user;
                return redirect('/register')->with('linkedInUser',$linkedinUser);
            }

        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}
