<?php
namespace App\Http\Controllers;

use App\Exports\EventAttendeeExports;
use App\Models\Article;
use App\Models\Event;
use App\Models\UserEvent;
use App\Traits\CanCreateSlug;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class EventController extends Controller
{
    use CanCreateSlug;

    public function __construct(Event $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = Event::orderBy('id','DESC')->paginate(100);

        return view('admin.events.index',compact('data'));
    }

    public function edit($id){
        $item = Event::find($id);

        if($item){
            return view('admin.events.edit',compact('item'));
        }

        return 'ERROR';
    }

    public function attendees($id){
            $item = Event::find($id);

            if($item){
                $attendees = $item->attendees;
            return view('admin.events.attendees',compact('item','attendees'));
        }

        return 'ERROR';
    }

    public function exportAttendees($id)
    {
        $item = Event::find($id);
        return Excel::download(new EventAttendeeExports($id), $item->title.'-Attendees.xlsx');
    }

    public function deleteAttendee($id,$user_id){
        $item = UserEvent::where('user_id',$user_id)->where('event_id',$id)->first();

        if($item){
            $item->delete();
            Session::flash('success','Item successfully deleted.');
            return redirect()->back();
        }

        return redirect()->back();
    }

    public function create(){
        return view('admin.events.create');
    }

    public function store(Request $request){
        $input = $request->except('_token','photo');
        $photo = $request->file('photo');

        if($photo){
            $destinationPath = 'public/uploads/events';
            $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

            Image::make($photo->getRealPath())->fit(1600, 600)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/events/'. $newFileName;

            $destinationPath = 'public/uploads/events/thumbnails';
            $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

            Image::make($photo->getRealPath())->fit(400, 240)->save($destinationPath.'/'.$newFileName);
            $input['thumbnail'] = 'uploads/events/thumbnails/'. $newFileName;
        }

        $input['date'] = Carbon::parse($input['date']);
        $input['slug'] = $this->generateSlug($input['title']);
        Event::create($input);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }


    public function update(Request $request){

        $input = $request->except('_token','delete_photo','id');
        $delete_photo = $request->input('delete_photo');
        $photo = $request->file('photo');
        $target = $this->model->find($request->input('id'));

        if($target){
            if($photo){
                $destinationPath = 'public/uploads/events';
                $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

                Image::make($photo->getRealPath())->fit(1600, 600)->save($destinationPath.'/'.$newFileName);
                $input['photo'] = 'uploads/events/'. $newFileName;

                $destinationPath = 'public/uploads/events/thumbnails';
                $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

                Image::make($photo->getRealPath())->fit(400, 240)->save($destinationPath.'/'.$newFileName);
                $input['thumbnail'] = 'uploads/events/thumbnails/'. $newFileName;
            }

            if($delete_photo){
                $input['thumbnail'] = null;
                $input['photo'] = null;
            }

            $input['date'] = Carbon::parse($input['date']);

            if($target->title != $input['title'])
                $input['slug'] = $this->generateSlug($input['title']);

            $target->update($input);
        }

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

}
