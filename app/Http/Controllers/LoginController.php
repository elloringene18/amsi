<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller {

    public function authenticate(Request $request){
        // Retrive Input
        $credentials = $request->only('email', 'password');

        $user = User::where('email',$request->input('email'))->first();

        if($user)
            if(!$user->verified)
                return response()->json(['success'=>0,'message'=>'Your account has not been verified yet. Please come back later.']);

        if (Auth::attempt($credentials)) {
            // if success login

            return response()->json(['success'=>1,'message'=>'Your account has not been verified yet.']);

            //return redirect()->intended('/details');
        }
        // if failed login
        return response()->json(['success'=>0,'message'=>'The email and/or password is incorrect.']);
    }

    public function authenticateMain(Request $request){
        // Retrive Input
        $credentials = $request->only('email', 'password');

        $user = User::where('email',$request->input('email'))->first();

        if($user)
            if(!$user->verified){
                Session::flash('error','Your account has not been verified yet.');
                return redirect()->back();
            }

        if (Auth::attempt($credentials)) {
            // if success login

            return redirect('/');

            //return redirect()->intended('/details');
        }
        // if failed login

        Session::flash('error','The email and/or password is incorrect.');
        return redirect()->back();
    }

}
