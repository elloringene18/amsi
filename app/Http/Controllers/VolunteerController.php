<?php
namespace App\Http\Controllers;

use App\Exports\EventAttendeeExports;
use App\Models\Volunteer;
use App\Models\UserEvent;
use App\Traits\CanCreateSlug;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class VolunteerController extends Controller
{
    use CanCreateSlug;

    public function __construct(Volunteer $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = Volunteer::orderBy('order','ASC')->get();

        return view('admin.volunteers.index',compact('data'));
    }

    public function edit($id){
        $item = Volunteer::find($id);

        if($item){
            return view('admin.volunteers.edit',compact('item'));
        }

        return 'ERROR';
    }

    public function create(){
        return view('admin.volunteers.create');
    }

    public function store(Request $request){
        $input = $request->except('_token');
        Volunteer::create($input);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }


    public function update(Request $request){

        $input = $request->except('_token');
        $target = $this->model->find($request->input('id'));

        if($target){
            $target->update($input);
        }

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

}
