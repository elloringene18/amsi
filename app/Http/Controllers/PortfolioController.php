<?php

namespace App\Http\Controllers;

use App\Models\Portfolio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PortfolioController extends Controller
{
    public function __construct(Portfolio $model)
    {
        $this->model = $model;
    }

    public function add(Request $request){
        $user = Auth::user();
        $file = $request->file('file');

        //Move Uploaded File
        $destinationPath = 'public/uploads/files';
        $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();

        $upload['path'] = 'uploads/files';
        $upload['original_name'] = $file->getClientOriginalName();
        $upload['file_name'] = $newFileName;
        $upload['mime_type'] = $file->getClientOriginalExtension();
        $upload['mime_type'] = $file->getClientOriginalExtension();
        $upload['title'] = $request->input('title');

        $file->move($destinationPath,$newFileName);

        $newFile = $user->portfolio()->create($upload);

        $newFile->toArray();

        $newFile['file'] = $newFile->url;

        return response()->json(['success'=>1,'data'=> $newFile]);
    }

    public function delete(Request $request){
        $user = Auth::user();
        $target = $this->model->find($request->input('id'));

        if($target){
            if($target->user_id == $user->id){
                $id = $target->id;
                $target->delete();

                return response()->json(['success'=>1,'data'=> $id]);
            }
        }

        return response()->json(['success'=>0]);
    }

    public function get($id){
        $target = $this->model->find($id);

        if($target){
            if($target->user_id == Auth::user()->id){
                $data = $target->toArray();
                $data['type'] = 'portfolio';
                return response()->json(['success'=>1,'data'=> $data]);
            }
        }

        return response()->json(['success'=>0]);
    }

    public function update(Request $request){
        $data = $request->except('_token');

        $target = $this->model->find($data['id']);

        if($target){
            if($target->user_id == Auth::user()->id){
                $target->update($data);
                $data['type'] = 'portfolio';

                return response()->json(['success'=>1,'data'=> $data]);
            }
        }

        return response()->json(['success'=>0]);
    }
}
