<?php

namespace App\Http\Controllers;

use App\Exports\NewJoinerExports;
use App\Http\Controllers\Controller;
use App\Models\AmsiVoiceCurrent;
use App\Models\AmsiVoicePast;
use App\Models\Country;
use App\Models\Education;
use App\Models\Industry;
use App\Models\PageContent;
use App\Models\Skill;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class AmsiVoiceController extends Controller
{
    public function __construct()
    {
    }

    public function index(){
        $current = AmsiVoiceCurrent::get();
        $past = AmsiVoicePast::get();
        return view('amsi-voices', compact('current','past'));
    }

    public function current(){
        $data = AmsiVoiceCurrent::get();
        return view('admin.amsi-voices.current.index', compact('data'));
    }

    public function createCurrent(){
        return view('admin.amsi-voices.current.create');
    }

    public function storeCurrent(Request $request){
        $input = $request->except('_token');
        AmsiVoiceCurrent::create($input);

        return redirect('admin/amsi-voices/current');
    }

    public function editCurrent($id){
        $item = AmsiVoiceCurrent::find($id);

        return view('admin.amsi-voices.current.edit',compact('item'));
    }

    public function updateCurrent(Request $request){
        $id = $request->only('id');

        $target = AmsiVoiceCurrent::find($id)->first();

        if(!$target)
            dd('Post not found');

        $input = $request->except('_token','id');

        $target->update($input);

        return redirect('admin/amsi-voices/current');
    }

    public function deleteCurrent($id){

        $target = AmsiVoiceCurrent::find($id);

        if(!$target)
            dd('Post not found');

        $target->delete();

        return redirect('admin/amsi-voices/current');
    }

    public function past(){
        $data = AmsiVoicePast::get();
        return view('admin.amsi-voices.past.index', compact('data'));
    }

    public function createPast(){
        return view('admin.amsi-voices.past.create');
    }

    public function storePast(Request $request){
        $input = $request->except('_token');

        $file = $request->file('content');
        //Move Uploaded File
        $destinationPath = 'public/uploads/amsi-voices';
        $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();

        $upload['path'] = 'uploads/amsi-voices';
        $upload['original_name'] = $file->getClientOriginalName();
        $upload['file_name'] = $newFileName;
        $upload['mime_type'] = $file->getClientOriginalExtension();

        $file->move($destinationPath,$newFileName);

        $input['content'] = $upload['path'].'/'.$upload['file_name'];

        AmsiVoicePast::create($input);

        return redirect('admin/amsi-voices/past');
    }

    public function editPast($id){
        $item = AmsiVoicePast::find($id);

        return view('admin.amsi-voices.past.edit',compact('item'));
    }

    public function updatePast(Request $request){
        $id = $request->only('id');

        $target = AmsiVoicePast::find($id)->first();

        if(!$target)
            dd('Post not found');

        $input = $request->except('_token','id');

        $target->update($input);

        return redirect('admin/amsi-voices/past');
    }

    public function deletePast($id){

        $target = AmsiVoicePast::find($id);

        if(!$target)
            dd('Post not found');

        $target->delete();

        return redirect('admin/amsi-voices/past');
    }

    public function edit(){
        $data['amsi-voice-7th-conference'] = PageContent::where('page','amsi-voice-7th-conference')->first();
        $data['our-amsi-voices'] = PageContent::where('page','our-amsi-voices')->first();

        return view('admin.amsi-voices.current.edit', compact('data'));
    }

}
