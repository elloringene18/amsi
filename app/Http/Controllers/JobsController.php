<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use App\Models\Industry;
use App\Models\Job;
use App\Models\JobLocation;
use App\Models\PageContent;
use App\Models\User;
use App\Traits\CanCreateSlug;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Psy\Util\Str;
use Spatie\Geocoder\Geocoder;

class JobsController extends Controller
{
    use CanCreateSlug;

    public function __construct(Job $model)
    {
        return $this->model = $model;
    }

    public function index(){
        $data = $this->model->get();

        $perPage = 10;
        $totalData = 0;

        $city = isset($_GET['city']) ? $_GET['city'] : null;
        $country = isset($_GET['country']) ? $_GET['country'] : null;
        $career_level = isset($_GET['career_level']) ? $_GET['career_level'] : null;
        $employment_type = isset($_GET['employment_type']) ? $_GET['employment_type'] : null;
        $experience = isset($_GET['experience']) ? $_GET['experience'] : null;
        $country = isset($_GET['country']) ? $_GET['country'] : null;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        $data = Job::query();

        if($country)
            $data->whereHas('country',function ($query) use($country){
                return $query->where('name',$country);
            });

        if($city)
            $data->where('city_id', $city);

        if($career_level)
            $data->where('career_level', $career_level);

        if($employment_type)
            $data->where('employment_type', $employment_type);

        if($experience)
            $data->where('experience', $experience);

        $data->where('created_at',  '>', Carbon::now()->subDays(30)->format('Y-m-d'));

        $totalData = $data->count();

        $data = $data->limit($perPage)->skip($page == 1 ? 0 : ($page-1) * $perPage)->get();

        $countries = Country::select('name','id')->orderBy('name','ASC')->get()->toArray();

        return view('jobs', compact('data','countries','totalData','perPage','page'));
    }

    public function single($id){
        $job = Job::find($id);

        return view('job', compact('job'));
    }

    public function getLocationsForMap(){

        $mapData = JobLocation::select('location','lat','lng','total')->where('total','>',0)->get()->toArray();

        foreach($mapData as $index=>$location){
            $cut = explode(',', $location['location']);

            if(count($cut)==1){
                $mapData[$index]['city'] = null;
                $mapData[$index]['country'] = $cut[0];
            }
            if(count($cut)==2){
                $mapData[$index]['city'] = $cut[0];
                $mapData[$index]['country'] = substr($cut[1], 1);
            }
            if(count($cut)==3){
                $mapData[$index]['city'] = $cut[0];
                $mapData[$index]['country'] = substr($cut[2], 1);
            }
            if(count($cut)==4){
                $mapData[$index]['city'] = $cut[0];
                $mapData[$index]['country'] = substr($cut[3], 1);
            }
        }

        return response()->json($mapData);
    }

    public function adminIndex(){

        $data = $this->model->paginate(50);

        $perPage = 10;
        $totalData = 0;
//
//        $city = isset($_GET['city']) ? $_GET['city'] : null;
//        $country = isset($_GET['country']) ? $_GET['country'] : null;
//        $career_level = isset($_GET['career_level']) ? $_GET['career_level'] : null;
//        $employment_type = isset($_GET['employment_type']) ? $_GET['employment_type'] : null;
//        $experience = isset($_GET['experience']) ? $_GET['experience'] : null;
//        $country = isset($_GET['country']) ? $_GET['country'] : null;
//        $page = isset($_GET['page']) ? $_GET['page'] : 1;
//
//        $data = Job::query();
//
//        if($country)
//            $data->whereHas('country',function ($query) use($country){
//                return $query->where('name',$country);
//            });
//
//        if($city)
//            $data->where('city_id', $city);
//
//        if($career_level)
//            $data->where('career_level', $career_level);
//
//        if($employment_type)
//            $data->where('employment_type', $employment_type);
//
//        if($experience)
//            $data->where('experience', $experience);

//        $totalData = $data->count();

//        $data = $data->paginate(50);
//        $data = $data->limit($perPage)->skip($page == 1 ? 0 : ($page-1) * $perPage)->get();

        $countries = Country::select('name','id')->get()->toArray();

        return view('admin.jobs.index', compact('data'));
    }

    public function store(Request $request){
        $input = $request->except('_token');
        $input['slug'] = $this->generateSlug($input['title']);
        $input['user_id'] = Auth::user()->id;
        $input['details'] = $input['details'] ? $input['details'] : 'N/A';

        $job = $this->model->create($input);

        $country = Country::find($input['country_id']);
        $city = isset($input['city_id']) ? Country::find($input['city_id']) : null;

        $location = JobLocation::where('location',($city ? $city->name.', ' : '').$country->name)->first();

        if($location){
            $location->total = $location->total + 1;
            $location->save();
        } else {
            $client = new \GuzzleHttp\Client();
            $geocoder = new Geocoder($client);
            $geocoder->setApiKey(config('geocoder.key'));
            $geocoder->setCountry(config('geocoder.country', 'AE'));
            $newLocation = $geocoder->getCoordinatesForAddress(($city ? $city->name.', ' : '').$country->name);

            JobLocation::create([
                'location' => ($city ? $city->name.', ' : '').$country->name,
                'lat' => $newLocation['lat'],
                'lng' => $newLocation['lng'],
                'total' => 1,
            ]);
        }

        Session::flash('success','Item created successfully.');

        return redirect()->back();
    }

    public function update(Request $request){
        $input = $request->except('_token');
        $target = $this->model->find($request->input('id'));

        if($target->title != $input['title'])
            $input['slug'] = $this->generateSlug($input['title']);

        $input['details'] = $input['details'] ? $input['details'] : 'N/A';
        $target->update($input);

        Session::flash('success','Item updated successfully.');

        return redirect()->back();
    }

    public function create(){
        $employment_types = ['Full-time','Part-time','Contract','Internship'];
        $experience = ['Less than a year','1-2 years','3-5 year','5-7 years','8-10 years','More than 10 years'];
        $careers = ['Junior','Mid Level','Senior','Management'];
        $countries = Country::orderBy('name','ASC')->get();
        $industries = Industry::get();

        return view('admin.jobs.create',compact('employment_types','experience','careers','countries','industries'));
    }

    public function email(){
        $data = PageContent::where('page','jobs-email')->first();
        return view('admin.jobs.email',compact('data'));
    }

    public function emailUpdate(Request $request){
        $target = PageContent::where('page','jobs-email')->first();

        if(!$target)
            PageContent::create($request->except('_token'));
        else
            $target->update($request->except('_token'));

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function edit($id){

        $data = $this->model->find($id);

        $employment_types = ['Full-time','Part-time','Contract','Internship'];
        $experience = ['Less than a year','1-2 years','3-5 year','5-7 years','8-10 years','More than 10 years'];
        $careers = ['Junior','Mid Level','Senior','Management'];
        $countries = Country::orderBy('name','ASC')->get();
        $industries = Industry::get();

        return view('admin.jobs.edit',compact('data','employment_types','experience','careers','countries','industries'));
    }

    public function delete($id){
        $page = $this->model->find($id);

        $country = Country::find($page->country_id);
        $city = Country::find($page->city_id);

        $location = JobLocation::where('location',($city ? $city->name.', ' : '').$country->name)->first();

        if($location){
            $location->total = $location->total - 1;
            $location->save();
        }

        if($page)
            $page->delete();

        Session::flash('success','Item deleted successfully.');
        return redirect()->back();
    }
}
