<?php

namespace App\Http\Controllers;

use App\Models\ContactEntry;
use App\Models\Volunteer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class FormController extends Controller
{
    public function __construct()
    {
    }

    public function sendEmail($input){

        $emailData = [
            'name' => $input['name'],
            'email' => $input['email'],
            'message' => $input['message'],
            'subject' => $input['subject'],
        ];

        $subject = 'AMSI Website Inquiry';

        try {
            Mail::send('mail.inquiry', ['data' => $emailData], function ($message) use ($input, $subject) {
                $message->from('alumni@amsi.ae', 'AMSI Website Admin')->to('alumni@amsi.ae', 'AMSI Website Admin')->subject($subject);
            });
            return true;
        }
        catch (\Exception $e) {
            return false;

        }

    }

    public function contact(Request $request){
        $input = $request->except('_token');

        $entry = ContactEntry::create(['ip'=>$request->ip(),'source'=>'contact-us']);

        if($entry){

            foreach ($input as $key=>$item){
                $entry->items()->create(['key'=>$key,'value'=>$item]);
            }

            Session::flash('message','Thank you for your contacting us. We will get back to you soon.');

            $this->sendEmail($input);

            return redirect()->back();
        }


        Session::flash('error','An error has occurred. Please try again later.');

        return redirect()->back();
    }

    public function contactPage(){
        $volunteers = Volunteer::orderBy('order','ASC')->get();
        return view('contact-us',compact('volunteers'));
    }
}
