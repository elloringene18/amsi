<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Country;
use App\Models\Education;
use App\Models\Event;
use App\Models\Industry;
use App\Models\Job;
use App\Models\JobApplicant;
use App\Models\Location;
use App\Models\Skill;
use App\Models\User;
use App\Models\UserChild;
use App\Models\UserEvent;
use App\Models\UserIndustry;
use App\Models\UserInvolve;
use App\Models\UserSkill;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Spatie\Geocoder\Geocoder;

class UserController extends Controller
{
    public function register(Request $request){
        $input = $request->except('password_confirmation','_token','has_child','involvement');

        $user['firstName'] = $request->input('firstName');
        $user['lastName'] = $request->input('lastName');
        $user['email'] = $request->input('email');
        $user['occupation'] = $request->input('occupation');
        $user['prefix'] = $request->input('prefix');
        $user['prefix'] = $request->input('prefix');
        $user['gender'] = $request->input('gender');
        $user['country'] = $request->input('country');
        $user['city'] = $request->input('city');
        $user['campus'] = $request->input('campus');
        $user['year_graduated'] = $request->input('year_graduated');
        $user['mobileNumber'] = $request->input('mobileNumber');

        if($request->input('oauth_id')){
            $linkedinUser['oauth_id'] = $request->input('oauth_id');
            $linkedinUser['oauth_type'] = $request->input('oauth_type');
            $linkedinUser['photo'] = $request->input('photo');

            if(User::where('email',$request->input('email'))->count()){
                Session::flash('error','Email address is already registered.');
                return view('auth.register',compact('user','linkedinUser'));
            }

            if($request->input('password') != $request->input('password_confirmation')){
                Session::flash('error','Password did not match. Please try again.');
                return view('auth.register',compact('user','linkedinUser'));
            }
        } else {

            if(User::where('email',$request->input('email'))->count()){
                Session::flash('error','Email address is already registered.');
                return view('auth.register',compact('user'));
            }

            if($request->input('password') != $request->input('password_confirmation')){
                Session::flash('error','Password did not match. Please try again.');
                return view('auth.register',compact('user'));
            }
        }

        $input['password'] = Hash::make($input['password']);
        $input['verified'] = 0;
        $user = User::create($input);

        if($user){

            if($request->input('has_child')==1)
                UserChild::create(['user_id'=>$user->id]);

            if($request->input('involvement'))
                $user->involvements()->sync($request->input('involvement'));

            $subject = 'New AMSI Alumni Website User Registration';

            try {
                Mail::send('mail.registration', ['data' => $input], function ($message) use ($subject) {
                    $message->from('alumni@amsi.ae', 'AMSI Website Admin')->to('info@amsi.ae', 'AMSI Website Admin')->subject($subject);
                });
            }
            catch (\Exception $e) {
            }

            Session::flash('success','Your registration has been sent to the admins. We will let you know once it has been approved.');
            return redirect('register');
        }

        Session::flash('error','There was an error in the system. Please try again later.');
        return redirect('register');
    }

    public function update(Request $request){

        $user = Auth::user();

        $profile = $request->except('_token','education','career','photo');
        $image = $request->file('photo');

        if($image){
            $img = Image::make($image);
            $img->resize(400, 400);

            $destinationPath = 'public/uploads/users/';
            $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

            Image::make($image->getRealPath())->fit(400, 400)->save($destinationPath.'/'.$newFileName);
            $profile['photo'] = 'uploads/users/'. $newFileName;
        }

        $user->update($profile);

        $education = $request->input('education');
        $career = $request->input('career');

        $user->education()->delete();

        foreach($education as $ed)
            $user->education()->create($ed);


        $user->career()->delete();

        foreach($career as $car)
            $user->career()->create($car);

        $user->load('career','education');

        Session::flash('success','Your profile has been updated successfully.');
        return redirect()->back();
    }


    public function updatePhoto(Request $request){
        $user = Auth::user();

        $folderPath = public_path('uploads/users/');

        $image_parts = explode(";base64,", $request->image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);

        $newName = Str::random(30);

        $file = $folderPath . $newName . '.png';

        file_put_contents($file, $image_base64);

        $profile['photo'] = 'uploads/users/' . $newName . '.png';
        $user->photo = $profile['photo'];
        $user->save();

        return response()->json(['success'=>1,'data'=> asset('public/'.$profile['photo'])]);
    }

    public function updateInvolvement(Request $request){
        $user = Auth::user();

        $involve_id = $request->input('involve_id');
        $checked = $request->input('checked');

        if($checked=='true')
            UserInvolve::create(['user_id'=>$user->id,'involve_id'=>$involve_id]);
        else {
            $data = UserInvolve::where('user_id',$user->id)->where('involve_id',$involve_id)->get();
            foreach ($data as $it)
                $it->delete();
        }

        return response()->json(['success'=>1]);
    }

    public function updateFields(Request $request){
        $user = Auth::user();
        $data = $request->except('_token');

        $user->update($data);
        return response()->json(['success'=>1,'data'=> $data]);
    }

    public function updateLocation(Request $request){
        $user = Auth::user();
        $data = $request->except('_token');

        $currentLocation = Location::where('location',Auth::user()->city.', '.Auth::user()->country)->first();

        if($currentLocation){
            $currentLocation->total = $currentLocation->total - 1;
            $currentLocation->save();
        }

        $location = Location::where('location',$data['city'].', '.$data['country'])->first();

        if($location){
            $location->total = $location->total + 1;
            $location->save();

        } else {
            $client = new \GuzzleHttp\Client();
            $geocoder = new Geocoder($client);
            $geocoder->setApiKey(config('geocoder.key'));
            $geocoder->setCountry(config('geocoder.country', 'AE'));
            $newLocation = $geocoder->getCoordinatesForAddress($data['city'].', '.$data['country']);

            Location::create([
                'location' => $data['city'].', '.$data['country'],
                'lat' => $newLocation['lat'],
                'lng' => $newLocation['lng'],
                'total' => 1,
            ]);
        }

        $user->update($data);

        return response()->json(['success'=>1,'data'=> $data]);
    }

    public function updateCover(Request $request){
        $user = Auth::user();
        $image = $request->file('photo');

        if($image){
            $img = Image::make($image);
            $img->resize(1600, 400);

            $destinationPath = 'public/uploads/users/';
            $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

            Image::make($image->getRealPath())->fit(1600, 400)->save($destinationPath.'/'.$newFileName);
            $profile['cover'] = 'uploads/users/'. $newFileName;
            $user->cover = $profile['cover'];
            $user->save();

            return response()->json(['success'=>1,'data'=> asset('public/'.$profile['cover'])]);
        }

        return response()->json(['success'=>0]);
    }

    public function updateResume(Request $request){
        $user = Auth::user();
        $file = $request->file('resume');

        //Move Uploaded File
        $destinationPath = 'public/uploads/resumes';
        $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();

        $upload['path'] = 'uploads/resumes';
        $upload['original_name'] = $file->getClientOriginalName();
        $upload['file_name'] = $newFileName;
        $upload['mime_type'] = $file->getClientOriginalExtension();
        $upload['mime_type'] = $file->getClientOriginalExtension();

        $file->move($destinationPath,$newFileName);

        $user->update(['resume'=>$upload['path'].'/'.$upload['file_name']]);

        $newFile = asset('public/'.$user->resume);

        return response()->json(['success'=>1,'data'=> $newFile]);
    }

    public function profile(){
        $campuses = Education::distinct()->where('campus','!=',null)->get(['campus']);
        $industries = Industry::orderBy('name','ASC')->get();
        $skills = Skill::orderBy('name','ASC')->get();
        $countries = Country::orderBy('name','ASC')->get();

        return view('users.index',compact('campuses','skills','industries','countries'));
    }

    public function addSkill(Request $request){
        $skillId = $request->input('id');

        $skill = Skill::find($skillId);

        if($skill){
            UserSkill::create(['skill_id'=>$skill->id,'user_id'=>Auth::user()->id]);
            return response()->json(['success'=>1,'data'=>$skill]);
        }

        return response()->json(['success'=>0]);
    }

    public function addIndustry(Request $request){
        $industryId = $request->input('id');

        $industry = Industry::find($industryId);

        if($industry){
            UserIndustry::create(['industry_id'=>$industryId,'user_id'=>Auth::user()->id]);
            return response()->json(['success'=>1,'data'=>$industry]);
        }

        return response()->json(['success'=>0]);
    }

    public function updateChild($val){

        $user = Auth::user();

        if($val==1){
            if(!$user->hasChild) {
                UserChild::create(['user_id' => $user->id]);
            }
        } else {
            UserChild::where('user_id', $user->id)->first()->delete();
        }

        return response()->json(['success'=>1]);
    }

    public function addEvent($id){
        $target = Event::find($id);

        if($target){
            UserEvent::create(['event_id'=>$id,'user_id'=>Auth::user()->id]);

            $emailData = [
                'title' => $target->title,
                'location' => $target->location,
                'date' => Carbon::parse($target->date)->format('d M Y'),
                'time' => $target->time,
                'content' => $target->email_content,
                'user_name' => Auth::user()->firstName . ' ' .Auth::user()->lastName,
                'user_id' => Auth::user()->id,
                'user_email' => Auth::user()->firstName . ' ' .Auth::user()->lastName,
            ];

            $subject = 'AMSI Website Event Registration';

            try {
                Mail::send('mail.event', ['data' => $emailData], function ($message) use ($subject) {
                    $message->from('alumni@amsi.ae', 'AMSI Website Admin')->to(Auth::user()->email, Auth::user()->firstName)->subject($subject);
                });
            }
            catch (\Exception $e) {

            }

            $subject = 'AMSI Event - Alumni Registration';

            try {
                Mail::send('mail.event', ['data' => $emailData], function ($message) use ($subject) {
                    $message->from('alumni@amsi.ae', 'AMSI Website Admin')->to('info@amsi.ae', 'AMSI Website Admin')->subject($subject);
                });
            }
            catch (\Exception $e) {
            }

            Session::flash('success','You have successfully registered to this event.');
            return redirect()->back();
        }

        Session::flash('error','There was an error. Please try again');
        return redirect()->back();
    }

    public function deleteEvent($id){
        $target = UserEvent::where('event_id',$id)->where('user_id',Auth::user()->id)->first();

        if($target){
            $target->delete();
            Session::flash('success','You have successfully unregistered to this event.');
        }

        Session::flash('error','There was an error. Please try again');
        return redirect()->back();
    }

    public function myEvents(){
        $data = UserEvent::where('user_id',Auth::user()->id)->get();

        $events = [];

        foreach ($data as $row){
            $events[] = $row->event;
        }

        return view('users.events', compact('events'));
    }

    public function myJobs(){
        $posted = Job::where('user_id',Auth::user()->id)->get();


        $data = JobApplicant::where('user_id',Auth::user()->id)->get();
        $applied = [];

        foreach ($data as $item)
            $applied[] = $item->job;

        return view('users.jobs', compact('posted','applied'));
    }

    public function myJobsRenew($id){
        $posted = Job::where('user_id',Auth::user()->id)->where('id',$id)->first();

        if($posted){
            $posted->created_at = Carbon::now();
            $posted->save();

            Session::flash('success','Job posting renewed successfully.');
            return redirect()->back();
        }

        Session::flash('error','Invalid Job ID.');
        return redirect()->back();
    }

    public function deleteSkill(Request $request){
        $skillId = $request->input('id');

        $skill = UserSkill::where(['skill_id'=>$skillId,'user_id'=>Auth::user()->id]);

        if($skill){
            $skill->delete();
            return response()->json(['success'=>1,'data'=>$skillId]);
        }

        return response()->json(['success'=>0]);
    }

    public function deleteIndustry(Request $request){
        $skillId = $request->input('id');

        $skill = UserIndustry::where(['industry_id'=>$skillId,'user_id'=>Auth::user()->id]);

        if($skill){
            $skill->delete();
            return response()->json(['success'=>1,'data'=>$skillId]);
        }

        return response()->json(['success'=>0]);
    }

    public function updatePassword(Request $request){

        if($request->input('password') == $request->input('password-confirm')){
            Auth::user()->update(['password' => Hash::make($request->input('password'))]);
            Session::flash('success','You password has been updated successfully.');
        } else {
            Session::flash('error','Passwords did not match. Please try again.');
        }


        return redirect()->back();
    }
}
