<?php

namespace App\Http\Controllers;

use App\Exports\NewJoinerExports;
use App\Models\Country;
use App\Models\Education;
use App\Models\Industry;
use App\Models\Skill;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class AlumniController extends Controller
{
    public function __construct(User $model)
    {
        return $this->model = $model;
    }

    public function index(){
        $perPage = 100;
        $totalData = 0;

        $firstName = isset($_GET['first_name']) ? $_GET['first_name'] : null;
        $lastName = isset($_GET['last_name']) ? $_GET['last_name'] : null;
        $graduate_year = isset($_GET['graduate_year']) ? $_GET['graduate_year'] : null;
        $campus = isset($_GET['campus']) ? $_GET['campus'] : null;
        $country = isset($_GET['country']) ? $_GET['country'] : null;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        $data = User::query();

        $data->where('is_admin',0);

        if($firstName)
            $data->where('firstName', $firstName);

        if($lastName)
            $data->where('lastName',  $lastName);

        if($graduate_year)
            $data->whereHas('education',function($query) use($graduate_year){
                return $query->where('classYear', $graduate_year);
            });

        if($campus)
            $data->whereHas('education',function($query) use($campus){
                return $query->where('campus', $campus);
            });

        if($country)
            $data->where('country', $country);

        $totalData = $data->count();
        $data = $data->with('education','career','portfolio')->limit($perPage)->skip($page == 1 ? 0 : ($page-1) * $perPage)->get();

        $campuses = Education::whereNotNull('campus')->groupBy('campus')->pluck('campus')->toArray();
        $cities = Education::whereNotNull('campus')->groupBy('campus')->pluck('campus')->toArray();

        $countries = Country::orderBy('name','ASC')->pluck('name')->toArray();

        return view('admin.alumnis.index', compact('data','campuses','countries','totalData','perPage','page'));
    }

    public function unverified(){
        $data = $this->model->with('education','career','portfolio')->where('verified',0)->get();

        return view('admin.alumnis.unverified', compact('data'));
    }

    public function newUsers(){
        $month = isset($_GET['month']) ? $_GET['month'] : Carbon::now()->startOfMonth()->format('m');
        $year = isset($_GET['year']) ? $_GET['year'] : Carbon::now()->endOfMonth()->format('Y');

        $data = $this->model->with('education','career','portfolio')->whereMonth('created_at',$month)->whereYear('created_at',$year)->get();

        return view('admin.alumnis.new-users', compact('data','month','year'));
    }

    public function newUsersExport(){
        $month = isset($_GET['month']) ? $_GET['month'] : Carbon::now()->startOfMonth()->format('m');
        $year = isset($_GET['year']) ? $_GET['year'] : Carbon::now()->endOfMonth()->format('Y');
        return Excel::download(new NewJoinerExports($month,$year), 'AMSI-New-Members-'.$month.'-'.$year.'.xlsx');
    }

    public function create(){
        return view('admin.alumnis.create');
    }

    public function edit($id){
        $data = User::find($id);

        $campuses = Education::distinct()->where('campus','!=',null)->get(['campus']);
        $industries = Industry::get();
        $skills = Skill::get();

        return view('admin.alumnis.edit', compact('data','campuses','skills','industries'));
    }

    public function delete($id){
        $data = User::find($id);

        $data->delete();
        return redirect()->back();
    }

    public function verify($id){
        $data = User::find($id);

        $data->update(['verified'=>1]);

        $subject = 'Your AMSI Alumni Website registration has been verified.';

        try {
            Mail::send('mail.verified', [], function ($message) use ($subject,$data) {
                $message->from('alumni@amsi.ae', 'AMSI Website Admin')->to($data->email, $data->firstName)->subject($subject);
            });
        }
        catch (\Exception $e) {
        }

        return redirect()->back();
    }

    public function activate($id){
        $data = User::find($id);

        $data->update(['is_active'=>1]);

        return redirect()->back();
    }

    public function deactivate($id){
        $data = User::find($id);

        $data->update(['is_active'=>0]);

        return redirect()->back();
    }

}
