<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\PageContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class PageContentController extends Controller
{
    public function __construct(PageContent $model)
    {
        $this->model = $model;
    }

    public function edit($page){
        $data = PageContent::where('page',$page)->get();
        return view('admin.page-contents.index',compact('data'));
    }

    public function update(Request $request){

        $input = $request->except('_token');

        foreach($input['section'] as $id=>$item){
            $target = $this->model->find($id);
            foreach($item as $key=>$content){

                if($key=='image'){
                    if($content){
                        $destinationPath = 'public/uploads/banners';

                        $file = $content;
                        $newFileName = Str::random(32).'.'.$file->getClientOriginalExtension();

                        Image::make($file->getRealPath())->fit(1437,755)->save($destinationPath.'/'.$newFileName);

                        $target->update(['content' => 'uploads/banners/'. $newFileName]);
                    }
                } else {
                    $target->update($item);
                }
            }
        }

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }
}
