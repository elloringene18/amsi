<?php

namespace App\Http\Controllers;

use App\Models\Alumni;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function index(){
        $data = Alumni::with('education','careers')->limit(1)->get();

        return view('directory', compact('data'));
    }
}
