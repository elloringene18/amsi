<?php

namespace App\Http\Controllers;

use App\Models\Education;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EducationController extends Controller
{
    public function __construct(Education $model)
    {
        $this->model = $model;
    }

    public function add(Request $request){
        $user = Auth::user();
        $career = $request->input('education');

        foreach($career as $car)
            $user->education()->create($car);

        return response()->json(['success'=>1,'data'=> $user->education]);
    }

    public function delete(Request $request){
        $user = Auth::user();
        $target = $this->model->find($request->input('id'));

        if($target){
            if($target->user_id == $user->id){
                $id = $target->id;
                $target->delete();

                return response()->json(['success'=>1,'data'=> $id]);
            }
        }

        return response()->json(['success'=>0]);
    }

    public function get($id){
        $target = $this->model->find($id);

        if($target){
            if($target->user_id == Auth::user()->id){
                $data = $target->toArray();
                $data['type'] = 'education';
                return response()->json(['success'=>1,'data'=> $data]);
            }
        }

        return response()->json(['success'=>0]);
    }

    public function update(Request $request){
        $data = $request->except('_token');

        $target = $this->model->find($data['id']);

        if($target){
            if($target->user_id == Auth::user()->id){
                $target->update($data);
                $data['type'] = 'education';

                return response()->json(['success'=>1,'data'=> $data]);
            }
        }

        return response()->json(['success'=>0]);
    }
}
