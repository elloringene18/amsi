<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function __construct(Article $model)
    {
        return $this->model = $model;
    }

    public function index(){
        $upcomingevents = Event::orderBy('date','DESC')->whereDate('date','>=',Carbon::now())->get();
        $events = Event::orderBy('date','DESC')->whereDate('date','<',Carbon::now())->get();
        $news = $this->model->where('event_type','news')->orderBy('date','DESC')->get();

        return view('news-and-events', compact('events','news','upcomingevents'));
    }

    public function businesses(){
        $businesses = $this->model->where('event_type','business')->orderBy('date','DESC')->get();

        return view('alumni-owned-businesses', compact('businesses'));
    }

    public function showNews($slug){
        $article = $this->model->where('slug',$slug)->first();
        $similar = $this->model->where('event_type',$article->event_type)->where('slug','!=',$slug)->limit(3)->inRandomOrder()->get();

        return view('article', compact('article','similar'));
    }

    public function showEvent($slug){
        $article = Event::where('slug',$slug)->first();
        $similar = Event::limit(3)->where('slug','!=',$slug)->inRandomOrder()->get();

        return view('event', compact('article','similar'));
    }
}
