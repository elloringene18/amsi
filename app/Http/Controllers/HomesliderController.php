<?php
namespace App\Http\Controllers;

use App\Exports\EventAttendeeExports;
use App\Models\Article;
use App\Models\Event;
use App\Models\HomeSlider;
use App\Models\UserEvent;
use App\Traits\CanCreateSlug;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class HomesliderController extends Controller
{
    use CanCreateSlug;

    public function __construct(HomeSlider $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = HomeSlider::orderBy('id','DESC')->paginate(100);

        return view('admin.home-sliders.index',compact('data'));
    }

    public function edit($id){
        $item = HomeSlider::find($id);

        if($item){
            return view('admin.home-sliders.edit',compact('item'));
        }

        return 'ERROR';
    }

    public function create(){
        return view('admin.home-sliders.create');
    }

    public function store(Request $request){
        $input = $request->except('_token','image');
        $photo = $request->file('image');

        if($photo){
            $destinationPath = 'public/uploads/';
            $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

            Image::make($photo->getRealPath())->fit(1366, 638)->save($destinationPath.'/'.$newFileName);
            $input['image'] = 'uploads/'. $newFileName;
        }

        HomeSlider::create($input);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }


    public function update(Request $request){

        $input = $request->except('_token','id');
        $photo = $request->file('image');
        $target = $this->model->find($request->input('id'));

        if($target){
            if($photo){
                $destinationPath = 'public/uploads/';
                $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

                Image::make($photo->getRealPath())->fit(1366, 638)->save($destinationPath.'/'.$newFileName);
                $input['image'] = 'uploads/'. $newFileName;
            }

            $target->update($input);
        }

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

}
