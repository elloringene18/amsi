<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Industry;
use App\Models\Job;
use App\Models\JobApplicant;
use App\Models\JobLocation;
use App\Traits\CanCreateSlug;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Spatie\Geocoder\Geocoder;

class UserJobController extends Controller
{
    use CanCreateSlug;

    public function __construct(Job $job)
    {
        $this->model = $job;
    }

    public function store(Request $request){
        $input = $request->except('_token');
        $input['slug'] = $this->generateSlug($input['title']);
        $input['user_id'] = Auth::user()->id;

        $input['details'] = $input['details'] ? $input['details'] : 'N/A';

        $job = $this->model->create($input);

        $country = Country::find($input['country_id']);
        $city = isset($input['city_id']) ? Country::find($input['city_id']) : null;

        $location = JobLocation::where('location',($city ? $city->name.', ' : '').$country->name)->first();

        if($location){
            $location->total = $location->total + 1;
            $location->save();
        } else {
            $client = new \GuzzleHttp\Client();
            $geocoder = new Geocoder($client);
            $geocoder->setApiKey(config('geocoder.key'));
            $geocoder->setCountry(config('geocoder.country', 'AE'));
            $newLocation = $geocoder->getCoordinatesForAddress(($city ? $city->name.', ' : '').$country->name);

            JobLocation::create([
                'location' => ($city ? $city->name.', ' : '').$country->name,
                'lat' => $newLocation['lat'],
                'lng' => $newLocation['lng'],
                'total' => 1,
            ]);
        }

        Session::flash('success','Item created successfully.');

        return redirect()->back();
    }

    public function update(Request $request){
        $input = $request->except('_token');
        $target = $this->model->find($request->input('id'));

        if($target->title != $input['title'])
            $input['slug'] = $this->generateSlug($input['title']);

        $input['details'] = $input['details'] ? $input['details'] : 'N/A';
        $target->update($input);

        Session::flash('success','Item updated successfully.');

        return redirect()->back();
    }

    public function create(){
        $employment_types = ['Full-time','Part-time','Contract','Internship'];
        $experience = ['Less than a year','1-2 years','3-5 year','5-7 years','8-10 years','More than 10 years'];
        $careers = ['Junior','Mid Level','Senior','Management'];
        $countries = Country::orderBy('name','ASC')->get();
        $industries = Industry::get();

        return view('jobs.create',compact('employment_types','experience','careers','countries','industries'));
    }

    public function edit($id){

        $data = $this->model->find($id);

        if($data->user_id != Auth::user()->id)
            return 'Item not found';

        $employment_types = ['Full-time','Part-time','Contract','Internship'];
        $experience = ['Less than a year','1-2 years','3-5 year','5-7 years','8-10 years','More than 10 years'];
        $careers = ['Junior','Mid Level','Senior','Management'];
        $countries = Country::orderBy('name','ASC')->get();
        $industries = Industry::get();

        return view('jobs.edit',compact('data','employment_types','experience','careers','countries','industries'));
    }

    public function applicants($id){
        $data = JobApplicant::where('job_id',$id)->get();
        $job = Job::find($id);

        if($job->user_id != Auth::user()->id)
            return 'Item not found';

        return view('jobs.applicants',compact('data','job'));
    }

    public function apply($id){
        $data = $this->model->find($id);

        JobApplicant::create(['job_id'=>$id,'user_id'=>Auth::user()->id]);

        $emailData = [
            'id' => $data->id,
            'title' => $data->title,
            'company' => $data->company,
            'employment_type' => $data->employment_type,
            'experience' => $data->experience,
            'career_level' => $data->career_level,
            'details' => $data->details,
        ];

        $subject = 'AMSI Website Job Application';

        try {
            Mail::send('mail.application', ['data' => $emailData], function ($message) use ($subject) {
                $message->from('alumni@amsi.ae', 'AMSI Website Admin')->to(Auth::user()->email, Auth::user()->firstName)->subject($subject);
            });
        }
        catch (\Exception $e) {

        }

        Session::flash('success','You have successfully applied to this job.');

        return redirect()->back();
    }

    public function cancel($id){
        $data = JobApplicant::where('job_id',$id)->where('user_id',Auth::user()->id)->first();

        $data->delete();

        Session::flash('success','You have successfully cancelled your application to this job.');

        return redirect()->back();
    }

    public function delete($id){

        $data = $this->model->find($id);

        if($data->user_id != Auth::user()->id)
            return 'Item not found';

        $country = Country::find($data->country_id);
        $city = Country::find($data->city_id);

        $location = JobLocation::where('location',($city ? $city->name.', ' : '').$country->name)->first();

        if($location){
            $location->total = $location->total - 1;
            $location->save();
        }

        $data->delete();

        return redirect()->back();
    }
}
