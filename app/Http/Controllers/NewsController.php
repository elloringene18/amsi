<?php
namespace App\Http\Controllers;

use App\Exports\EventAttendeeExports;
use App\Models\Article;
use App\Models\UserEvent;
use App\Traits\CanCreateSlug;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;

class NewsController extends Controller
{
    use CanCreateSlug;

    public function __construct(Article $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = Article::orderBy('id','DESC')->where('event_type','news')->paginate(100);

        return view('admin.news.index',compact('data'));
    }

    public function edit($id){
        $item = Article::find($id);

        if($item){
            return view('admin.news.edit',compact('item'));
        }

        return 'ERROR';
    }

    public function create(){
        return view('admin.news.create');
    }

    public function store(Request $request){
        $input = $request->except('_token','photo');
        $photo = $request->file('photo');

        if($photo){
            $destinationPath = 'public/uploads/news';
            $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

            Image::make($photo->getRealPath())->fit(1600, 600)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/news/'. $newFileName;

            $destinationPath = 'public/uploads/news/thumbnails';
            $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

            Image::make($photo->getRealPath())->fit(400, 240)->save($destinationPath.'/'.$newFileName);
            $input['thumbnail'] = 'uploads/news/thumbnails/'. $newFileName;
        }

        $input['date'] = Carbon::parse($input['date']);
        $input['slug'] = $this->generateSlug($input['title']);
        Article::create($input);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }


    public function update(Request $request){

        $input = $request->except('_token','delete_photo','id');
        $delete_photo = $request->input('delete_photo');
        $photo = $request->file('photo');
        $target = $this->model->find($request->input('id'));

        if($target){
            if($photo){
                $destinationPath = 'public/uploads/news';
                $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

                Image::make($photo->getRealPath())->fit(1600, 600)->save($destinationPath.'/'.$newFileName);
                $input['photo'] = 'uploads/news/'. $newFileName;

                $destinationPath = 'public/uploads/news/thumbnails';
                $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

                Image::make($photo->getRealPath())->fit(400, 240)->save($destinationPath.'/'.$newFileName);
                $input['thumbnail'] = 'uploads/news/thumbnails/'. $newFileName;
            }

            if($delete_photo){
                $input['thumbnail'] = null;
                $input['photo'] = null;
            }

            $input['date'] = Carbon::parse($input['date']);

            if($target->title != $input['title'])
                $input['slug'] = $this->generateSlug($input['title']);

            $target->update($input);
        }

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

}
