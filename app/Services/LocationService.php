<?php

namespace App\Services;

use App\Models\Country;

class LocationService {

    public function getAllCountries(){
        return Country::pluck('name');
    }

    public function getAllCountriesWithId(){
        return Country::get();
    }
}
