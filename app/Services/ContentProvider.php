<?php

namespace App\Services;

use App\Models\PageContent;
use Carbon\Carbon;

class ContentProvider {

    public function getPageContent($page){
        $contents = PageContent::where('page',$page)->get();

        foreach($contents as $content){
            $data[$content->type] = $content->content;
        }

        return $data;
    }

}
